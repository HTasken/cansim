\documentclass[11pt, a4paper]{article} 
\usepackage[T1]{fontenc} 				% Vise norske tegn.
\usepackage[norsk]{babel}				% Tilpasning til norsk.
\usepackage[utf8]{inputenc}             % selv puttet inn for å kunne skrive norske tegn
\usepackage{graphicx}       				% For å inkludere figurer.
\usepackage{amsmath,amssymb} 				% Ekstra matematikkfunksjoner.
\usepackage{siunitx}					% Må inkluderes for blant annet å få tilgang til kommandoen \SI (korrekte måltall med enheter)
\usepackage{cite}
	\sisetup{exponent-product = \cdot}      	% Prikk som multiplikasjonstegn (i steden for kryss).
 	\sisetup{output-decimal-marker  =  {,}} 	% Komma som desimalskilletegn (i steden for punktum).
 	\sisetup{separate-uncertainty = true}   	% Pluss-minus-form på usikkerhet (i steden for parentes). 
\usepackage{booktabs}                     		% For å få tilgang til finere linjer (til bruk i tabeller og slikt).
\usepackage[font=small,labelfont=bf]{caption}		% For justering av figurtekst og tabelltekst.
\usepackage{comment}                            % for å kunne kommentere i bolker (med \begin{comment} og \end{comment}
\usepackage[export]{adjustbox}            % for å kunne plassere figurer bedre, høyre venstre justering
\usepackage[below]{placeins}                      %tillatter bruk av \FloatBarrier ([above], [below])

%\usepackage[section]{below}        %begrenser figuerer til sin section, kan bruker i stedet for \usepackage{placeins} 

% Disse kommandoene kan gjøre det enklere for LaTeX å plassere figurer og tabeller der du ønsker.
\setcounter{totalnumber}{5}
\renewcommand{\textfraction}{0.05}
\renewcommand{\topfraction}{0.95}
\renewcommand{\bottomfraction}{0.95}
\renewcommand{\floatpagefraction}{0.35}

\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}


\title{Model used by \textit{cansim}}
\maketitle

\textit{cansim} simulates the growth of a tumor of a specific patient on a specified dosage of and drug administration schedule for Avastin (that affects the growth of blood vessels) and the chemotherapies; Fluorouracil, Epirubicain and Cyclophosphamide. This note details the model used by \textit{cansim} and can be used as a reference when looking through the source code. It does not include any justifications as to why this is a reasonable model biologically (or otherwise) or explain the dynamics of the possible solutions. For this see \cite{Lai4293} and its supplementary material.

\section*{What is included?}
\begin{itemize}
\item Two spatial dimensions (in addition to time); \textit{cansim} looks at a slice of the tumor and the surrounding area. 
\item Cancer cells and non-cancer cells
\item A simple cell cycle based on the oxygen concentration. 
\item Rules for proliferation and death of the cancer cells based on the chemotherapies consentration.
\item The chemotherapies: Fluorouracil, $G_1$, Epirubicain, $G_2$, and Cyclophosphamide, $G_3$
\item Oxygen, $K$
\item The drug Avastin, $A$
\item VEGF (Vascular Endothelial Growth Factor), $V$ and the complex it creates together with Avastin, $C$
\item P53, $P$, a protein that acts as a tumor suppressor in cells, and equations describing its interaction with intercellular VEGF, $V_{\mathrm{cell}}$ and oxygen.
\item Blood vessels 
\item Pharmacokinetic equations giving the concentration of Avastin and chemotherapies in the blood.
\item Diffution-reaction equations describing how the concentration of chemotherapies, Oxygen, Avastin, VEGF, VEGF-Avastin complex changes with time in the extracellular space and how Avastin and VEGF interact.
\item Rules from creation and removal of blood vessels based on the concentration of VEGF.
\end{itemize}

\section*{The Model}
\subsection*{Cells and Blood Vessels}
The cancer cells and non-cancer cells are represented by a cellular automata of a square, 2D grid, where each gridpoint can be either a cancer cell, a healthy, non-cancer cell or empty. The cellular automata is initialized from a biopsy of a tumor where all the cells have been determined to be cancer or non-cancer cell.

The blood vessels are also represented by a separate cellular automata of a square, 2D grid, where each gridpoint can either be a vessel or empty. The vessels permeability and size are a function of the time since the creation of the vessel, where vessles younger that a limit have a different permeability and/or size. All the vessels are initialized as older than this limit. 

\subsection*{Vessels Placing Algorithm}
The placing for the initial vessels are determined by the algorithm described here. The first vessel is placed on the gridpoint with coordiantes, $(x_1, y_1)$, with can either be placed in the center gridpoint or on a random gridpoint. The position of the n'th vessel is then drawn with equal probability from all the gridpoints with coordinates, $(x, y)$, such that 
\begin{align}
\left| x - x_i \right| &= d_{sep} \\
&\text{or} \\
\left| y - y_i \right| &= d_{sep}
\end{align}
for some $i < n$, and 
\begin{equation}
\sqrt{(x - x_i)^2 + (y - y_i)^2} \geq d_{sep}
\end{equation}
for all $i < n$. $d_{sep}$ is here the provided minimum distance between the vessels. The number of vessels are specified by the size of the cellular automata and the vessel density.


\subsection*{Drug Concentration in the Blood}
The drug concentration of Avastin and the chemotherapies in the by their respective pharmacokinetic equations, dosage and drug administration schedule. For the Avastin concentration in the blood vessels (plasma), $A_1$, a two-compartments model are used with the following equations:
\begin{equation}
\begin{cases}
\frac{\mathrm{d} A_1(t)}{\mathrm{d} t} &= - \frac{q}{v_1}A_1(t) - \frac{cl}{v_1}A_1(t) + \frac{q}{v_2}A_2(t) \\
\frac{\mathrm{d} A_2(t)}{\mathrm{d} t} &= - \frac{q}{v_2}A_2(t) - \frac{cl}{v_1}A_1(t),
\end{cases}
\end{equation}
where $q$, $v_1$, $v_2$, $cl$ are constants and $A_2$ is the concentration of Avastin if the peripheral compartments. 

The concentration of Fluorouracil in the blood, $G_1^1$, is given by:
\begin{equation}
\frac{\mathrm{d}G_1^1}{\mathrm{d}t} = -\frac{V_{max}}{K_m + G_1^1} G_1^1,
\end{equation}
where $V_{max}$ and $K_m$ are constants.

The concentration of Epirubicin in the blood, $G_2^1$, is given by:
\begin{equation}
\begin{cases}
\frac{\mathrm{d}G_2^1}{\mathrm{d}t} &= - \frac{q_2}{w_1}G_2^2 - \frac{q_3}{w_1}G_2^3 - \frac{cl_2}{w_1}G_2^1 + \frac{q_3}{w_3}G_2^3 + \frac{q_2}{w_2}G_2^2 \\
\frac{\mathrm{d}G_2^2}{\mathrm{d}t} &= - \frac{q_2}{w_2}G_2^2 + \frac{q_2}{w_1}G_2^1 \\
\frac{\mathrm{d}G_2^3}{\mathrm{d}t} &= - \frac{q_3}{w_3}G_2^3 + \frac{q_3}{w_1}G_2^1, 
\end{cases}
\end{equation}
where  $q_1$, $q_2$, $q_3$, $w_1$, $w_2$, $w_3$ and $cl_2$ are constants and $G_2^2$ and $G_2^3$ are concentration in two peripheral compartments.

The concentration of Cyclophosphamide in the blood, $G_3^1$, is given by:
\begin{equation}
\frac{\mathrm{d}G_3^1}{\mathrm{d}t} = -\frac{cl_3}{u} G_3^1,
\end{equation}
where $cl_3$ and $u$ are constants.
 
\subsection*{Extracellular-Extravascular Diffusion-Reaction Equations}
Each drug is modelled as flowing from the vessels to the extracellular-extravascular space (EES) at rate, $R_{\mathrm{U}}$. The rate is given by the concentration difference and the surface-permeability product as:
\begin{align}
\begin{split}
R_{\mathrm{U}}(t) =& \int_{V_0} \left(U_{\mathrm{v}}(t) - U_{\mathrm{EES}}(\boldsymbol{x}, t)\right) P_{\mathrm{U}} S(\boldsymbol{x}) \, \mathrm{d}\boldsymbol{x} 
\\
    =& \int_{V_0} \left(U_{\mathrm{v}}(t) - U_{\mathrm{EES}}(\boldsymbol{x}, t)\right)P_{\mathrm{U}} \frac{2\pi r(\boldsymbol{x}, t)h}{V_0} \sum_{\boldsymbol{x}_{\mathrm{v}} \in \boldsymbol{X}_{\mathrm{v}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{v}}, t) \, \mathrm{d}\boldsymbol{x} 
\\
    =& \frac{P_{\mathrm{U}} 2\pi r_{\mathrm{avg}}^{\mathrm{init}} h}{V_0} \int_{V_0} \left(U_{\mathrm{v}}(t) - U_{\mathrm{EES}}(\boldsymbol{x}, t)\right)\frac{r(\boldsymbol{x}, t)}{r_{\mathrm{avg}}^{\mathrm{init}}} \\ &\sum_{\boldsymbol{x}_{\mathrm{v}} \in \boldsymbol{X}_{\mathrm{v}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{v}}, t) \, \mathrm{d}\boldsymbol{x}
\,,
\label{rate_eq_1}
\end{split}
\end{align}
where $U_{\mathrm{v}}$ and $U_{\mathrm{EES}}$ is the concentration of the drug in the vessel and EES respectivly, $P_{\mathrm{U}}$ is the permeability of the drug, $S$ is the surface per volume, $V_0$ is the total volume, $r(\boldsymbol{x}, t)$ is the radius of the vessel at $\boldsymbol{x}$, $h$ is the length of the vessel (which is the height of the model and equal to the cell size), $X_{\mathrm{v}}$ is the set of blood vessels locations, $\delta$ is Dirac's delta function and $r_{\mathrm{avg}}^{\mathrm{init}}$ is the average radius of the initial vessels. 

The concentration of oxygen, $K$ in the EES is modelled by this diffusion equation:
\begin{equation}   
\begin{split}
\frac{\partial K(\boldsymbol{x}, t)}{\partial t} = D_K \nabla^2K(\boldsymbol{x}, t) - \frac{\phi_K K(\boldsymbol{x}, t)}{K_1 + K(\boldsymbol{x}, t)}\sum_{\boldsymbol{x}_{\mathrm{c}} \in \boldsymbol{X}_{\mathrm{c}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{c}}, t) \\
+ \frac{P_{\mathrm{K}}2\pi r_{\mathrm{avg}}^{\mathrm{int}}h}{V_0}\frac{r(\boldsymbol{x}, t)}{r_{\mathrm{avg}}^{\mathrm{init}}}(K_0 - K(\boldsymbol{x}, t))\sum_{\boldsymbol{x}_{\mathrm{v}} \in \boldsymbol{X}_{\mathrm{v}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{v}}, t) \dot, 
\end{split}
\end{equation}
where $D_k$ is the diffusion constant for $K$, $K_0$ is the oxygen concentration in th vessels, $\phi_K$ and $K_1$ are constants and $X_c$ is the set of cell locations.

The initial volume transfer constant, $K_{\mathrm{trans}}^{\mathrm{init}}$, is (under some conditions that is assumed to be fulfilled) a measure of the permeability of Gadolinium-based contrast agent and equal to the surface-permeability product: $K_{\mathrm{trans}}^{\mathrm{init}} = P_{\mathrm{Gado}} S^{\mathrm{init}} = P_{\mathrm{Gado}} N_{\mathrm{v}}^{\mathrm{init}} 2\pi r_{\mathrm{avg}}^{\mathrm{init}} h / V_0$, where $N_{\mathrm{v}}^{\mathrm{init}}$ is the initial number of vessels. The permeability of the chemotherapies and Avastin is assumed to be the same as that of the contrast agent except with a scaling factor, $c_{\mathrm{U}}$; $P_{\mathrm{U}}= c_{\mathrm{U}}K_{\mathrm{trans}}^{\mathrm{init}}V_0/(N_{\mathrm{v}}^{\mathrm{init}} 2\pi r_{\mathrm{avg}}^{\mathrm{init}} h)$. $P_{\mathrm{U}}$ in equation \eqref{rate_eq_1} can then for Avastin and the chemotherapies be substituted to give: 
\begin{align}
    R_{\mathrm{U}}(t) = \frac{c_{\mathrm{U}}K_{\mathrm{trans}}^{\mathrm{init}}}{N_{\mathrm{v}}^{\mathrm{init}}}\int_{V_0} &\frac{r(\boldsymbol{x}, t)}{r_{\mathrm{avg}}^{\mathrm{init}}}\left(U_{\mathrm{v}}(t) - U_{\mathrm{EES}}(\boldsymbol{x}, t)\right) \\
         & \sum_{\boldsymbol{x}_{\mathrm{v}} \in \boldsymbol{X}_{\mathrm{v}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{v}}, t) \,\mathrm{d}\boldsymbol{x} \,,
\end{align}

The total number of vessels can further be expressed as $N_{\mathrm{v}}^{\mathrm{init}} = \\ \rho_{\mathrm{v}}^{\mathrm{init}}V_0/(\pi {r_{\mathrm{avg}}^{\mathrm{init}}}^{2}h)$, where $\rho_{\mathrm{v}}^{\mathrm{init}}$ is the initial blood volume density. That is the vessel volume per tissue volume at the start of the simulation. (The permeability of the oxygen on the other hand is modelled as a constant.)

The concentration of a chemotherapy, $G_i$, in the EES are modelled by these diffusion equations:
\begin{align}
\frac{\partial G_i(\boldsymbol{x}, t)}{\partial t} &= D_{G_i}\nabla^2G_i(\boldsymbol{x}, t) - \psi_{G_i}G_i \\&+ \frac{c_{\mathrm{G}}K_{\mathrm{trans}}^{\mathrm{init}}}{N_{\mathrm{v}}^{\mathrm{init}}}\frac{r(\boldsymbol{x}, t)}{r_{\mathrm{avg}}^{\mathrm{init}}}(G_i^1(t) - G_i(\boldsymbol{x}, t))\sum_{\boldsymbol{x}_{\mathrm{v}} \in \boldsymbol{X}_{\mathrm{v}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{v}}, t) \,, 
\end{align}
where $D_{G_i}$ is the diffusion constant of $G_i$, $\psi_{G_i}$ is the linear decay rate of $G_i$, $G_i^1$ is the chemotherapy concentration in the blood plasma.
 
Avastin, $V$, is a VEGF-inhibitor and reduces the amount of VEGF, $V$, and produces a VEGF-Avastin complex, $C$. There interaction and concentrations are given by this set of equations:
\begin{equation}
\begin{cases}
\frac{\partial V(\boldsymbol{x}, t)}{\partial t} = &D_V \nabla^2 V(\boldsymbol{x}, t) + (aV_{\mathrm{c}}(\boldsymbol{x}, t) + b)\sum_{\boldsymbol{x}_{\mathrm{c}} \in \boldsymbol{X}_{\mathrm{c}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{}}, t) \\ & - k_a A(\boldsymbol{x}, t) V(\boldsymbol{x}, t) + k_d C(\boldsymbol{x}, t) - \psi_V V(\boldsymbol{x}, t) \\
\\
\frac{\partial A(\boldsymbol{x}, t)}{\partial t} = &D_A \nabla^2 A(\boldsymbol{x}, t) \\ &- k_a A(\boldsymbol{x}, t) V(\boldsymbol{x}, t) + k_d C(\boldsymbol{x}, t) - \psi_A A(\boldsymbol{x}, t) \\ &+ \frac{c_{\mathrm{A}}K_{\mathrm{trans}}^{\mathrm{init}}}{N_{\mathrm{v}}^{\mathrm{init}}}\frac{r(\boldsymbol{x}, t)}{r_{\mathrm{avg}}^{\mathrm{init}}}(A_1(t) - A(\boldsymbol{x}, t))\sum_{\boldsymbol{x}_{\mathrm{v}} \in \boldsymbol{X}_{\mathrm{v}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{v}}, t) \\
\\
\frac{\partial C(\boldsymbol{x}, t)}{\partial t} = &D_C \nabla^2 C(\boldsymbol{x}, t) + k_a A(\boldsymbol{x}, t) V(\boldsymbol{x}, t) \\ & -k_d C(\boldsymbol{x}, t) - \psi_C C(\boldsymbol{x}, t),
\end{cases}
\end{equation}
where $D_V$, $D_A$, $D_C$, $a$, $b$, $k_a$, $k_d$, $\psi_V$, $\psi_A$, $\psi_C$ and $c_{\mathrm{A}}$ are constants and $V_{\mathrm{c}}$ is the intracellular concentration of VEGF.

\subsection*{Intracellular VEGF}
The concentration of intracellular VEGF, $V_{\mathrm{c}}$, of each cell is given by the equation set:
\begin{equation}
\begin{cases}
\frac{\mathrm{d}P(t) }{\mathrm{d}t} = k_7 - k_{77} \frac{K(\boldsymbol{x}, t}{K_{p53} + K(\boldsymbol{x}, t)} P(t) \\
\\
\frac{\mathrm{d} V_{\mathrm{c}}(t) }{\mathrm{d} t} = k_8 + k_{888} \frac{P(t)V_{\mathrm{c}}}{J_5 + V_{\mathrm{c}}} - k_{88} \frac{K(\boldsymbol{x}, t)} {K_{V}  + K(\boldsymbol{x}, t)}V_{\mathrm{c}}
\end{cases}
\end{equation}

\subsection*{Proliferation and Death of Cancer Cells}
The cell cycle, $\phi$, of the cancer cell is model with:
\begin{equation}
\frac{\mathrm{d}\phi}{\mathrm{d}t} = \frac{K(\boldsymbol{x}, t)}{T(K_{\phi} + K(\boldsymbol{x}, t))},
\end{equation}
where $T$ the length of the cell cycle. All initial cancer cells are assigned a random $\phi$ value between 0 and 1. The value of the cell cycle of a newly divided cell is set to $\phi = 0$, and it increases until it reaches proliferation at $\phi = 1$. At that point the cancer cell will either divide or die. 

The cancer cell is killed by chemotherapy $i$ with a probability given by:
\begin{equation}
\mathrm{B}\left(\frac{G_i(\boldsymbol{x}_{\mathrm{cell}}, t)}{\mathrm{max}(G_i^1)}, 1, b\right),
\end{equation}
where B is the beta function, $\mathrm{max}(G_i^1)$ is the highest concentration of chemotherapy $i$ in the blood for all time and $b$ is the sensitivity towards chemotherapy $i$.

All cancer cells as proliferation ($\phi = 1$) that are not killed by the chemotherapies will add a new cancer cell in the empty, neighbouring gridpoint with highest oxygen concentration. If no neighbouring gridpoints are empty no cancer cells are placed. A neighbouring gridpoint of a gridpoint in row $r$ and column $c$ is here a gridpoint in row $r_n$ and column $c_n$ such that $r_n = r$ and $\left| c_n - c \right| = 1$ or $c_n = c$ and $\left| r_n - r \right| = 1$. That is a gridpoint in the Moore neighbourhood.

The healthy, non-cancer cells does neither proliferate or die, but stay in the same place the whole simulation.

\subsection*{Blood vessel creation and removal}
The number of new vessels created, $n_{\mathrm{new}}$, at each time-step, $\Delta t$, is drawn from a binomial distribution with a expected value: 
\begin{align}
E[n_{\mathrm{new}}] =& Pr_{\mathrm{sprout}}^{\mathrm{max}}r_{\mathrm{avg}}^{\mathrm{init}}\Delta t 2\pi  h_{\mathrm{c}}\\ 
& \int \frac{r(\boldsymbol{x}_{\mathrm{v}}, t)}{r_{\mathrm{avg}}^{\mathrm{init}}}\frac{V(\boldsymbol{x}_{\mathrm{v}}, t)}{V_{\mathrm{sprout}} + V(\boldsymbol{x}_{\mathrm{v}}, t)}\sum_{\boldsymbol{x}_{\mathrm{v}} \in \boldsymbol{X}_{\mathrm{v}}}\delta(\boldsymbol{x} - \boldsymbol{x}_{\mathrm{v}}, t)\, \mathrm{d}\boldsymbol{x},
\end{align}
where $Pr_{\mathrm{sprout}}^{\mathrm{max}}$ is that maximum probability that a endothelial sprout emerge from a surface of a vessel and form a new vessel, $h_{\mathrm{c}}$ is the hight of the cell layer simulated and $V_{\mathrm{sprout}}$ is the VEGF concentration at which the probability is half-maximal. The model used in \cite{Owen2011} gives this expected value.

The new vessels are only placed in empty gridpoints where the VEGF concentration is higher that a lower limit and lower that a high limit. The probability of a new vessel to be placed in a gridpoint satisfying this is proportional to the VEGF concentration. 

Each vessel where the VEGF concentration satisfy $V < V_{\mathrm{low}}$ or $V > V_{\mathrm{high}}$ will die and be removed with a fixed propabiltiy, $P_{\mathrm{death}}$.

\section*{Numerical Solution}
This section gives some more details as to how it is all is solved numerically. Most this section just give the equation on the form that is used for solving them. That is the form you find them in the source code. 

\subsection*{Cells and Blood Vessels}
The creation and removal of new cancer cells and blood vessels is done as explained in the model section. \textit{cansim} is designed to run in parallell and having the differential equations interact with these discrete part of the model, while keeping it scalable, is a technical and interesting challenge. It will however not be discussed here.

\subsection*{Drug Concentration in the Blood}
The pharmacokinetic equations for Avastin and the chemotherapies does not depent on any thing other than the known dosages and administration schedule, so they are solved for all time right away.

For Avastin the solution look like:
\begin{equation}
A_1(t) = D\left(\frac{\alpha - \frac{q}{v_2}}{(\alpha - \beta)v_1} e^{-\alpha (t - T_{\mathrm{dose}}(t))} + \frac{-\beta + \frac{q}{v_2}}{(\alpha - \beta)v_1} e^{-\beta (t - T_{\mathrm{dose}}(t))}\right),
\end{equation}
where
\begin{align}
\beta &= \frac{1}{2} \left(\frac{q + cl}{v_1} + \frac{q}{v_2} - \sqrt{\frac{q + cl}{v_1} + \frac{q}{v_2} - 4\frac{q \, cl}{v_1 v_2}} \right), \\
\alpha & = \frac{q \, cl}{v_1 v_2 \beta}
\end{align}
$D$ is the weight in grams multiplied by the dose of Avasting and $T_{\mathrm{dose}}(t)$ is the time of the last dose that was taken.


\bibliography{model}{}
\bibliographystyle{plain}
\end{document}
