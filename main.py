from model_parts.helpers.mesh import BaseMesh
from model_parts.drugs_in_vessels import DrugsInVessels
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.chemo import Chemotherapy
from model_parts.oxygen import Oxygen
from model_parts.vegf_avastin import VegfAvastin
from input_output.save import Save
from model_parts.helpers.checkpoint_time import get_time_last_checkpoint
import input_output.log
import model_parts.helpers.add_sim_db_to_path
import sim_db
import os
from dolfin import *

set_log_level(LogLevel.WARNING)
timer_whole_sim = Timer("Whole simulation")
timer_init = Timer("Initialise simulation")
################# Connect to database and read parameters #####################
sim_database = sim_db.SimDB(rank=MPI.comm_world.Get_rank())
save = Save(sim_database, "root/results")
res_dir = sim_database.read("results_dir")
input_output.log.log('Init database', 'w', res_dir)
############# Initialise the classes used in the simulation ###################
# Calculate drug plasma concentration for all time using pharmacokinetic models
input_output.log.log('Init drug','a', res_dir)
drugs_in_vessels = DrugsInVessels(sim_database)

# Mesh used as basis for all the functions.
input_output.log.log('Init mesh','a',res_dir)
base_mesh = BaseMesh(sim_database)
# Initialise the cells from the type and location data in 'cell_init_filename'
# or a randomized algorithme.
# Cell cylce is set randomly for each cell
input_output.log.log('Init cell','a',res_dir)
cells = Cells(sim_database, base_mesh)

# Initialise the vessels with a randomized algorithme or 'vessel_init_filename'
input_output.log.log('Init vessel','a',res_dir)
vessels = Vessels(sim_database, base_mesh)
input_output.log.log('Init Chemos','a',res_dir)
G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')
G2 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G2')
G3 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G3')
input_output.log.log('Init O2','a',res_dir)
oxygen = Oxygen(sim_database, base_mesh)
input_output.log.log('Init VAC','a',res_dir)
# Extracellular-extravascular VEGF, Avastin and VEGF-Avastin complex functions
vegf_avastin = VegfAvastin(sim_database, base_mesh, drugs_in_vessels)

# Read time parameters from database and convert to minutes.
tot_time = sim_database.read('tot_time')*7*24*60
dt_cell_update = sim_database.read('dt_cell_update')
dt_vessel_update = sim_database.read('dt_vessel_update')*60
dt_visualise = sim_database.read('dt_visualise')*60
dt_checkpoint = sim_database.read('dt_checkpoint')*60

# Set time to zero or last checkpoint if one exists.
time_at_last_checkpoint = get_time_last_checkpoint(sim_database)
time = time_at_last_checkpoint
########### Solve steady-state equations and use as initial values ############

if time < 1e-9:
    input_output.log.log("Solving oxygen steady-state",'a',res_dir)
    e = Expression("constant", constant=1e-4, degree=1)
    oxygen.concentration.function.interpolate(e)
    oxygen.solve_steady_state(vessels, cells)
    input_output.log.log("Solving intercellular vegf steady-state",'a',res_dir)
    cells.solve_vegf_steady_state(oxygen)
    input_output.log.log("Solving vegf-avastin steady-state",'a',res_dir)
    vegf_avastin.solve_steady_state(vessels, cells, oxygen)

save.hdf5(sim_database, time, vessels, cells, oxygen, G1, G2, G3, vegf_avastin,'w')
#save mesh
with HDF5File(MPI.comm_world,
                      os.path.join(res_dir, "functions.hdf5"), 'a') as f:
            f.write(base_mesh.mesh, "mesh")
timer_init.stop()
################### Run simulation until finished #############################

time_of_last_vessel_update = 0
is_vessel_updated = True
time = time + dt_cell_update

while time < tot_time:

    input_output.log.log("Current time: {0}".format(time),'a',res_dir)

    # Update chemotherapies
    input_output.log.log("Update G1",'a',res_dir)
    G1.update(time, is_vessel_updated, vessels)
    input_output.log.log("Update G2",'a',res_dir)
    G2.update(time, is_vessel_updated, vessels)
    input_output.log.log("Update G3",'a',res_dir)
    G3.update(time, is_vessel_updated, vessels)
    # Update cancer cells and vessels
    input_output.log.log("Update Cancer Cells",'a',res_dir)
    cells.update_cancer_cells(time, oxygen, G1, G2, G3)
    # Update vessels at each 'dt_vessel_update' time-step.
    if (time % dt_vessel_update > dt_vessel_update - 0.5*dt_cell_update
            or time % dt_vessel_update <= 0.5*dt_cell_update): 
        input_output.log.log("Update Vessels",'a',res_dir)
        vessels.update(time, vegf_avastin)
        time_of_last_vessel_update = time
        is_vessel_updated = True
    elif time_at_last_checkpoint != 0:
        is_vessel_updated = True
        time_at_last_checkpoint = 0
    else:
        is_vessel_updated = False
    # Update oxygen
    input_output.log.log("Update Oxygen",'a',res_dir)
    oxygen.update_linearized(time, is_vessel_updated, vessels, cells)
    # Update vegf-avasin
    input_output.log.log("Update VAC",'a',res_dir)
    vegf_avastin.update(time, is_vessel_updated, vessels, cells, oxygen, update_avastin=False) 



    # Write to vtk files at each 'dt_visualise' time-step.
    if (time % dt_visualise > dt_visualise - 0.5*dt_visualise
            or time % dt_visualise <= 0.5*dt_cell_update): 
        save.vtk(vessels, cells, oxygen, G1, G2, G3, vegf_avastin)

    # Write to hdf5 files at each 'dt_checkpoint' time-step.
    if (time % dt_checkpoint > dt_checkpoint - 0.5*dt_checkpoint
            or time % dt_checkpoint <= 0.5*dt_cell_update): 
        save.hdf5(sim_database, time, vessels, cells, oxygen, G1, G2, G3, 
                  vegf_avastin,'a')
    # Update time.
    time = time + dt_cell_update

# Save functions as hdf5, times for solving the different PDE's and timings.
save.hdf5(sim_database, time, vessels, cells, oxygen, G1, G2, G3, vegf_avastin,'a')
save.solve_times(oxygen, G1, G2, G3, vegf_avastin)
timer_whole_sim.stop()
save.timings()

sim_database.close()
input_output.log.log("Simulation ended",'a',res_dir)
