import __init__
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.load_function import load_function_if_checkpoint
from model_parts.helpers.checkpoint_time import get_time_last_checkpoint
from model_parts.helpers.checkpoint_time import get_varing_max_dt_after_injection
from model_parts.helpers.init_num_vessels import calc_init_num_vessels
import model_parts.helpers.add_sim_db_to_path
import sim_db
from dolfin import *
import numpy as np
import input_output.log as log
class Chemotherapy:
    def __init__(self, sim_database, base_mesh, drugs_in_vessels, name):
        """Initiale a chemotherapy with name 'G1', 'G2' or 'G3'."""
        self.drugs_in_vessels = drugs_in_vessels
        self.name = name
        if (name != 'G1' and name != 'G2' and name != 'G3'):
            print("ERROR: Invalid name passed to Chemotherapy. "
                  "MUST be 'G1', 'G2' or 'G3'.")
            exit()
        chemo_index = {'G1': 0, 'G2': 1, 'G3': 2}

        # Read parameters from database.
        self.size = sim_database.read('size')
        self.sensitivity = sim_database.read('chemo_sensitivity')[chemo_index[name]]
        self.ktrans = Constant(sim_database.read('ktrans'))
        self.init_num_vessels = calc_init_num_vessels(sim_database)
        self.dt_injections = sim_database.read('schedule')[chemo_index[name]]*7*24*60
        self.dt_at_injection = sim_database.read('chemo_dt_at_injection')[chemo_index[name]]
        self.dt_increase_factor_after_injection = sim_database.read(
                'chemo_dt_increase_factor_after_injection')[chemo_index[name]]
        self.dt_after_vessel_update = sim_database.read(
                'chemo_dt_after_vessel_update')[chemo_index[name]]
        self.dt_increase_factor_after_vessel_update = sim_database.read(
                'chemo_dt_increase_factor_after_vessel_update')[chemo_index[name]]
        self.max_dt = sim_database.read('chemo_max_dt')[chemo_index[name]]
        n_refinements = sim_database.read('n_refinements_chemo')[chemo_index[name]]
        degree = sim_database.read('degree_chemo')[chemo_index[name]]

        # Set time to last checkpoint of zero if no checkpoints exists.
        self.time = get_time_last_checkpoint(sim_database)

        # Time-step variables
        self.time_next_injection = self.dt_at_injection
        self.time_last_injection_dt_increase = 0.0
        self.time_last_vessel_update_dt_increase = 0.0
        self.varing_max_dt_after_vessel_update = self.dt_after_vessel_update
        self.varing_max_dt_after_injection = get_varing_max_dt_after_injection(
            self.time, self.dt_injections, self.dt_at_injection, 
            self.dt_increase_factor_after_injection)

        # Maximum concentration in blood.
        self.peak_vessel_concentration = np.max(
                drugs_in_vessels.concentrations[name])

        # Using RefinedFunction allow a more accurate function and can be used
        # to check and adjust the error in spatial discritisation.
        self.concentration = RefinedFunction(base_mesh, n_refinements, degree,
                                  create_trial_function=True)

        # Load from checkpoint if any previous checkpoints.
        load_function_if_checkpoint(sim_database, self.concentration.function,
                                    name)

        # Times at which the diffusion-reaction equation was solved.
        self.times_solved = []
        self.times_solved_nonlinearly = []

        # Define constants
        self.D = Constant(9.6*10**(3)) # Diffusion coefficient
        self.psi = Constant(0.01) # Decay rate
        self.c = Constant(np.pi) # Permeability coefficient
        self.phi = Constant(0.0) # Consumption rate (Take care if changing from 0.0)

    def update(self, end_time, is_vessels_updated, vessels, cells=None):
        """Solves a diffusion eq. for chemo conc. for time interval.

        Diffusion equation:
            d G/dt = D*nabla^2*G - psi*G
                   + (c*ktrans/init_num_vessels*relative_radius/avg_radius
                      *(G_vessel - G)*delta_func(x - x_vessel))
        where the relative_radius is the radius of a vessel divided the average
        radius of the initial vessels.

        A consumption rate can be added the equation by setting phi to 
        non-zero values:
            - phi*G*\delta(x - x_cell)

        Args:
            end_time (float): Time of the final solution of G.
            is_vessels_updated (bool): Set 'True' if vessels (or cells) have 
                                       changed since last update() call. 
        """
        # Switch for using nonlinear solver if the linear solver does not
        # converge.
        use_nonlinear_solver_if_not_converging = False

        # Set solver method and preconditioner
        PETScOptions.set("ksp_max_it", 5000)
        PETScOptions.set("ksp_rtol", 1e-8)
        PETScOptions.set("ksp_atol", 1e-8)
        # PETScOptions.set("ksp_view")
        # PETScOptions.set("ksp_converged_reason")
        # PETScOptions.set("ksp_monitor_true_residual")
        PETScOptions.set("ksp_type", "cg")
        PETScOptions.set("pc_type", "hypre")
        PETScOptions.set("pc_hypre_type", "boomeramg")
        PETScOptions.set("pc_hypre_boomeramg_strong_threshold", 0.5)
        # PETScOptions.set("pc_hypre_boomeramg_agg_nl",2)        
#        PETScOptions.set("pc_hypre_boomeramg_interp_type","ext+i")

        timer_update = Timer("Update Chemo")

        # Define functions used in the equation.
        g = self.concentration.trial_function
        v = self.concentration.test_function
        g_prev = self.concentration.prev_function
        g_mid = 0.5*(g_prev + g)
        if use_nonlinear_solver_if_not_converging:
            gf = self.concentration.function
            gf_mid = 0.5*(g_prev + gf)

        timer = Timer("Project")

        # Delta function for vessels times the relative radius of the vessel.
        if is_vessels_updated:        
            self.vessel_locations_rel_radius = interpolate(
                vessels.rel_radius_delta_function.function, 
                self.concentration.function_space)

        # Delta function for cells.
        if cells != None:
            self.cell_locations = interpolate(cells.delta_function.function, 
                                              self.concentration.function_space)
        timer.stop()
        # log.log('Project')
        # Get concentration of G in the vessels.
        g_vessel = Constant(self.drugs_in_vessels.get(self.name, self.time))
        # Set time-step
        if is_vessels_updated:
            self.varing_max_dt_after_vessel_update = self.dt_after_vessel_update
        if (self.varing_max_dt_after_injection < self.varing_max_dt_after_vessel_update
                and self.varing_max_dt_after_injection < self.max_dt):
            dt = Constant(self.varing_max_dt_after_injection)
        elif (self.varing_max_dt_after_vessel_update < self.max_dt):
            dt = Constant(self.varing_max_dt_after_vessel_update)
        else:
            dt = Constant(self.max_dt)

        # Set up variational form of the equation for the chemo concentration.
        F = (
            - (g - g_prev)*v*dx()
            - dt*inner(self.D*grad(g_mid), grad(v))*dx()
            + dt*(self.c*self.ktrans*(g_vessel - g_mid)
                  *self.vessel_locations_rel_radius*v*dx())
            - dt*self.psi*g_mid*v*dx())

        if cells != None:
            F -= dt*self.phi*g_mid*self.cell_locations*v*dx()

        (a, L) = system(F)
        timer = Timer("Assemble Chemo")
        A = assemble(a)
        b = None
        timer.stop()
        solver = PETScKrylovSolver()
        solver.set_from_options()

        if use_nonlinear_solver_if_not_converging:
            F_nonlin = (
                - (gf - g_prev)*v*dx()
                - dt*inner(self.D*grad(gf_mid), grad(v))*dx()
                + dt*(self.c*self.ktrans/self.init_num_vessels*(g_vessel - gf_mid)
                   *self.vessel_locations_rel_radius*v*dx())
                - dt*self.psi*gf_mid*v*dx()
            )
            if cells != None:
                F_nonlin -= self.phi*gf*self.cell_locations*v*dx()

            dF_nonlin = derivative(F_nonlin, self.concentration.function,
                    self.concentration.trial_function)
            problem_nonlin = NonlinearVariationalProblem(F_nonlin,
                    self.concentration.function, None, dF_nonlin)
            nonlin_solver = NonlinearVariationalSolver(problem_nonlin)
            """
            nonlin_solver.parameters["newton_solver"]["linear_solver"] = "cg"
            nonlin_solver.parameters["newton_solver"]["preconditioner"] = "hypre_amg" 
            nonlin_solver.parameters["newton_solver"]["convergence_criterion"] = "incremental"
            nonlin_solver.parameters["newton_solver"]["relative_tolerance"] = 1e-5
            nonlin_solver.parameters["newton_solver"]["maximum_iterations"] = 25 
            """

        # Solve the equation for the time interval.
        while self.time < end_time + 1e-9:
            g_vessel.assign(Constant(self.drugs_in_vessels.get(self.name, self.time)))
            timer = Timer("Assemble Chemo")
            b = assemble(L, tensor=b)
            timer.stop()
            timer = Timer("Solve Chemo")
            try:
                num_iter = solver.solve(A, self.concentration.function.vector(), b)
                # solve(A, self.concentration.function.vector(), b, solver_method, 
                #       preconditioner)
            except Exception as e:
                if use_nonlinear_solver_if_not_converging:
                    nonlin_solver.solve()
                    self.times_solved_nonlinearly.append(self.time)
                else:
                    raise e
            timer.stop()
            self.times_solved.append(self.time)
            g_prev.assign(self.concentration.function)

            # Update time and time-step variables
            next_dt = self.__get_next_dt(end_time)
            dt.assign(Constant(next_dt))
            self.time += next_dt

        self.time -= next_dt
        timer_update.stop()

        return num_iter

    def solve_steady_state(self, vessels, cells,
                           time_update_first=None, first_dt=None):
        """Solves the steady-state version of the PDE for chemo conc.
        Diffusion equation:
            d G/dt = D*nabla^2*G - psi*G
                   + (c*ktrans/init_num_vessels*relative_radius/avg_radius
                      *(G_vessel - G)*delta_func(x - x_vessel))
        where the relative_radius is the radius of a vessel divided the average
        radius of the initial vessels.

        A consumption rate can be added the equation by setting phi to 
        non-zero values:
            - phi*G*\delta(x - x_cell)

        Args:
            end_time (float): Time of the final solution of G.
            is_vessels_updated (bool): Set 'True' if vessels (or cells) have 
                                       changed since last update() call. 
            time_update_first (float): Run update() until 'time_update_first' 
                                       before solving the steady-state.
            first_dt (float): Time-step used when first calling update().
        """
        # Solve the time-dependent equations untils 'time_update_first' to have
        # a G concentration that converges easier into
        # the steady-state equations.
        if time_update_first != None:
            tmp_time = self.time
            tmp_times_solved = list(self.times_solved)
            tmp_dt_after_cell_update = self.dt_after_cell_update
            if first_dt != None:
                self.dt_after_cell_update = first_dt
                self.update(time_update_first + self.time, True, vessels, cells=None)
                self.time = tmp_time
                self.times_solved = tmp_times_solved
                self.dt_after_cell_update = self.dt_after_cell_update

        # Define functions used in the equation.
        g = self.concentration.trial_function
        v = self.concentration.test_function

        # Delta function for vessels times the relative radius of the vessel.
        self.vessel_locations_rel_radius = interpolate(
                vessels.rel_radius_delta_function.function, self.concentration.function_space)
        # Delta function for cells.
        if cells != None:
            self.cell_locations = interpolate(cells.delta_function.function, self.concentration.function_space)

        # Get concentration of G in the vessels.
        g_vessel = Constant(self.drugs_in_vessels.get(self.name, self.time))

        # Set up variational form of the steady-state equation for the VEGF,
        # Avastin and VEGF-Avastin complex concentrations.
        F = (
            - inner(self.D*grad(g), grad(v))*dx()
            + (Constant(self.c*self.ktrans/self.init_num_vessels)
               *(g_vessel - g)*self.vessel_locations_rel_radius*v*dx())
            - self.psi*g*v*dx()
        )
        if cells != None:
            F -= self.phi*g*self.cell_locations*v*dx()

        (a, L) = system(F)
        A, b = assemble_system(a, L)
        solver = PETScKrylovSolver()
        # Solve steady-state        
#        solver.set_from_options()        
        solver.solve(A, self.concentration.function.vector(), b)
        
        # Update previous concentration.
        self.concentration.prev_function.assign(self.concentration.function)
        
    def __get_next_dt(self, end_time):
        if (self.varing_max_dt_after_vessel_update
                < self.varing_max_dt_after_injection
                and self.varing_max_dt_after_vessel_update < self.max_dt):
            next_dt = self.varing_max_dt_after_vessel_update
        elif (self.varing_max_dt_after_injection < self.max_dt):
            next_dt = self.varing_max_dt_after_injection
        else:
            next_dt = self.max_dt
        if (self.time + next_dt > end_time and self.time < end_time - 1e-9):
            next_dt = end_time - self.time
        elif (self.time + next_dt
                >= self.time_next_injection - self.dt_at_injection):
            next_dt = self.time_next_injection - self.dt_at_injection - self.time
            self.time_next_injection += self.dt_injections
            self.varing_max_dt_after_injection = self.dt_at_injection
        if (self.time > self.time_last_vessel_update_dt_increase
                + self.varing_max_dt_after_vessel_update):
            self.time_last_vessel_update_dt_increase += (
                    self.varing_max_dt_after_vessel_update)
            self.varing_max_dt_after_vessel_update *= (
                    self.dt_increase_factor_after_vessel_update)
        if (self.time > self.time_last_injection_dt_increase
                + self.varing_max_dt_after_injection):
            self.time_last_injection_dt_increase += (
                    self.varing_max_dt_after_injection)
            self.varing_max_dt_after_injection *= (
                    self.dt_increase_factor_after_injection)

        return next_dt

if __name__ == '__main__':
    import os
    from model_parts.drugs_in_vessels import DrugsInVessels
    from model_parts.cells import Cells
    from model_parts.vessels import Vessels
    from model_parts.helpers.mesh import BaseMesh

    set_log_level(LogLevel.PROGRESS)

    n = 10

    # Just to make sure vessel_init_filename and cell_init_filename is in the
    # database.
    sim_database = sim_db.add_empty_sim(False)
    sim_database.write("vessel_init_filename", "empty_vessel_init_file.txt", "string")
    sim_database.write("cell_init_filename", "empty_cell_init_file.txt", "string")
    sim_database.delete_from_database()
    sim_database.close()

    sim_database = sim_db.add_empty_sim(False)
    # DrugsInVessels
    sim_database.write("tot_time", 6, "float")
    sim_database.write("vac_dt_at_injection", 0.01, "float")
    sim_database.write("chemo_dt_at_injection", [0.01, 0.01, 0.01], "float array")
    sim_database.write("vac_max_dt", 10.0, "float")
    sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
    sim_database.write("height", 160, "float")
    sim_database.write("weight", 58.2, "float")
    dose = [600, 100, 600, 15]
    sim_database.write("dose", dose, "float array")
    schedule = [3, 3, 3, 3]
    sim_database.write("schedule", schedule, "float array")
    # BaseMesh
    sim_database.write("size", [n, n], "int array")
    sim_database.write("n_refinements", 2, "int")
    sim_database.write("cell_size", 10.0, "float")
    # Cells
    sim_database.write("seed", 1235, "int")
    sim_database.write("frac_cancer", 0.2, "float")
    sim_database.write("frac_normal", 0.1, "float")
    sim_database.write("n_refinements_cells", 0, "int")
    sim_database.write("t_cell", 14.69, "float")
    sim_database.write("k_phi", 1.4, "float")
    # Vessels
    sim_database.write("n_refinements_vessels", 0, "int")
    sim_database.write("n_other_rngs", 1000, "int")
    sim_database.write("vessel_sep", 3, "int")
    sim_database.write("init_vessel_density", 0.1197, "float")
    sim_database.write("avg_init_vessel_radius", 10.0, "float")
    sim_database.write("first_vessel_random", False, "bool")
    sim_database.write("p_max_sprout", 0.002, "float")
    sim_database.write("v_sprout", 0.5, "float")
    sim_database.write("vegf_low", 1e-6, "float")
    sim_database.write("vegf_high", 1e-4, "float")
    sim_database.write("p_death", 0.001, "float")
    sim_database.write("rel_radius_new_vessels", 1.0, "float")
    # Chemotherapy
    sim_database.write("ktrans", 0.14, "float")
    chemo_sensitivity = [10000, 10000, 10000]
    sim_database.write("degree_chemo", [1, 1, 1], "int array")
    sim_database.write("n_refinements_chemo", [0, 0, 0], "int array")
    sim_database.write("chemo_sensitivity", chemo_sensitivity, "float array")
    sim_database.write("chemo_dt_after_vessel_update", [0.1, 0.1, 0.1], "float array")
    sim_database.write("chemo_dt_increase_factor_after_injection", [1.00, 1.00, 1.00], "float array")
    sim_database.write("chemo_dt_increase_factor_after_vessel_update", [1.00, 1.00, 1.00], "float array")
    sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")

    base_mesh = BaseMesh(sim_database)
    drugs_in_vessels = DrugsInVessels(sim_database)
    cells = Cells(sim_database, base_mesh)
    vessels = Vessels(sim_database, base_mesh)
    G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')
    G1.update(30, True, vessels, cells)

    f = File("figs/G1.pvd")
    f << G1.concentration.function

    G1.update(60, False, vessels)

    f << G1.concentration.function

    G1.solve_steady_state(vessels, cells)

    f = File("figs/chemo_steady-state.pvd")
    f << G1.concentration.function

    sim_database.delete_from_database()
    sim_database.close()

    list_timings(TimingClear.keep, [TimingType.wall])
