import __init__
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.delta_function import DeltaFunction
from model_parts.helpers.random_numbers import random_number_generator_state
from model_parts.helpers.load_function import load_function_if_checkpoint
import model_parts.helpers.add_sim_db_to_path
import sim_db
import numpy as np
import random
import math
import time
import warnings
from dolfin import *

class Vessels:
    """Models apperance of new vessels and removal of old.

    The dynamics of this is determinded based on the concentartion of VEGF.
    
    - Provides a FEniCS function of where the vessels are for use as sources 
      and sinks in other PDEs in the model (vessels got volume 1 with exception).
    """
    def __init__(self, sim_database, base_mesh):
        """Initialise the vessels by a randomized algorithme."""

        # Number of refinements done on the mesh for the cell function used as
        # source and sink in the other PDEs, where each refinement half the
        # distance between the gridpoints.
        n_refinements = sim_database.read('n_refinements_vessels')

        seed = sim_database.read('seed')

        # Maximum probability of creating a vessel per vessel surface areal per
        # time multiplied by the average radius of the initial vessels.
        self.p_max_sprout = sim_database.read('p_max_sprout')

        # VEGF concentartion at which point the probability of creating a
        # vessel is half maximum.
        self.V_sprout = sim_database.read('v_sprout')

        # Lower and higher limit for vessel creation and removal.
        self.vegf_low = sim_database.read('vegf_low')
        self.vegf_high = sim_database.read('vegf_high')

        # Probability of a vessel with VEGF concentration between vegf_low and
        # vegf_high to be removed.
        self.p_death = sim_database.read('p_death')

        # The radius of the new vessels created relative to the average
        # initial vessels.
        self.rel_radius_new_vessels = sim_database.read('rel_radius_new_vessels')

        # Average radius of the initial vessels.
        self.avg_init_radius = sim_database.read('avg_init_vessel_radius')

        if seed == None:
            if MPI.comm_world.Get_rank() == 0:
                seed = int(time.time())
                sim_database.write('seed', seed, 'int')
            seed = MPI.comm_world.bcast(seed, root=0)

        self.size = base_mesh.size
        self.cell_size = base_mesh.cell_size
        self.time = 0
        self.comm = MPI.comm_world

        n_tot_refinements = base_mesh.n_refinements + n_refinements
        if n_tot_refinements == 0:
            warnings.warn("The volume of the vessels may deviate for one, " \
                              "as the mesh is not refined.")

        # The vessels are placed on a celluar automata equivalent to the one
        # used for the cells, but with possible a different number of refinements.
        # The celluar automata is also used to represent the relative radius of
        # the vessels. That is the radius divided by the average radius of the
        # initial vessels.
        # relative_radius = 0 -> no vessel, relative_radius > min_radius -> vessel
        self.relative_radius = RefinedFunction(base_mesh, n_refinements=0)

        self.min_radius = 1e-100

        # Load from checkpoint if there exists any previous checkpoints.
        from_checkpoint = load_function_if_checkpoint(
                sim_database, self.relative_radius.function, "vessels")

        # Read the vessels placement and relative radius from file if provided
        # and not starting from checkpoint.
        vessel_init_filename = sim_database.read('vessel_init_filename')
        if not from_checkpoint and vessel_init_filename != None:
            try:
                with open(vessel_init_filename,'r') as f:
                    vessel_init = [line.strip().split(',') for line in f]
            except:
                raise Exception("Could NOT open {}.".format(vessel_init_filename))
            vessel_init = np.array(vessel_init, dtype=float)
            n_vessels = np.sum(vessel_init > self.min_radius)

            # Get density of the initial vessels.
            if not sim_database.read('is_vessel_radius_relative'):
                self.avg_init_radius = np.sum(vessel_init)/n_vessels
                if MPI.comm_world.Get_rank() == 0:
                    sim_database.write('avg_init_vessel_radius',
                                       self.avg_init_radius, "float")
                init_density = 0.0
                for radius in vessel_init.flatten():
                    init_density += math.pi*radius*radius
                init_density /= self.cell_size**2*self.size[0]*self.size[1]
                # Normalize values to relative radii.
                vessel_init *= n_vessels/np.sum(vessel_init)
            else:
                init_density = 0.0
                for rel_radius in vessel_init.flatten():
                    init_density += math.pi*rel_radius**2*self.avg_init_radius**2
                init_density /= self.cell_size**2*self.size[0]*self.size[1]
            if MPI.comm_world.Get_rank() == 0:
                sim_database.write('init_vessel_density', init_density, "float")

            # Set the relative radius.
            self.relative_radius.set(vessel_init)

        # Otherwis generate a vessels placement, reducing vessel_sep until the vessels fits.
        elif not from_checkpoint:

            # Density of the initial vessels.
            init_density = sim_database.read('init_vessel_density')

            # Distance between vessels measured in cell_size.
            vessel_sep = sim_database.read('vessel_sep')

            # If false the firste vessel is placed in the midle, else it is placed
            # randomly.
            first_vessel_random = sim_database.read('first_vessel_random')

            timer = Timer("Generate Vessel Placement")
            vessel_init = self.generate_vessel_placement(self.size, seed,
                    init_density, self.avg_init_radius, vessel_sep,
                    first_vessel_random)
            timer.stop()

            # Set the relative radius.
            self.relative_radius.set(vessel_init)

        # Number and coordinates of the possible, local vessel positions.
        self.n_potential_local_vessels = self.relative_radius.n_potential_local_cells
        self.x_vessel_local = self.relative_radius.local_x_at_cell_positions
        self.y_vessel_local = self.relative_radius.local_y_at_cell_positions

        # Create vessel function to use as sinks and sources when solving the
        # other PDEs.
        # Each vessel is represented by a delta function and modelled as a square
        # pyramid, unless number of total refinements ('n_tot_refinements') is
        # zero. In that case the cells are six sided, slighty squeezed pyramids.
        # This delta function is multiplied with the relative radius.
        self.rel_radius_delta_function = DeltaFunction(self.relative_radius,
                base_mesh, n_refinements, multiply=True)

        # Initialise location of cells from relative_radius.
        self.rel_radius_delta_function.update()

        # Get a unique number generator state with non-overlapping sequences
        # for each coordinate.
        self.rngs = [[None] for i in range(self.n_potential_local_vessels)]
        for i in range(self.n_potential_local_vessels):
            self.rngs[i] = random_number_generator_state(self.x_vessel_local[i],
                                                         self.y_vessel_local[i],
                                                         self.size[0],
                                                         self.relative_radius.cell_size,
                                                         1,
                                                         seed)

    def update(self, end_time, vegf_avastin):
        """Creation and remove of vessels.

        The number of new vessels is drawn from a poisson distribution with 
        expected value:
            E[n_new_vessels] = Pr_max_sprout*r_avg*dt*2*pi*h
                               *sum_x_vessels(r(x)/r_avg*V(x)/(V_sprout + V(x)))

        The new vessels are placed on empty gridpoints where the VEGF 
        concentartion is between vegf_low and vegf_high, with a probability 
        proportional to the VEGF concentartion.

        All old vessels can die with a probability p_death.
        """
        timer_update = Timer("Update Vessels")

        # The relative radii after creation and removal of vessels.
        new_relative_radius = np.empty(self.n_potential_local_vessels, dtype=float)

        # Delta function for vessels times the relative radius of the vessel.
        vessel_locations_rel_radius = interpolate(
                           self.rel_radius_delta_function.location.function,
                            vegf_avastin.V.function_space)

        # Calculate expected number of vessels created.
        const = Constant(self.p_max_sprout*self.avg_init_radius*2*math.pi
                *self.cell_size*(end_time - self.time))
        exp_n_new_vessels = assemble(const*inner(vessel_locations_rel_radius,
                vegf_avastin.V.function/
                (self.V_sprout + vegf_avastin.V.function))*dx)
        # Update time
        self.time = end_time

        # Iterate over all local gridpoints.
        vegf_avastin.V.collect_values()
        tot_vegf_in_valid_interval = 0
        v = 0
        for i, (x, y, r) in enumerate(zip(self.x_vessel_local, self.y_vessel_local,
                self.relative_radius.get_local_at_cell_positions())):
            new_relative_radius[i] = r
            v  = vegf_avastin.V.get_value_at(x, y)
            # Sum up the VEGF concentration at empty gridpoint where the
            # concentration is in the stabile interval.
            if (r < self.min_radius
                    and (v > self.vegf_low and v < self.vegf_high)):
                tot_vegf_in_valid_interval += v

            # Remove vessels where the VEGF concentration is outside stabile
            # interval with a probability p_death.
            else:
                self.rngs[i].set_binomial_int(1, self.p_death)
                if ((v < self.vegf_low or v > self.vegf_high)
                        and r > self.min_radius
                        and self.rngs[i].rand_binomial_int()):
                    new_relative_radius[i] = 0.0
        
        # Sum up tot_vegf_in_valid_interval from each process.
        tot_vegf_in_valid_interval = self.comm.reduce(tot_vegf_in_valid_interval)

        # Send sum back to all processes.
        tot_vegf_in_valid_interval = self.comm.bcast(tot_vegf_in_valid_interval)

        # Iterate over all local gridpoints.
        for i, (x, y, r) in enumerate(zip(self.x_vessel_local, self.y_vessel_local,
                self.relative_radius.get_local_at_cell_positions())):
            v  = vegf_avastin.V.get_value_at(x, y)
            if (tot_vegf_in_valid_interval > 0
                and v > self.vegf_low and v < self.vegf_high):
                self.rngs[i].set_binomial_int(1,
                        exp_n_new_vessels*v/tot_vegf_in_valid_interval)
            else:
                self.rngs[i].set_binomial_int(1, 0.0)
            if (r < self.min_radius
                    and v > self.vegf_low and v < self.vegf_high
                    and self.rngs[i].rand_binomial_int()):
                new_relative_radius[i] = self.rel_radius_new_vessels

        # Update vessels to the newly created and removed ones.
        self.relative_radius.set_local_at_cell_positions(new_relative_radius)
        self.rel_radius_delta_function.update()

        timer_update.stop()

    def generate_vessel_placement(self, size, seed, init_density, avg_radius, vessel_sep,
                                  first_vessel_random, make_video=False):
        """Generate a vessel placement, reducing the vessel separation until the vessel fits.

        The algorithme in vessel_placement() is used.
        """
        # ALL INTERNAL COORDINATES ARE SCALED TO (SMALLEST POSSIBLE) INTEGERS
        # TO BE USED AS INDICES

        random.seed(seed)
        while True:
            try:
                vessel_x, vessel_y = self.vessel_placement(size, init_density,
                        avg_radius, vessel_sep, first_vessel_random, make_video)
                break
            except ValueError:
                vessel_sep -= 1
                print("The vessel separation had to be reduced to:", vessel_sep)
                if vessel_sep == 0:
                    raise Exception("Could NOT place the vessels, even with vessel_sep == 0.")
        vessel_init = np.zeros(size[0]*size[1], dtype=float)
        vessel_init[vessel_y*size[0] + vessel_x] = 1.0

        return vessel_init

    def vessel_placement(self, size, init_density, avg_radius, vessel_sep, first_vessel_random, make_video=False):
        """Place the vessels by a randomized algorithme.

        A starting point at is either taken in the middle or at 
        random if spesified. New gridpoints are drawn randomly from gridpoints 
        with coordinates (x, y) such that (abs(x-x_prev) == vessel_sep or 
        abs(y-y_prev) == vessel_sep for any (x_prev, y_prev)) and (x-x_prev)**2 
        + (y-y_prev)**2 >= vessel_sep**2 for all (x_prev, y_prev), where 
        (x_prev, y_prev) is the coordinates of previously added vessels.

        The number of vessels is determind by the size and init_density.
        """
        # ALL INTERNAL COORDINATES ARE SCALED TO (SMALLEST POSSIBLE) INTEGERS
        # TO BE USED AS INDICES

        # All the vessels are of the same size.
        n_vessels = int(round(self.cell_size**2*size[0]*size[1]*init_density
                              /(math.pi*avg_radius**2)))

        # Return values
        vessel_x = np.zeros(n_vessels, dtype=int)
        vessel_y = np.zeros(n_vessels, dtype=int)

        if n_vessels == 0:
            return (vessel_x, vessel_y)

        # Starting point
        if first_vessel_random:
            vessel_x[0] = random.randint(0, n_vessels - 1)
            vessel_y[0] = random.randint(0, n_vessels - 1)
        else:
            vessel_x[0] = int(size[0]/2.0)
            vessel_y[0] = int(size[1]/2.0)

        # Keep track of which gridpoint that does not fulfill (x-x_prev)**2 +
        # (y-y_prev)**2 >= vessel_sep**2 for all (x_prev, y_prev)
        too_close = np.zeros((size[1], size[0]), dtype=int) #bool)

        # The relative coordinates that fulfills abs(x-x_prev) == vessel_sep
        # abs(y-y_prev) == vessel_sep for a particular (x_prev, y_prev).
        rel_boarder_x = []
        rel_boarder_y = []
        vessel_sep = int(vessel_sep)
        for x in range(-vessel_sep, vessel_sep + 1):
            for y in range(-vessel_sep, vessel_sep + 1):
                if abs(x) == vessel_sep or abs(y) == vessel_sep:
                    rel_boarder_x.append(x)
                    rel_boarder_y.append(y)

        # The relative coordinates that fulfills (x-x_prev)**2 + (y-y_prev)**2
        # >= vessel_sep for a all (x_prev, y_prev).
        rel_dist_x = []
        rel_dist_y = []
        for x in range(-vessel_sep, vessel_sep + 1):
            for y in range(-vessel_sep, vessel_sep + 1):
                if x*x + y*y < vessel_sep*vessel_sep:
                    rel_dist_x.append(x)
                    rel_dist_y.append(y)

        # Dictionary of coordinates that currently fulfills both requirements.
        # (Only the keys in the dictionary are used, not the values.)
        candidates = {}

        j = 0
        while (j < n_vessels - 1):

            # Exclude new candidates and remove old candidates
            # that are too close.
            for dx, dy in zip(rel_dist_x, rel_dist_y):
                x = vessel_x[j]
                y = vessel_y[j]
                if (0 <= x+dx < size[0] and 0 <= y+dy < size[1]):
                    too_close[y+dy, x+dx] = True
                    if (x+dx, y+dy) in candidates:
                        del candidates[(x+dx, y+dy)]

            # Add candidates for new vessel
            for dx, dy in zip(rel_boarder_x, rel_boarder_y):
                x = vessel_x[j]
                y = vessel_y[j]
                if (0 <= x+dx < size[0] and 0 <= y+dy < size[1]
                        and too_close[y+dy, x+dx] == 0):
                    candidates[(x+dx, y+dy)] = True

            if make_video:
                if j == 0:
                    self.video = Vessels.Video(n_vessels, size)
                self.video.add_frame(j, too_close, vessel_x, vessel_y)
                if (j == n_vessels - 2):
                    self.video.make_video()

            # Choose new vessel
            j += 1
            # (Throws exception ValueError if len(candidates) is 0.)
            choice = random.randint(0, len(candidates) - 1)
            new_coord = list(candidates)[choice]
            vessel_x[j] = new_coord[0]
            vessel_y[j] = new_coord[1]
            del candidates[new_coord]

        return (vessel_x, vessel_y)

    class Video:
        import matplotlib.pyplot as plt
        import matplotlib.image as mpimg
        import matplotlib.animation as animation
        import matplotlib.patches as mpatches
        import os as os
        import time as time


        def __init__(self, n_vessels, size):
            self.n_vessels = n_vessels
            self.frame = np.zeros((size[1], size[0]))
            self.video_time = 0

        def add_frame(self, frame_num, too_close, vessel_x, vessel_y):
            start_time = self.time.time()
            self.frame = too_close
            self.frame[vessel_y, vessel_x] = 2
            if self.frame[0, 0] == 2:
                self.frame[0, 0] = 0
            self.plt.matshow(self.frame)
            self.plt.axis('off')
            self.plt.savefig("image_{}.png".format(frame_num))
            self.plt.close()
            self.video_time += self.time.time() - start_time

        def make_video(self):
            start_time = self.time.time()

            def read_img(i):
                img = self.mpimg.imread("image_{}.png".format(i))
                img_plot = self.plt.imshow(img)
                self.plt.axis('off')
                return img_plot,

            fig = self.plt.figure(frameon=False)
            ani = self.animation.FuncAnimation(fig, read_img, frames=self.n_vessels-1, blit=True)
            ani.save('vessel_placement.mp4', fps=5)

            for i in range(0, self.n_vessels - 1):
                self.os.remove("image_{}.png".format(i))

            self.video_time = self.time.time() - start_time
            print("It took {} extra seconds to make the video.".format(self.video_time))

if __name__ == '__main__':
    # Test the initialisation and update of the vessels.
    from model_parts.helpers.mesh import BaseMesh
    from model_parts.drugs_in_vessels import DrugsInVessels
    from model_parts.cells import Cells
    from model_parts.oxygen import Oxygen
    from model_parts.vegf_avastin import VegfAvastin

    n = 50

    # Just to make sure vessel_init_filename and cell_init_filename is in the
    # database.
    sim_database = sim_db.add_empty_sim(False)
    sim_database.write("vessel_init_filename", "empty_vessel_init_file.txt", "string")
    sim_database.write("cell_init_filename", "empty_cell_init_file.txt", "string")
    sim_database.delete_from_database()
    sim_database.close()

    sim_database = sim_db.add_empty_sim(False)
    # DrugsInVessels
    sim_database.write("tot_time", 12, "float")
    sim_database.write("vac_dt_at_injection", 0.01, "float")
    sim_database.write("chemo_dt_at_injection", [0.01, 0.01, 0.01], "float array")
    sim_database.write("vac_max_dt", 10.0, "float")
    sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
    sim_database.write("height", 160, "float")
    sim_database.write("weight", 58.2, "float")
    dose = [600, 100, 600, 15]
    sim_database.write("dose", dose, "float array")
    schedule = [3, 3, 3, 3]
    sim_database.write("schedule", schedule, "float array")
    # BaseMesh
    sim_database.write("size", [n, n], "int array")
    sim_database.write("n_refinements", 2, "int")
    sim_database.write("cell_size", 10.0, "float")
    # Cells
    sim_database.write("seed", 1234, "int")
    sim_database.write("frac_cancer", 0.2, "float")
    sim_database.write("frac_normal", 0.1, "float")
    sim_database.write("n_refinements_cells", 0, "int")
    sim_database.write("t_cell", 14.69, "float")
    sim_database.write("k_phi", 1.4, "float")
    # Vessels
    sim_database.write("n_refinements_vessels", 0, "int")
    sim_database.write("n_other_rngs", 1000, "int")
    sim_database.write("vessel_sep", 3, "int")
    sim_database.write("init_vessel_density", 0.1197, "float")
    sim_database.write("avg_init_vessel_radius", 10.0, "float")
    sim_database.write("first_vessel_random", False, "bool")
    sim_database.write("p_max_sprout", 0.002, "float")
    sim_database.write("v_sprout", 0.5, "float")
    sim_database.write("vegf_low", 1e-6, "float")
    sim_database.write("vegf_high", 1e-4, "float")
    sim_database.write("p_death", 0.001, "float")
    sim_database.write("rel_radius_new_vessels", 1.0, "float")
    # Oxygen
    sim_database.write("degree_oxygen", 1, "int")
    sim_database.write("n_refinements_oxygen", 0, "int")
    sim_database.write("oxygen_dt_after_cell_update",1.0, "float")
    sim_database.write("oxygen_dt_increase_factor_after_injection", 1.05, "float")
    sim_database.write("oxygen_dt_increase_factor_after_cell_update", 1.05, "float")
    sim_database.write("oxygen_max_dt", 10.0, "float")
    # VEGF-Avastin
    sim_database.write("ktrans", 0.14, "float")
    sim_database.write("degree_vac", 1, "int")
    sim_database.write("n_refinements_vac", 0, "int")
    sim_database.write("vac_dt_after_cell_update", 1.0, "float")
    sim_database.write("vac_dt_increase_factor_after_injection", 1.05, "float")
    sim_database.write("vac_dt_increase_factor_after_cell_update", 1.05, "float")
    sim_database.write("vac_max_dt", 10.0, "float")

    base_mesh = BaseMesh(sim_database)
    drugs_in_vessels = DrugsInVessels(sim_database)
    vessels = Vessels(sim_database, base_mesh)
    cells = Cells(sim_database, base_mesh)
    oxygen = Oxygen(sim_database, base_mesh)
    vegf_avastin = VegfAvastin(sim_database, base_mesh, drugs_in_vessels)
    vegf_avastin.update(0, 12*60, vessels, cells, oxygen)

    f = File("figs/vessel_rel_radius.pvd")
    f << vessels.relative_radius.function
    f2 = File("figs/vessel_rel_radius_delta_func.pvd")
    f2 << vessels.rel_radius_delta_function.location.function

    vessels.update(12*60, vegf_avastin)

    f << vessels.relative_radius.function
    f2 << vessels.rel_radius_delta_function.location.function

    sim_database.delete_from_database()
    sim_database.close()
