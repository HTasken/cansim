import __init__
import model_parts.helpers.add_sim_db_to_path
import sim_db

def get_time_last_checkpoint(sim_database):
    """Return time of last checkpoint or 0 if not checkpoint exists."""
    if sim_database.column_exists("time_last_checkpoint"):
        time_last_checkpoint = sim_database.read("time_last_checkpoint")
        if time_last_checkpoint == None:
            time_last_checkpoint = 0
    else:
       time_last_checkpoint = 0

    return time_last_checkpoint

def get_varing_max_dt_after_injection(time, dt_injection, dt_at_injection, increase_factor):
    """Used after checkpoint to set the varing_max_dt_after_injection."""
    t = 0
    varing_dt = dt_at_injection
    time_next_injection = dt_injection
    while t < time:
        if t + varing_dt > time_next_injection:
            t = time_next_injection
            time_next_injection += dt_injection
            varing_dt = dt_at_injection
        else:
            t += varing_dt
            varing_dt *= increase_factor

    return varing_dt
