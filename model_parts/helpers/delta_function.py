import __init__
import numpy as np
import warnings
from model_parts.helpers.refined_function import RefinedFunction

class DeltaFunction:
    """Representing the locations of cells/vessels as delta functions.

    Convert a RefinedFunction instance into a number of delta function for 
    values above 0 at the potential cell/vessel locations.

    Used as sink and sources in the other PDEs.
    
    The delta functions are approximated as square pyramids with a base that 
    decreases with a increasing number of refinements, unless the number of
    total refinement (for DeltaFunction and BaseMesh) is one. In that case the
    delta functions is represented as a six sided, slightly squeezed pyramid.
    """
    def __init__(self, refined_function, base_mesh, n_refinements, multiply=False):
        """Initialise delta function from 'refined_function'.

        Args:
            refined_function (RefinedFunction): Representing the location of 
                the cells or vessel with values greater than 0.5.
            base_mesh (BaseMesh): Need to be the base mesh of 'refined_function'.
            n_refinements (int): Number of extra refinements done on the mesh, 
                where each refinement half the distance between the gridpoints.
            multiply (bool): If true the delta function is multiplied with the
                'refined_function' (also at each update).
        """
        # Issue warning if too few refinements are done on the base mesh to 
        # insure that the volume is 1.
        if base_mesh.n_refinements < 2:
            warnings.warn("Less than 2 refinements are done on the base mesh "
                + "used in a RefinedFunction. This may lead to cell/vessels "
                + "placed on edges where mesh are divided and volumes differ "
                + "from 1 when used as delta functions.")

        self.refined_func = refined_function
        self.location = RefinedFunction(base_mesh, n_refinements, 
                      create_test_function=False, allow_neighbour_access=False)
        self.multiply = multiply
        self.__calc_height_of_delta_function()
        self.update()

    @property
    def function(self):
        return self.location.function

    @property
    def function_space(self):
        return self.location.function_space

    def update(self):
        """Update the location of the refined function it represent.

        Values greater than 0 at the cell/vessel location are turned into 
        delta functions.
        
        The height of each cell is set such that the volume of each cell become one.
        """

        # Find indices containing cells/vessels.
        local_cell_values = self.refined_func.get_local_at_cell_positions()
        local_cell_locations = np.ceil(local_cell_values).astype(int)
        cell_indices = np.arange(0, self.location.n_potential_local_cells, dtype=int)
        self.local_cell_indices = cell_indices[local_cell_locations > 0]

        # Make the height such that the volum becomes one
        # or the value of the 'refined_function' if multiply is True.
        local_cell_locations[local_cell_locations > 0] = 1
        local_cell_locations = local_cell_locations*self.height
        if self.multiply:
            local_cell_locations *= local_cell_values
        
        self.location.set_local_at_cell_positions(local_cell_locations)

    def __calc_height_of_delta_function(self):
        """Calculate the height that give a 'cell' volume one.""" 

        mesh_size = self.location.cell_size/float(2**self.location.n_total_refinements)
        
        # If 'n_total_refinements' is 2, then the square pyramid representing 
        # each cell is turned 90 degrees. 
        if (self.location.n_total_refinements == 2):
            mesh_size /= np.sqrt(2)

        if self.location.n_total_refinements > 0:
            self.height = 3.0/((2*mesh_size)**2)
        else:
            self.height = 1.0/(mesh_size**2)
 
if __name__ == '__main__':
    import numpy
    from mesh import BaseMesh
    import add_sim_db_to_path
    import sim_db
    from dolfin import *
    
    set_log_level(LogLevel.ERROR)

    size = [4, 4]
    seed = 0
    n_refinements = 2

    sim_database = sim_db.add_empty_sim(False)
    sim_database.write('size', size, "int array")
    sim_database.write('n_refinements', n_refinements, "int")
    sim_database.write('cell_size', 10, "float")

    base_mesh = BaseMesh(sim_database)
    rank = MPI.comm_world.Get_rank()
    refined_func = RefinedFunction(base_mesh, 0)
    np.random.seed(seed)
    init_array = np.random.randint(0, 3, size[0]*size[1])
    correct_volume = np.sum(init_array > 0)
    refined_func.set(init_array)
    if rank == 0:
        print("Initialise delta function.")
    delta_func = DeltaFunction(refined_func, base_mesh, 1)
    volume = assemble(delta_func.location.function*dx)

    if rank == 0:
        print( "Volume is {0} and should be {1}.".format(volume, correct_volume))
        assert abs(volume - correct_volume) < 0.001

    init_array = np.random.randint(0, 2, size[0]*size[1])
    correct_volume = np.sum(init_array > 0)
    refined_func.set(init_array)
    if rank == 0:
        print( "Set new values to refined function and update delta function.")
    delta_func.update()
    volume = assemble(delta_func.location.function*dx)
 
    if rank == 0:
        print("Volume is {0} and should be {1}.".format(volume, correct_volume))
        assert abs(volume - correct_volume) < 0.001

    sim_database.delete_from_database()
    sim_database.close()
