import __init__
import model_parts.helpers.add_sim_db_to_path
import sim_db
import warnings
from dolfin import *


class BaseMesh:
    """Mesh intended as basis for all functions."""
    # renameing to Mesh would collide with function in dolfin

    def __init__(self, sim_database):
        """Create a rectangular mesh intended as basis for all functions.
        
        The reason for a common mesh for all functions is so that the value of 
        the function at the cell positions can be retrived without evaluating 
        the function (expensive) and without communication between processes as
        the values are on the same process.

        For using finer meshes for some functions only, 
        `refine(mesh, redistribute=False)` can be used. redistribute must be 
        set to False to avoid the communication across processes. 
        'n_refinements' must also be 2 or larger to avoid possible cell/vessel  
        locations ending up on the edge of the mesh, which then could end on 
        a different processes when refining (even with redistribute=False).

        The potential cell/vessel furthest down and to the left is placed at 
        the coordinates (0, 0).
        """

        # Number of potential cell and vessel positions in x- and y-direction 
        # respecfully the mesh is intended for given av [nx, ny]. 
        self.size = sim_database.read('size')

        # Number of refinements done on the mesh. Recommended to be 2 or 
        # greater to garante that cells are not on the edge of any mesh 
        # divitions.
        self.n_refinements = sim_database.read('n_refinements')


        # The size of the biological cells is given in micrometers.
        self.cell_size = sim_database.read('cell_size') # micrometers

        if self.n_refinements < 2:
            warnings.warn("Less than 2 refinements are done on the base mesh, "
                    "so cells may be on the edge of mesh where it is divited.")

        nx = self.size[0]
        ny = self.size[1]

        # Define the vessels potential placements as a rectangular grid.
        # The placement of origo relative to the mesh depend on 'n_refinements'
        # to avoid having possible cell/vessel locations of the mesh edges, 
        # which are only certain for 'n_refinements' >= 2.
        if self.n_refinements == 0:
            bottom_left = Point(0.0, 0.0)
            upper_right = Point(self.cell_size*(nx - 1), self.cell_size*(ny - 1))
            self.mesh = RectangleMesh(bottom_left, upper_right, nx - 1, ny - 1)
        elif self.n_refinements == 1:
            bottom_left = Point(-self.cell_size*0.5, -self.cell_size*0.5)
            upper_right = Point(self.cell_size*(nx - 0.5), self.cell_size*(ny - 0.5))
            self.mesh = RectangleMesh(bottom_left, upper_right, nx, ny)
        else:
            bottom_left = Point(-self.cell_size*0.25, -self.cell_size*0.5)
            upper_right = Point(self.cell_size*(nx - 0.25), self.cell_size*(ny - 0.5))
            self.mesh = RectangleMesh(bottom_left, upper_right, nx, ny)

        for i in range(self.n_refinements):
            self.mesh = refine(self.mesh, redistribute=False)

if __name__ == '__main__':
    import numpy as np
    n_refinements = 2
    sim_database = sim_db.add_empty_sim(False)
    sim_database.write('size', [3, 3], "int array")
    sim_database.write('n_refinements', n_refinements, "int")
    sim_database.write('cell_size', 10, "float")

    base_mesh = BaseMesh(sim_database)
    sim_database.delete_from_database()
    sim_database.close()
    
    mesh_size = base_mesh.mesh.hmax()/sqrt(2)
    if MPI.comm_world.Get_rank() == 0:
        print("The mesh is missing the coordinate of (0, 0), if it is not " \
               "printed below that is has one (and it should only be printed once).")
    for coord in base_mesh.mesh.coordinates():
        if abs(coord[0]) < mesh_size/2.0 and abs(coord[1]) < mesh_size/2.0:
            print("The mesh have a coordinate of (0, 0).")
    plot(mesh)
    interactive()
