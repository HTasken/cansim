import __init__
import model_parts.helpers.add_sim_db_to_path
import sim_db
from dolfin import *
import numpy as np

def calc_init_num_vessels(sim_database):
    size = sim_database.read('size')
    cell_size = sim_database.read('cell_size')
    volume = size[0]*size[1]*cell_size*cell_size*cell_size
    vessel_length = cell_size
    init_num_vessels = (sim_database.read('init_vessel_density')*volume
        /(np.pi*sim_database.read('avg_init_vessel_radius')**2*vessel_length))
    
    return Constant(init_num_vessels)

if __name__ == '__main__':
    sim_database = sim_db.add_empty_sim(False)
    sim_database.write('size', [10, 10], 'int array')
    sim_database.write('cell_size', 10.0, 'float')
    sim_database.write('init_vessel_density', 0.1197, 'float') 
    sim_database.write('avg_init_vessel_radius', 10.0, 'float') 
    print(float(calc_init_num_vessels(sim_database)))
     
