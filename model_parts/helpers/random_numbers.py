import __init__
from model_parts.helpers.pcg.simple_pcg import SimplePCG

def random_number_generator_state(x, y, nx, grid_size,  offset_n_generators, seed):
    """Random number generator state uniq to and only dependent on parameters.

    For generating random numbers independently of how FEniCS divied the mesh.

    The sequence of random number is not overlapping for different parameters,
    as long as each number generator does not generate more than
    `max_numbers_generated_per_generator` and `offset_n_generators` is set
    correctly.

    PCG is used as the number generator instead of mt19937, which is the
    standard PRNG in random and numpy.random, since it have a state with such a
    huge size. This is a problem because a each cell and vessel have its own
    state. The PCG generator is small and is statistically good.
    [http://www.pcg-random.org/pdf/hmc-cs-2014-0905.pdf]

    If moved to another machine the c++ implementation and the swig wrapper
    might have to be recompiled, by `running pcg/build.sh`.

    Args:
        x (float): X-coordinate of a gridpoint.
        y (float): Y-coordinate of a gridpoint.
        nx (int): Number of gridpoints in x-direction.
        grid_size (float): Distance between gridpoints.
        offset_n_generators (int): Generator will be advance the number of
            steps equivalently to offset_n_generators in addition to the steps
            advanced because of the coordinates. Allow for multiple generators at same
            coordinates. Should be equal to the sum of nx*ny for a possible
            second generator at a coordinate.
        seed (int): Same seed should be used for all calls as the other
            parameters is used to advance the number generator a uniq and
            sufficent number of steps.
    Return:
        A instance of the SimplePCG class with a state uniq to the parameters.
    """

    # Variable used to make the sequences of random numbers non-overlapping.
    max_numbers_generated_per_generator = 1000

    rng = SimplePCG(seed)

    # Convert coordinates of gridpoint to its ordered number in each direction.
    ix = int(round(x/grid_size))
    iy = int(round(y/grid_size))

    # Jump a number of steps forward uniq to the parameters and spaced
    # spaced sufficently out to non-overlapping sequences of random numbers.
    n_steps = (nx*iy + ix + offset_n_generators)*max_numbers_generated_per_generator
    if n_steps < 4e9: # make sure not to jump longer that a c type long
        rng.advance(int(n_steps))
    else:
        
        for i in range(max_numbers_generated_per_generator):
            rng.advance(int(nx*iy + ix + offset_n_generators))
    return rng

if __name__ == '__main__':
    correct = True

    # Check that different parameters give different states and equal
    # parameters give the same.
    rng1 = random_number_generator_state(1, 1, 3, 1, 0, 1234)
    rng2 = random_number_generator_state(1, 1, 3, 1, 0, 1234)
    rng3 = random_number_generator_state(0, 1, 3, 1, 0, 1234)
    rng4 = random_number_generator_state(1, 1, 3, 1, 9, 1234)
    i11 = rng1.rand_uniform_long()
    i12 = rng2.rand_uniform_long()
    i13 = rng3.rand_uniform_long()
    i14 = rng4.rand_uniform_long()
    i21 = rng1.rand_uniform_long()
    if (i11 != i12 or i11 == i13 or i11 == i14 or i11 == i21):
        correct = False

    # Check that a distribution can be set.
    rng1.set_uniform_double(10.1, 10.5)
    for i in range(10):
        rand = rng1.rand_uniform_double()
        if (rand < 10.1 or rand > 10.5):
            correct = False
    
    # Check that advancing does not crash
    rng1.advance(1000000)

    if correct:
        print("random_number_generator_state seem to work")
    else:
        print("random_number_generator_state did NOT work!")

