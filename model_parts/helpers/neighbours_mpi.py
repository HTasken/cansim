from dolfin import *
import numpy as np

class LocalAndNeighbours:
    """Used to get and set the values at local or neighbour cells locations.

    Any of the local or eight neighbour vertices can be returned or set even
    for values at different processes.
    collect_values must be called before this happens and return_values must be
    called after.
    """
    def __init__(self, V, n_intermediate, n_vector_dim=1):
        """Calculate and set up what is need to communicate with neighbours.

        Args:
            V: FunctionSpace or VectorFunctionSpace on a rectangular mesh with 
               non-negative coordinates for the (biological) cells, a cell in 
               origo (0,0) and equal spacing in x- and y-direction.
            n_intermediate (int): Number of gridpoints between cells.
            n_vector_dim (int): Number of dimensions in the vector space if 
                                'V' is a VectorFunctionSpace, 1 otherwise.
        """
        # ALL INTERNAL COORDINATES ARE SCALED TO (SMALLEST POSSIBLE) INTEGERS
        # TO BE USED AS INDICES
        self.comm = MPI.comm_world

        self.dim = n_vector_dim

        # Make a map of the needed communication
        all_neighbour_vertices_map = AllNeighbourVerticesMap(V, n_intermediate)
        self.scale = all_neighbour_vertices_map.scale
        self.coord_recv_vecs = all_neighbour_vertices_map.coord_recv_vecs
        self.local_indices_to_send = all_neighbour_vertices_map.local_indices_to_send
        self.rank = all_neighbour_vertices_map.rank
        self.n_processes = all_neighbour_vertices_map.n_processes
        self.x_cell_local_scaled = all_neighbour_vertices_map.x_cell_local_scaled
        self.y_cell_local_scaled = all_neighbour_vertices_map.y_cell_local_scaled
        self.local_size = all_neighbour_vertices_map.local_size # Include intermediate gridpoints
        self.local_size_scaled = all_neighbour_vertices_map.local_size_scaled # Include just cells
        self.remove_local_non_cell_map = all_neighbour_vertices_map.remove_local_non_cell_map
        
        # Calculate the minimum size and offset of the matrices holding local
        # and neighbour values
        if self.x_cell_local_scaled.size == 0:
            min_x = 0
            min_y = 0
            max_x = 0
            max_y = 0
        else:
            min_x = self.x_cell_local_scaled.min()
            min_y = self.y_cell_local_scaled.min()
            max_x = self.x_cell_local_scaled.max()
            max_y = self.y_cell_local_scaled.max()
        for i in range(self.n_processes):
            for coord in self.coord_recv_vecs[self.rank][i]:
                if coord[0] < min_x:
                    min_x = coord[0]
                if coord[1] < min_y:
                    min_y = coord[1]
                if coord[0] > max_x:
                    max_x = coord[0]
                if coord[1] > max_y:
                    max_y = coord[1]
        self.nx = max_x - min_x + 1
        self.ny = max_y - min_y + 1
        self.offset_x = min_x
        self.offset_y = min_y

        # Matrices holding the local and neighbour values and info to solve
        # conflicts of same possisions set by different processes.
        self.values = np.ones((self.ny, self.nx, self.dim), dtype=float)*(-1)
        self.changed_by = np.empty((self.ny, self.nx), dtype=int)
        self.indices_changer = np.empty((self.ny, self.nx), dtype=int)
        self.priorities = np.zeros((self.ny, self.nx), dtype=int)
        self.identifiers = np.zeros((self.ny, self.nx), dtype=int)

        # Return value of return_values()
        self.coord_and_identifiers_not_set = []

        # Length of the vectors received from and sent to each process
        self.received_sizes = np.empty(self.n_processes, dtype=int)
        self.sent_sizes = np.empty(self.n_processes, dtype=int)

        self.received_values = [[] for i in range(self.n_processes)]
        self.received_priorities = [[] for i in range(self.n_processes)]
        self.returned_values = [[] for i in range(self.n_processes)]
        self.returned_priorities = [[] for i in range(self.n_processes)]

        # Is the values returned back from other processes set?
        # The corresponding values are owned by this process
        self.is_returned_set = [[] for i in range(self.n_processes)]

        # Is the changed values sent back to the owner processes set?
        # The corresponding values are owned by other processes
        self.is_change_set = [[] for i in range(self.n_processes)]
        for i in range(self.n_processes):
            self.received_sizes[i] = self.dim*len(self.coord_recv_vecs[self.rank][i])
            self.sent_sizes[i] = self.dim*len(self.coord_recv_vecs[i][self.rank])
            self.received_values[i] = np.empty(self.received_sizes[i], dtype=float)
            self.received_priorities[i] = np.zeros(self.received_sizes[i], dtype=int)
            self.returned_values[i] = np.empty(self.sent_sizes[i], dtype=float)
            self.returned_priorities[i] = np.zeros(self.sent_sizes[i], dtype=int)
            self.is_returned_set[i] = np.empty(self.sent_sizes[i], dtype=bool)
            self.is_change_set[i] = np.empty(self.received_sizes[i], dtype=bool)

    def collect_values(self, function):
        """Collect the local values and neighbour values from other processes.

        Args:
            function: Function on the same FunctionSpace used to create the
                      all_neighor_vertices_map the class was initiated with.
        """
        # ALL INTERNAL COORDINATES ARE SCALED TO (SMALLEST POSSIBLE) INTEGERS
        # TO BE USED AS INDICES

        self.original_local_values = function.vector().get_local()
        local_values = self.original_local_values[self.remove_local_non_cell_map]
        requests = [None] * self.n_processes

        # Sent the neccessary values between processes.
        for i in range(self.n_processes):
            if (self.sent_sizes[i] > 0):
                self.comm.Isend(local_values[self.local_indices_to_send[i]], dest=i, tag=88)
            if (self.received_sizes[i] > 0):
                requests[i] = self.comm.Irecv(self.received_values[i] , source=i, tag=88)
        for i in range(self.n_processes):
            if (self.received_sizes[i] > 0):
                requests[i].wait()

        # Fill in the recived values by coordinates
        for i in range(self.n_processes):
            j = 0
            for coord in self.coord_recv_vecs[self.rank][i]:
                if (self.received_sizes[i] != 0):
                    y = coord[1] - self.offset_y
                    x = coord[0] - self.offset_x
                    for d in range(self.dim):
                        self.values[y, x, d] = self.received_values[i][j]
                        j = j +1

        # Fill in the local values by coordinates
        d = 0
        for value, x, y in zip(local_values, self.x_cell_local_scaled, 
                               self.y_cell_local_scaled):
            self.values[y - self.offset_y, x - self.offset_x, d] = value
            d = (d + 1) % self.dim

    def get_value_at(self, x, y):
        """Get value at (x,y)
        Args:
            x (float): x-coordinate of a vertex
            y (float): y-coordinate of a vertex
        """
        x = int(round(x*self.scale))
        y = int(round(y*self.scale))
        return_value = []
        for d in range(self.dim):
            return_value.append(self.values[y - self.offset_y, 
                                            x - self.offset_x, d])
        if self.dim == 1: 
            return return_value[0]
        else:
            return tuple(return_value)

    def set_value_at(self, x, y, value, priority, identifier=0):
        """Set value at (x,y) with a priority for conflicts across processes.

        Args:
            x (float): x-coordinate of a vertex
            y (float): y-coordinate of a vertex
            value (float or tuple(float)): 
            priority (int): For conflicts across processes the value with
                            highest priority is set. Values with priorty 0 is
                            not set.
            identifier (int): return_values() return this number if not set.
        """
        original_coord = (x, y)
        x = int(round(x*self.scale))
        y = int(round(y*self.scale))

        # If return_value() is called with reset_priorities set to False, the
        # priorities will be set to there negative values to distinguish 
        # values that have already tried to be set.
        prev_priority = abs(self.priorities[y - self.offset_y, x - self.offset_x])
        if priority > prev_priority:
            if (prev_priority > 0 
                    and self.changed_by[y - self.offset_y, x - self.offset_x] 
                        == self.rank):
                prev_identifier = self.identifiers[y - self.offset_y, x - self.offset_x]
                self.coord_and_identifiers_not_set.append((original_coord, prev_identifier))
            self.changed_by[y - self.offset_y, x - self.offset_x] = self.rank
            self.priorities[y - self.offset_y, x - self.offset_x] = priority
            self.identifiers[y - self.offset_y, x - self.offset_x] = identifier
            if self.dim == 1:
                self.values[y - self.offset_y, x - self.offset_x, 0] = value
            else:
                for d in range(self.dim):
                    self.values[y - self.offset_y, x - self.offset_x, d] = value[d]
        else:
            if priority > 0:
                self.coord_and_identifiers_not_set.append((original_coord, identifier))
            
    def return_values(self, function, reset_priorities=True):
        """Return new values to the owner rank and inform about values not set.

        Args:
            function: Function on the same FunctionSpace used to create the
                all_neighor_vertices_map the class was initiated with.
            reset_priorities_and_return_value (bool): If set to False the 
                current priorities of the values are kept and compared against
                the priority of new values set. 

        Returns:
            List of tuples containing the coordinates for the vertices that the 
            process tried to set, but was set by another process instead and 
            the identifier provided when the value was set.
        """
        # ALL INTERNAL COORDINATES ARE SCALED TO (SMALLEST POSSIBLE) INTEGERS
        # TO BE USED AS INDICES

        # Reset what values are set.
        for i in range(len(self.is_returned_set)):
            self.is_returned_set[i] = np.ones(len(self.is_returned_set[i]), dtype=bool)

        # Find the values and priorities owned by other processes and prepare
        # to send them back.
        for i in range(self.n_processes):
            j = 0
            for coord in self.coord_recv_vecs[self.rank][i]:
                if (self.received_sizes[i] != 0):
                    y = coord[1] - self.offset_y
                    x = coord[0] - self.offset_x
                    self.received_values[i][j] = self.values[y, x, j % self.dim]
                    self.received_priorities[i][j] = self.priorities[y, x]
                    j = j + 1
        requests_values = [None] * self.n_processes
        requests_priorities = [None] * self.n_processes

        # Sent the updated values and priorities back to the owner rank
        for i in range(self.n_processes):
            if (self.received_sizes[i] > 0):
                self.comm.Isend(self.received_values[i], dest=i, tag=77)
                self.comm.Isend(self.received_priorities[i], dest=i, tag=66)
            if (self.sent_sizes[i] > 0):
                requests_values[i] = self.comm.Irecv(self.returned_values[i] , 
                                                     source=i, tag=77)
                requests_priorities[i] = self.comm.Irecv(
                        self.returned_priorities[i], source=i, tag=66)
        for i in range(self.n_processes):
            if (self.sent_sizes[i] > 0):
                requests_values[i].wait()
                requests_priorities[i].wait()

        # Insert the changes from other processes and set self.is_returned_set
        for i in range(self.n_processes):
            j = 0
            for coord in self.coord_recv_vecs[i][self.rank]:
                if (self.sent_sizes[i] != 0):
                    y = coord[1] - self.offset_y
                    x = coord[0] - self.offset_x
                    if (self.returned_priorities[i][j] > 0):
                        self.__insert_changes_from_other_nodes(i, j, x, y)
                    j = j + 1

        # Set the function equal to the new values
        for i, (x, y) in enumerate(zip(self.x_cell_local_scaled, self.y_cell_local_scaled)):
            for d in range(self.dim):
                self.original_local_values[self.remove_local_non_cell_map[i]] \
                    = self.values[y - self.offset_y, x - self.offset_x, d]
        function.vector().set_local(self.original_local_values)

        # Sent back which changes that was suggested by the other processes, but not set.
        requests_is_set = [None] * self.n_processes
        for i in range(self.n_processes):
            if (self.sent_sizes[i] > 0):
                self.comm.Isend(self.is_returned_set[i], dest=i, tag=55)
            if (self.received_sizes[i] > 0):
                requests_is_set[i] = self.comm.Irecv(self.is_change_set[i], source=i, tag=55)
        for i in range(self.n_processes):
            if (self.received_sizes[i] > 0):
                requests_is_set[i].wait()

        # Add the coordinates of the values that was suggested changed by the
        # process, but not performed, and return there coordinates.
        for i in range(self.n_processes):
            for is_placed, coord in zip(self.is_change_set[i], 
                                        self.coord_recv_vecs[self.rank][i]):
                if (not is_placed):
                    self.coord_and_identifiers_not_set.append((
                        (coord[0]/self.scale, coord[1]/self.scale), 
                        self.identifiers[coord[1] - self.offset_y, 
                        coord[0] - self.offset_x]))

        # Reset priorities if 'reset_priorities' is set to True, otherwise 
        # the priorities will be set to there negative values with same 
        # absolute value. This way they can be values that have already been 
        # tried to be set can be distinguished from newly set values.
        if reset_priorities:
            self.reset_priorities()
        else: 
            for y in range(self.ny):
                for x in range(self.nx):
                    if self.priorities[y, x] > 0:
                        self.priorities[y, x] *= -1

        # Return coordiantes and identifiers of values not set.
        return_value = self.coord_and_identifiers_not_set
        self.coord_and_identifiers_not_set = []

        return return_value

    def reset_priorities(self):
        """Stop new values set from competing against old priorities."""
        self.priorities = np.zeros((self.ny, self.nx), dtype=int)
        
    def __insert_changes_from_other_nodes(self, i, j, x, y):
        if (self.returned_priorities[i][j] <= abs(self.priorities[y, x])):
            x_return = (x + self.offset_x)/self.scale
            y_return = (y + self.offset_y)/self.scale
            self.is_returned_set[i][j] = False
        else:
            if (self.changed_by[y, x] == self.rank):
                x_return = (x + self.offset_x)/self.scale
                y_return = (y + self.offset_y)/self.scale
                identifier = self.identifiers[y, x]
                self.coord_and_identifiers_not_set.append(((x_return, y_return), identifier))
            elif (abs(self.priorities[y, x]) > 0):
                x_return = (x + self.offset_x)/self.scale
                y_return = (y + self.offset_y)/self.scale
                self.is_returned_set[self.changed_by[y, x]][self.indices_changer[y, x]] = False
            self.values[y, x, j % self.dim] = self.returned_values[i][j]
            self.changed_by[y, x] = i
            self.indices_changer[y, x] = j
            self.priorities[y, x] = self.returned_priorities[i][j]
            self.is_returned_set[i][j] = True

class AllNeighbourVerticesMap:
    """Map of communication between neighbour cells on different processes.

    Contain a map over which local values should be sent to which process
    (rank), so that every cell has the values over all its eight possible
    neighbour cells in a rectangular mesh. It also contains the coordinates of 
    these sent values.
    """
    def __init__(self, V, n_intermediate):
        """Make the maps mentioned above.

        Args:
            V: FunctionSpace or VectorFunctionSpace on a rectangular mesh with 
               non-negative coordinates for the (biological) cells, a cell in 
               origo (0,0) and equal spacing in x- and y-direction.
            n_intermediate (int): Number of gridpoints between cells.
        """

        self.comm = MPI.comm_world
        self.rank = self.comm.Get_rank()
        self.n_processes = self.comm.Get_size()

        # Create a local map that remove gridpoints that can not contain a cell/vessel.
        x_local = V.tabulate_dof_coordinates()[:, 0] # syntax for dolfin 2018.1.0
        y_local = V.tabulate_dof_coordinates()[:, 1] # syntax for dolfin 2018.1.0
        self.local_size = len(x_local)
        self.mesh_size = V.mesh().hmax()/sqrt(2)
        self.cell_size = self.mesh_size*(n_intermediate + 1)
        is_x_at_cell = np.any([x_local % self.cell_size < self.mesh_size/2.0, \
            x_local % self.cell_size > self.cell_size - self.mesh_size/2.0], axis=0)
        is_y_at_cell = np.any([y_local % self.cell_size < self.mesh_size/2.0, \
            y_local % self.cell_size > self.cell_size - self.mesh_size/2.0], axis=0)
        is_coord_at_cell = np.all([is_x_at_cell, is_y_at_cell], axis=0)
        is_coord_positive = np.all([x_local > -self.mesh_size/2.0, \
                                    y_local > - self.mesh_size/2.0], axis=0)
        is_cell = np.all([is_coord_at_cell, is_coord_positive], axis=0)
        self.remove_local_non_cell_map = np.arange(self.local_size)[is_cell]
        self.local_size_scaled = self.remove_local_non_cell_map.size

        # Scale the coordinates of the possible cell positions to smallest 
        # possible integers.
        self.scale = sqrt(2)/V.mesh().hmax()/(n_intermediate + 1.0)
        self.x_cell_local_scaled = x_local[self.remove_local_non_cell_map]*self.scale
        self.y_cell_local_scaled = y_local[self.remove_local_non_cell_map]*self.scale
        self.x_cell_local_scaled = np.rint(self.x_cell_local_scaled).astype(int)
        self.y_cell_local_scaled = np.rint(self.y_cell_local_scaled).astype(int)

        # Collect the scaled coordinates of possible cell positions from every process at rank 0
        recv_x = self.comm.gather(self.x_cell_local_scaled, root=0)
        recv_y = self.comm.gather(self.y_cell_local_scaled, root=0)

        # Coordinates of the vectors received from the other nodes.
        # coord_recv_vecs[i, j, k] contains the coordinates for the k-th
        # entry in the vector from j to i.
        self.coord_recv_vecs = None
        self.coord_to_owner_rank = None
        if self.rank == 0:
            self.__coordinates_of_received_vectors(recv_x, recv_y)
        self.comm.barrier()
        
        # Sent self.coord_recv_vecs to all processes
        self.coord_recv_vecs = self.comm.bcast(self.coord_recv_vecs, root=0)

        # self.local_indices_to_send[i] is all the local indices that should be
        # sent to rank i
        self.local_indices_to_send = [[] for i in range(self.n_processes)]
        for j in range(self.n_processes):
            for recv_coord in self.coord_recv_vecs[j][self.rank]:
                for i in range(self.local_size_scaled):
                    if (self.x_cell_local_scaled[i] == recv_coord[0]
                        and self.y_cell_local_scaled[i] == recv_coord[1]):
                        self.local_indices_to_send[j].append(i)

    def __coordinates_of_received_vectors(self, recv_x, recv_y):
        """Fill in the coordinates of self.coord_recv_vecs"""
        # ALL INTERNAL COORDINATES ARE SCALED TO (SMALLEST POSSIBLE) INTEGERS
        # TO BE USED AS INDICES
    
        # rank_list[i] contain rank that own cell at (x[i], y[i]) after created
        rank_list = []
        x = []
        y = []
        for i in range(self.n_processes):
            rank_list = rank_list + [i]*recv_x[i].size
            x = x + list(recv_x[i])
            y = y + list(recv_y[i])
        if len(x) > 0:
            nx = max(x) + 1
            ny = max(y) + 1
        else:
            nx = 0
            ny = 0

        # self.coord_to_owner_rank[y, x] contain the rank that own cell at (x,y)
        self.coord_to_owner_rank = np.empty((ny, nx), dtype=int)
        for i in range(len(x)):
            self.coord_to_owner_rank[y[i], x[i]] = rank_list[i]

        # Fill Coordinates of self.coord_recv_vecs
        self.coord_recv_vecs = [[set() for i in range(self.n_processes)] \
                                for j in range(self.n_processes)]
        for j in range(0, ny):
            for i in range(0, nx):
                self.__add_coordinates_for_point(i, j)

    def __add_coordinates_for_point(self, x, y):
        # ALL INTERNAL COORDINATES ARE SCALED TO (SMALLEST POSSIBLE) INTEGERS
        # TO BE USED AS INDICES

        (ny, nx) = np.shape(self.coord_to_owner_rank)
        curr_rank = self.coord_to_owner_rank[y, x]

        # Iterate over each neighbour and add coordinates if the neighbour rand
        # is different.
        for k in [(0, 1), (1,-1), (1,0), (1,1)]:
            if (0 <= (y + k[1]) < ny and 0 <= (x + k[0]) < nx):
                neighbour_rank = self.coord_to_owner_rank[y+k[1]][x+k[0]]
                if curr_rank != neighbour_rank:
                    self.coord_recv_vecs[neighbour_rank][curr_rank].add((x,y))
                    self.coord_recv_vecs[curr_rank][neighbour_rank].add((x+k[0],y+k[1]))

if __name__ == "__main__":
    # This test must be run with 'mpirun -n 4 python neighbour_mpi.py' to work
    # and is NOT guaranteed to work, as it relies on that the coordinates of 
    # the neighbours get the following mpi rank: [[3 3 1 1]
    #                                             [3 3 0 1]
    #                                             [2 3 0 0]
    #                                             [2 2 0 0]]

    # All the 4 processes print the value at (1.0, 1.0), then set if its value 
    # to minus the process's rank with the rank as the priority as well. Each
    # process then prints the new local value, and the coordinates of the
    # values that was not set globally. Finally each process prints its updated
    # values and check that it is correct.

    # Set true for nice output, set false to check that it works without the barriers
    barrier_on = True

    n_refinements = 1 # 1 and 2 are valid choices.

    def barrier():
        if barrier_on:
            comm.Barrier()

    mesh = RectangleMesh(Point(-0.5,-0.5), Point(3.5,3.5), 4, 4) 

    for i in range(n_refinements):
        mesh = refine(mesh, redistribute=False)

    V = FunctionSpace(mesh, 'CG', 1)
    u = interpolate(Expression('2*x[1]+1+20*x[0]+10', element=V.ufl_element()), V)

    initial_local_values = u.vector().get_local()

    comm = MPI.comm_world
    rank = comm.Get_rank()
    n_processes = comm.Get_size()

    n_intermediate = 2**n_refinements - 1
    local_and_neighbours = LocalAndNeighbours(V, n_intermediate)

    # Print map of the owner rank of each coordinate. (Only rank 0 have the map.)
    neighbour_map = AllNeighbourVerticesMap(V, n_intermediate)
    if rank == 0:
        print( "Map over which rank that owns each vertex for the possible cell "\
              + "locations:")
        print(neighbour_map.coord_to_owner_rank)
        print("Values before they are set:")

    local_and_neighbours.collect_values(u)

    barrier()
    print("Rank", rank, "at (1,1):", local_and_neighbours.get_value_at(1,1))
    barrier()
    if rank == 0:
        print( "Values after they are set:")
    barrier()
    local_and_neighbours.set_value_at(1,1,-rank, rank + 1)
    print("Rank {} at (1,1): {}".format(rank, local_and_neighbours.get_value_at(1,1)))
    barrier()

    coord_of_not_set = local_and_neighbours.return_values(u)
    local_and_neighbours.collect_values(u)

    print("Rank {} had these coordinates not set:".format(rank, coord_of_not_set))
    barrier()
    if rank != 3:
        if len(coord_of_not_set) == 1 and coord_of_not_set[0][0] == (1,1):
            print("Rank", rank, "returned correct coordinated for values not set.")
        else:
            print("Rank", rank, "returned INCORRECT coordinates for values not set.")
    else:
        if len(coord_of_not_set) == 0:
            print("Rank", rank, "returned correct coordinated for values not set.")
        else:
            print("Rank", rank, "returned INCORRECT coordinates for values not set.", \
                "Have you remembered to run with: `mpirun -n 4 python neighbour_mpi.py`?", \
                "Have you check that FEniCS divied the mesh as required for this test?")
    barrier()
    try:
        if rank == 0:
            print("Rank", rank, "had these local values originally:")
            print(initial_local_values)
            comm.send("start", dest=1)
        else:
            comm.recv(source=rank-1)
            print("Rank", rank, "had these local values originally:")
            print(initial_local_values)
            if rank < n_processes-1:
                comm.send("start", dest=rank+1)
        barrier()
        if rank == 0:
            print("Rank", rank, "have these values now:")
            print(u.vector().get_local())
            comm.send("start", dest=1)
        else:
            comm.recv(source=rank-1)
            print("Rank", rank, "have these values now:")
            print(u.vector().get_local())
            if rank < n_processes-1:
                comm.send("start", dest=rank+1)
    except Exception as err:
        print("ERROR: {}".format(err))
        print("Example must be run with `mpirun -n 4 python neighbour_mpi.py`.")
        exit(1)
    barrier()

    if rank == 3:
        if n_refinements == 1:
            initial_local_values[13] = -3
        elif n_refinements == 2:
            initial_local_values[43] = -3
        else:
            print("ERROR: Invalid value of n_refinements. The correct result, "\
                  "is not added for n_refinements = {}.".format(n_refinements))
    correct_result = np.rint(initial_local_values).astype(int)
    actual_result = np.rint(u.vector().get_local()).astype(int)
    if np.array_equal(correct_result, actual_result):
        print("Rank", rank, "have the correct values.")
    else:
        print("Rank", rank, "have INCORRECT values.", \
            "Have you remembered to run with: `mpirun -n 4 python neighbour_mpi.py`?", \
            "Have you check that FEniCS divied the mesh as required for this test?")
