#!/bin/sh

swig -python -py3 -c++ -Isrc simple_pcg.i

g++ -std=c++11 -Isrc -fPIC $(pkg-config --cflags --libs python3) -c simple_pcg.cpp simple_pcg_wrap.cxx
g++ -shared -fPIC -o _simple_pcg.so simple_pcg.o simple_pcg_wrap.o

python -c "import simple_pcg" # test
