
/* file: helloworld.i */
%module simple_pcg
%{
/* include C++ header files necessary to compile the interface */
#include "simple_pcg.hpp"
%}

%include "simple_pcg.hpp"
