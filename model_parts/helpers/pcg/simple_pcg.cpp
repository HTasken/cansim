#include "simple_pcg.hpp"
#include "pcg_random.hpp"
#include <random>

SimplePCG::SimplePCG(long seed): rng(seed){};

long SimplePCG::rand_uniform_long(){return uniform_long(rng);};

void SimplePCG::set_uniform_long(long lower, long upper){
    uniform_long = std::uniform_int_distribution<long>(lower, upper);
};
 
double SimplePCG::rand_uniform_double(){return uniform_double(rng);};

void SimplePCG::set_uniform_double(double lower, double upper){
    uniform_double = std::uniform_real_distribution<double>(lower, upper);
};

int SimplePCG::rand_binomial_int(){return binomial_int(rng);};

void SimplePCG::set_binomial_int(int n_trials, double probability){
    binomial_int = std::binomial_distribution<int>(n_trials, probability);
}

void SimplePCG::advance(long n_steps){rng.advance(n_steps);};
