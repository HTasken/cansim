#include "pcg_random.hpp"
#include <random>

class SimplePCG{
    public:
        SimplePCG(long seed);
        long rand_uniform_long();
        void set_uniform_long(long lower, long upper);
        double rand_uniform_double();
        void set_uniform_double(double lower, double upper);
        int rand_binomial_int();
        void set_binomial_int(int n_trials, double probability);
        void advance(long n_steps); // long long not well supported
    private:
        pcg32 rng;
        std::uniform_int_distribution<long> uniform_long;
        std::uniform_real_distribution<double> uniform_double;        
        std::binomial_distribution<int> binomial_int;
};
        
