import __init__
import model_parts.helpers.add_sim_db_to_path
import sim_db
from dolfin import *
import os.path

def load_function_if_checkpoint(sim_database, function, name):
    """Conditionally load function 'name' in 'results_dir'/functions.hdf5.

    Function is loaded in there exists a previous checkpoint, that is if 
    'n_checkpoints' exists in the database and is greater than one.

    'results_dir' and 'n_checkpoints' are read from 'sim_database'.

    Return True if function is loaded from checkpoint, False otherwise.
    """
    if not sim_database.column_exists("n_checkpoints"):
        return False

    n_checkpoints = sim_database.read("n_checkpoints")
    if n_checkpoints == None or n_checkpoints < 1:
        return False

    results_dir = sim_database.read("results_dir")
    filename = os.path.join(results_dir, "functions.hdf5")
    hdf5_file = HDF5File(MPI.comm_world, filename, "r")
    hdf5_file.read(function, "{0}/vector_{1}".format(name, n_checkpoints - 1))

    return True

if __name__ == '__main__':
    n = 5

    mesh = UnitSquareMesh(n, n)
    V = FunctionSpace(mesh, 'CG', 1)
    e = Expression("100*x[1] + x[0]", degree = 1)
    original_func = interpolate(e, V)

    f = HDF5File(MPI.comm_world, "functions.hdf5", "w")
    f.write(original_func, "test_func", 0.0)
    f.close()

    sim_database = sim_db.add_empty_sim(False)
    sim_database.write("results_dir", "{0}".format(os.getcwd()), "string")


    loaded_func = Function(V)
    is_loaded = load_function_if_checkpoint(sim_database, loaded_func, "test_func")
    print("Did load before writing to 'n_checkpoints': ", is_loaded)
    sim_database.write("n_checkpoints", 1, "int")
    is_loaded = load_function_if_checkpoint(sim_database, loaded_func, "test_func")
    print("Did load after writing to 'n_checkpoints': ", is_loaded)

    print("L2 Error Norm: ", errornorm(loaded_func, original_func))

    sim_database.delete_from_database()
    sim_database.close()
    os.remove("{0}/functions.hdf5".format(os.getcwd()))
