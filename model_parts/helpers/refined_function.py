import __init__
import numpy as np
import warnings
from model_parts.helpers.mesh import BaseMesh
from model_parts.helpers.neighbours_mpi import LocalAndNeighbours
from dolfin import *

class RefinedFunction:
    """Class allowing for a function on a finer mesh.

    Values at the potential cell/vessel locations can easily be accessed, 
    including neighbour values if spesified at initialisation.
    Equivalent test and trial functions can also be included.
    """

    def __init__(self,
                 base_mesh,
                 n_refinements,
                 degree=1,
                 dim=1,
                 create_test_function=True,
                 create_trial_function=False,
                 create_prev_function=True,
                 allow_neighbour_access=True):
        """Create a function on a refined mesh.

        Args:
            base_mesh (Mesh): Must be refined at least once and the location of the 
                potential cell/vessel furthest down and to the left is at 
                coordinate (0,0). 
            n_refinements (int): Number of extra refinements made on the mesh.
                Can only be greater than zero if number of refinements done on
                base_mesh.mesh is 2 or greater.
            degree (int): The created functions is of family 'CG' and has 
                degree 'degree'.
            dim (int): Dimension of the functions space. (>1 => vector func space)
            create_test_function (bool): set False if test function is not 
                needed in the problem.
            create_trial_function (bool): set True if trial function is needed 
                in the problem
            create_prev_function (bool): set True if the variable 'prev_function'
                should be available to store the previous function in.
            allow_neighbour_access (bool): set True if it is needed to access 
                (set or get) neighbour values, where neighbour are the 6 closest
                potential cell/vessel positions.
        """
        self.rank = MPI.comm_world.Get_rank()
        self.degree = degree
        self.allow_neighbour_access = allow_neighbour_access
        self.n_total_refinements = n_refinements + base_mesh.n_refinements
        self.size = base_mesh.size # Number of potential cells in each direction.
        self.cell_size = base_mesh.cell_size

        # Check for correct input of 'n_refinements'.
        if n_refinements > 0 and base_mesh.n_refinements < 2:
            raise UserWarning("'n_refinements' can NOT be greater than zero, if "\
                + "the number of refinements on the base mesh is less than two.")

        # (redistribute is set to False to keep the new gridpoints on the same
        # process)
        mesh = base_mesh.mesh
        for i in range(n_refinements):
            mesh = refine(mesh, redistribute=False)

        # Create the function space and functions
        if (dim == 1):
            self.function_space = FunctionSpace(mesh, 'CG', degree)
        else:
            self.function_space = VectorFunctionSpace(mesh, 'CG', degree, dim=dim)
        self.function = Function(self.function_space)
        if create_test_function:
            self.test_function = TestFunction(self.function_space)
        if create_trial_function:
            self.trial_function = TrialFunction(self.function_space)
        if create_prev_function:
            self.prev_function = Function(self.function_space)

        # Create map from function values to the local position of the possible
        # cell/vessel positions.
        # Does also create vectors of the x and y coordinates of these positions.
        self.__make_map_to_local_cell_positions()

        if self.allow_neighbour_access:
            n_intermediate = 2**self.n_total_refinements - 1
            self.local_and_neighbours = LocalAndNeighbours(self.function_space,
                                                           n_intermediate, dim)
    def __make_map_to_local_cell_positions(self):
        """Map from refined mesh to a special order of potential cell/vessel locations.

        Map local values of a function defines on a refined mesh to positions of 
        potential cells/vessels ordered row-major.

        Does also create vectors for the local x and y coordinates of the 
        potential cell/vessel location:
            self.local_x_at_cell_positions
            self.local_y_at_cell_positions
        """
        mesh_size = self.function_space.mesh().hmax()/sqrt(2)

        sorting_map = []
        self.n_potential_local_cells = 0
        coord = self.function_space.tabulate_dof_coordinates() # dolfin 2018.1.0 only
        for i, (x, y) in enumerate(coord):
            if ((x % self.cell_size < mesh_size/2.0) \
               or (x % self.cell_size > self.cell_size - mesh_size/2.0)) \
               and ((y % self.cell_size < mesh_size/2.0) \
               or (y % self.cell_size > self.cell_size - mesh_size/2.0)):
                sorting_map.append(((y, x), i))
                self.n_potential_local_cells += 1
        sorting_map.sort()

        self.to_local_cell_positions_map = np.empty(self.n_potential_local_cells, dtype=int)
        self.local_x_at_cell_positions = np.empty(self.n_potential_local_cells, dtype=float)
        self.local_y_at_cell_positions = np.empty(self.n_potential_local_cells, dtype=float)
        j = 0
        for (y, x), i in sorting_map:
            self.to_local_cell_positions_map[j] = i
            self.local_x_at_cell_positions[j] = x
            self.local_y_at_cell_positions[j] = y
            j += 1

    def get_local_at_cell_positions(self):
        """Return the values at the potential cell/vessel positions."""
        return self.function.vector().get_local()[self.to_local_cell_positions_map]

    def set_local_at_cell_positions(self, new_local_values):
        """Set the values at the local, potential cell/vessel positions.

        Args:
            new_local_values (array of floats): New values at potential cell
                positions in row-major order.
        """
        local_values = self.function.vector().get_local()
        local_values[self.to_local_cell_positions_map] = new_local_values
        self.function.vector().set_local(local_values)

    def set(self, init_array):
        """Set the function to the init_array at the cell/vessel positions.

        The function is set to zero everywhere else.

        Reverse the y-axis of the init_array to have the printed version equal 
        the plotted one. 'print init_array[::-1]'

        Args:
            init_array (list of floats): Row-major array of all the values at 
                the cell/vessel positions. The y-axis is reversed to have the   
                printed init_array and the plotted match.
        """
        y = self.local_y_at_cell_positions
        x = self.local_x_at_cell_positions

        # Pick out the indices of the local values in row-major order.
        init_indices = np.rint((y*self.size[0] + x)/self.cell_size).astype(int)
        local_init_array = np.array(init_array).reshape(-1)[init_indices]

        new_local_values = np.zeros(self.function_space.tabulate_dof_coordinates().shape[0])
        new_local_values[self.to_local_cell_positions_map] = local_init_array
        self.function.vector().set_local(new_local_values)

    def collect_values(self):
        """Collect the local values and neighbour values from other processes.

        Can only be used in allow_neighbour_access is True.
        """
        if not self.allow_neighbour_access:
            print("ERROR: RefinedFunction.collect_values() can only be used " \
                + "if allow_neighbour_access is set to True.")
            exit(1)
        self.local_and_neighbours.collect_values(self.function)

    def get_value_at(self, x, y):
        if not self.allow_neighbour_access:
            print("ERROR: RefinedFunction.get_value_at() can only be used if" \
                + "allow_neighbour_access is set to True.")
            exit(1)
        return self.local_and_neighbours.get_value_at(x, y)

    def set_value_at(self, x, y, value, priority, identifier=0):
        """Set value at (x,y) with a priority for conflicts across processes.

        To get the same results as a single process without priority, set the
        priorities in increasing order by the order a single process would have
        set them.
        The priority is only used across processes.
        """
        if not self.allow_neighbour_access:
            print("ERROR: RefinedFunction.set_value_at() can only be used if" \
                + "allow_neighbour_access is set to True.")
            exit(1)
        self.local_and_neighbours.set_value_at(x, y, value, priority, identifier)

    def return_values(self, reset_priorities=True):
        """Return new values to the owner rank and inform about values not set."""
        if not self.allow_neighbour_access:
            print("ERROR: RefinedFunction.return_values() can only be used " \
                + "if allow_neighbour_access is set to True.")
            exit(1)
        return self.local_and_neighbours.return_values(self.function, reset_priorities)

    def reset_priorities(self):
        """Stop new values set from competing against old priorities."""
        self.local_and_neighbours.reset_priorities()


if __name__ == '__main__':
    # This test must be run with 'python refined_function.py' or
    # 'mpirun -n 4 python neighbour_mpi.py' to work and for the later is NOT
    # guaranteed to work, as it relies on that the coordinates of
    # the cell/vessels get one of the following mpi rank: [[0 1 1] or [[0 0 1]
    #                                                      [0 0 1]     [2 0 1]
    #                                                      [2 3 3]     [2 3 3]
    #                                                      [2 2 3]]    [2 2 3]]

    from model_parts.helpers.neighbours_mpi import AllNeighbourVerticesMap
    import add_sim_db_to_path
    import sim_db

    set_log_level(LogLevel.ERROR)

    sim_database = sim_db.add_empty_sim(False)
    sim_database.write('size', [3, 4], "int array")
    sim_database.write('n_refinements', 2, "int")
    sim_database.write('cell_size', 10.0, "float")

    mesh = BaseMesh(sim_database)
    comm = MPI.comm_world
    rank = comm.Get_rank()
    refined_func = RefinedFunction(mesh, 2, allow_neighbour_access=True)

    # Find check is the rank mapping is one of that if implemented.
    possible_rank_maps_over_cell = [[[0,1,1], [0,0,1], [2,3,3], [2,2,3]],
                                    [[0,0,1], [2,0,1], [2,3,3], [2,2,3]]]

    neighbour_map = AllNeighbourVerticesMap(refined_func.function_space, 15)
    is_aborting = False
    if rank == 0:
        for i in range(len(possible_rank_maps_over_cell)):
            if np.array_equal(neighbour_map.coord_to_owner_rank, possible_rank_maps_over_cell[i]):
                rank_map_number = i
                break
            elif i == len(possible_rank_maps_over_cell) - 1:
                print("Map over which rank the possible cell/vessel positions belong to:")
                print(neighbour_map.coord_to_owner_rank)
                print("The test relies on having a different rank map.")
                is_aborting = True
    else:
        rank_map_number = None
    if comm.bcast(is_aborting, root=0):
        exit()
    rank_map_number = comm.bcast(rank_map_number, root=0)

    if rank == 0:
        print("\nTesting the set(init_array) and get_local_at_cell_positions() functions.")

    init_array = [[1, 2, 3],
                  [4, 5, 6],
                  [7, 8, 9],
                  [10, 11, 12]]
    refined_func.set(init_array)
    comm.barrier()
    if rank == 0:
        print("Map over which rank the possible cell/vessel positions belong to:")
        print(neighbour_map.coord_to_owner_rank)
    plot(refined_func.function)
    #interactive()
    comm.barrier()
    print(rank, refined_func.get_local_at_cell_positions())
    if comm.Get_size() == 1:
        if np.array_equal(refined_func.get_local_at_cell_positions(),
                          np.array(init_array).reshape(-1)):
            print("get_local_at_cell_positions() return correct values after " \
                  "setting the values with set(init_array).")
        else:
            print("ERROR: get_local_at_cell_positions returned INCORRECT values " \
                  "after setting the values with set(init_array).")
            exit(1)
    else:
        correct_local = [[[1,4,5], [2,3,6], [7,10,11], [8,9,12]],
                         [[1,2,5], [3,6], [4,7,10,11], [8,9,12]]]

        # The y-axis is reversed to match internal in refined function where
        # the origo is at the bottom left.
        if np.array_equal(refined_func.get_local_at_cell_positions(),
                          correct_local[rank_map_number][comm.Get_rank()]):
            print("get_local_at_cell_positions() return correct values after " \
                  "setting the values with set(init_array).")
        else:
            print("ERROR: get_local_at_cell_positions returned INCORRECT values " \
                  "after setting the values with set(init_array).")
            exit(1)

    plot(refined_func.function)
    interactive()

    comm.barrier()
    if rank == 0:
        print("\nTesting functions from LocalAndNeighbours.")
        print("Testing collect_values and get_value_at.")
    comm.barrier()
    refined_func.collect_values()
    test_coord_and_values = [[1, 2, 8],
                             [1, 2, 8]]
    x = refined_func.cell_size*test_coord_and_values[rank_map_number][0]
    y = refined_func.cell_size*test_coord_and_values[rank_map_number][1]
    value = refined_func.get_value_at(x, y)
    print("get_value_at({0}, {1}) return {2},".format(x, y, value))
    if abs(value - test_coord_and_values[rank_map_number][2]) < 0.001:
        print("which is the correct value.", rank)
    else:
        print("which is the INCORRECT value.")

    comm.barrier()
    if rank == 0:
        print("Testing set_value_at and return_values.")
    comm.barrier()
    refined_func.set_value_at(x, y, -rank, rank)
    refined_func.return_values()
    refined_func.collect_values()
    value = refined_func.get_value_at(x, y)
    print("get_value_at({0}, {1}) return {2},".format(x, y, value))
    if abs(value + comm.Get_size() - 1) < 0.001:
        print("which is the correct value.")
    else:
        print("which is the INCORRECT value.")

    sim_database.delete_from_database()
    sim_database.close()
