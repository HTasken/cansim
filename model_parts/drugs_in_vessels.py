import __init__
import model_parts.helpers.add_sim_db_to_path
import sim_db
from scipy.integrate import solve_ivp
from dolfin import *
import numpy as np
import math


class DrugsInVessels:
    """Chemotherapies' and Avastin's concentrations in the plasma.

    The calculation is performed using pharmacokinetic models.
    """

    def __init__(self, sim_database):
        """Calculate the plasma concentrations for all time.

        Calculations are performed using the pk models of the drugs.
        """

        # Total time of simulation in in minutes.
        self.tot_time = sim_database.read('tot_time')*7*24*60

        # Time-step used at drug injection.
        self.chemo_dt_at_injection = sim_database.read('chemo_dt_at_injection')
        self.vac_dt_at_injection = sim_database.read('vac_dt_at_injection')

        # Maxumum time-step used for chemotherapies and vegf-avastin.
        self.chemo_max_dt = sim_database.read('chemo_max_dt')
        self.vac_max_dt = sim_database.read('vac_max_dt')

        # Patient weight and height
        self.weight = sim_database.read('weight')
        self.height = sim_database.read('height')

        # Body surface aread in m^2
        self.BSA = 0.007184 * (self.weight**(0.425)) * (self.height**(0.725))

        # Concentrations of chemotherapies and Avastin for all time
        timer = Timer("Update Intravascular Drug conc.")
        G1, G1_times = self.__calc_G1_concentration(sim_database)
        G2, G2_times = self.__calc_G2_concentration(sim_database)
        G3, G3_times = self.__calc_G3_concentration(sim_database)
        avastin, avastin_times = self.__calc_avastin_concentration(sim_database)
        timer.stop()

        self.concentrations = {'G1': G1, 'G2': G2, 'G3': G3, 'Avastin': avastin}
        self.times = {'G1': G1_times, 'G2': G2_times, 'G3': G3_times, 
                      'Avastin': avastin_times}
        
        # Indies to keep track of which concentration and time is next.
        self.indies = {'G1': -1, 'G2': -1, 'G3': -1, 'Avastin': -1}

    def get(self, name, time):
        """Get drug concentration with name 'name' at time 'time'.

        The return concentration is a linear interpolation of the closest 
        solved concentrations.

        Args:
            name (string): Name of drug. Can be 'G1', 'G2', 'G3' or 'Avastin'. 
            time (float): Time or returned concentration.
        """
        while (self.times[name][self.indies[name]] > time):
            self.indies[name] -= 1
        while (self.indies[name] + 1 < len(self.times[name]) - 1 
               and self.times[name][self.indies[name] + 1] <= time):
            self.indies[name] += 1
        next_time = self.times[name][self.indies[name] + 1]
        prev_time = self.times[name][self.indies[name]]
        frac_next = (time - prev_time)/(next_time - prev_time)
        frac_prev = 1 - frac_next
        c = (self.concentrations[name][self.indies[name]]*frac_prev
             + self.concentrations[name][self.indies[name] + 1]*frac_next)

        return c

    @staticmethod
    def __rhs_G1_ode(G1, Vm, Km):
        return - Vm*G1/(Km + G1)

    def __calc_G1_concentration(self, sim_database):
        """Fluorouracil - G1

        Drug plasma conc. calculation according to 2-compartment PK model

        Differential equation to solve, where G1 is the Fluorouracil
        concentration:
            d G1/dt = -Vm/(Km + G1)*G1
        """
        # Constants in equation
        V = 24 * (10**(3))  # compartment vol, mL
        Vm = 105.0 /60
        Km = 27

        # Dose of Fluorouracil, G1, used in one injection, mug
        dose = sim_database.read('dose')[0]

        # Time between drug injections given in minutes
        dt_drug = sim_database.read('schedule')[0]*7*24*60

        # Value at injection time
        G1_0 = dose * self.BSA * 10**3 / V

        # Concentrations of G1 and time of the concentrations
        G1 = np.array([])
        times = np.array([])

        # Solve for all time
        n_injections = int((self.tot_time-dt_drug/2.0)/dt_drug) + 1
        time = 0
        fun=lambda t, y: self.__rhs_G1_ode(y, Vm, Km)

        for i in range(n_injections):
            sol = solve_ivp(fun, (time, time + dt_drug - self.chemo_dt_at_injection[0]), 
                    [G1_0], max_step=self.chemo_max_dt[0])
            G1 = np.append(G1, sol.y)
            times = np.append(times, sol.t)
            time += dt_drug
            G1_0 += G1[-1]
        return G1, times

    @staticmethod
    def __derivative_G2_solution(t, tD, y, G2_0, A, alpha, B, beta, C, gamma):
        return G2_0*(- alpha*A*np.exp(-alpha*(t-tD)) - beta*B*np.exp(-beta*(t-tD)) 
                     - gamma*C*np.exp(-gamma*(t-tD)))

    def __calc_G2_concentration(self, sim_database):
        """EPIRUBICIN - G2

        Drug plasma conc. calculation according to 3 compartment PK model

        Differential equation to solve, where G2 is the Epirubicin
        concentration:
            d G2_1/dt = - Q2/V1*G2_2 - Q3/V1*G2_3 - CL2/V1*G2_1 + Q3/V3*G2_3 
                        + Q2/V2*G2_2
            d G2_2/dt = - Q2/V2*G2_2 + Q2/V1*G2_1
            d G2_3/dt = - Q3/V3*G2_3 + Q3/V1*G2_1
        """
        # Define constants used in the calculations
        CL = 59. / 60. * (10**(3))  #central clearance, mL/min
        CL2 = 56. / 60. * (10**(3))  #rapid clearance, mL/min
        CL3 = 15. / 60. * (10**(3))  #slow clearance, mL/min
        V1 = 18 * (10**(3))
        V3 = 25 * (10**(3))
        V2 = 1000 * (10**(3)) - V1 - V3

        # Find eigenvalues of system
        a0 = CL * CL2 * CL3 / V1 / V2 / V3
        a1 = CL * CL3 / V1 / V3 + CL2 * CL3 / (V2 * V3 + V2 * V1) + \
             CL * CL2 / V1 / V2 + CL3 * CL2 / V3 / V1
        a2 = CL / V1 + CL2 / V1 + CL3 / V1 + CL2 / V2 + CL3 / V3
        p = a1 - a2**(2) / 3.  # define update time interval
        q = 2 * a2**(3) / 27. - a1 * a2 / 3. + a0
        r1 = math.sqrt(-(p**(3)) / 27)
        r2 = 2 * (r1**(1. / 3.))
        phi = math.acos(-q / 2. / r1) / 3.

        alpha = -(math.cos(phi) * r2 - a2 / 3.)
        beta = -(math.cos(phi + 2 * math.pi / 3.) * r2 - a2 / 3.)
        gamma = -(math.cos(phi + 4 * math.pi / 3.) * r2 - a2 / 3.)

        # Constants giving correct concentration at time of injecting the drug
        A = (CL2 / V2 - alpha) * (CL3 / V3 - alpha) / (beta - alpha) / (
                gamma - alpha)
        B = (beta - CL2 / V2) * (beta - CL3 / V3) / (beta - alpha) / (
                beta - gamma)
        C = (gamma - CL2 / V2) * (CL3 / V3 - gamma) / (beta - gamma) / (
                gamma - alpha)

        # Dose of Avastin used in one injection, mug
        dose = sim_database.read('dose')[1]

        # Time between drug injections given in minutes.
        dt_drug = sim_database.read('schedule')[1]*7*24*60

        # Value at injection time
        G2_0 = dose * self.BSA * 10**3 / V1

        # Concentrations of G2 and time of the concentrations
        G2 = np.array([])
        times = np.array([])

        # Solve for all time
        n_injections = int((self.tot_time-dt_drug/2.0)/dt_drug) + 1
        time = 0
        tD = 0
        fun = lambda t, y: self.__derivative_G2_solution(t, time, y, G2_0, A, alpha, B, beta, C, gamma)       
        for i in range(n_injections):
            sol = solve_ivp(fun, (time, time + dt_drug - self.chemo_dt_at_injection[1]), 
                    [G2_0], max_step=self.chemo_max_dt[1])
            G2 = np.append(G2, sol.y)
            G2[G2<0] = 0
            times = np.append(times, sol.t)
            time += dt_drug
        return G2, times

    @staticmethod 
    def __derivative_G3_solution(t, tD, y, G3_0, V, CL):
        return -CL/V*G3_0*np.exp(-CL/V*(t-tD))

    def __calc_G3_concentration(self, sim_database):
        """Cyclophosphamide- G3

        Drug plasma conc. calculation according to 2 compartment PK model

        Differential equation to solve, where G3 is the Cyclophosphamide
        concentration:
            d G3/dt = -CL/V*G3
        """
        # Constants in the equation
        V = 2430. * (10**(3))  #plasma vol., ml
        CL = 236. * (10**(3)) / 60.  #clearance, ml/min

        # Dose of Avastin used in one injection, mug
        dose = sim_database.read('dose')[2]

        # Time between drug injections given in minutes.
        dt_drug = sim_database.read('schedule')[2]*7*24*60

        # Value at injection time
        G3_0 = dose * self.BSA * 10**3 / V

        # Concentrations of G3 and time of the concentrations
        G3 = np.array([])
        times = np.array([])

        # Solve for all time
        n_injections = int((self.tot_time-dt_drug/2.0)/dt_drug) + 1        
        time = 0
        fun = lambda t, y: self.__derivative_G3_solution(t, time, y, G3_0, V, CL)
        for i in range(n_injections):
            sol = solve_ivp(fun, (time, time + dt_drug - self.chemo_dt_at_injection[2]), 
                [G3_0], max_step=self.chemo_max_dt[2])
            G3 = np.append(G3, sol.y)
            G3[G3 < 0] = 0
            G3_0 += G3[-1]
            times = np.append(times, sol.t)
            time += dt_drug
        return G3, times

    @staticmethod
    def __derivative_avastin_solution(t, tD, avastin_0, A, alpha, B, beta):
        return avastin_0*(- alpha*A*math.exp(-alpha*(t-tD)) 
                          - beta*B*math.exp(-beta*(t-tD)))

    def __calc_avastin_concentration(self, sim_database):
        """Avastin plasma conc. calculation according to 2-compartment PK model

        Set of differential equation to solve, where A1 is the Avastin plasma
        concentration:
            d A1(t)/dt = -Q/V1 A1(t) - CL/V1 A1(t) + Q/V2 A2(t) 
            d A2(t)/dt = -Q/V2 A2(t) + Q/V1 A1(t)
        """
        # Constants in equation
        CL = 0.207 / 24 / 60 * 10**(3)  # clearance, mL/min
        V1 = 2.66 * 10**(3)  # volume, mL
        k12 = 0.223 / 24. / 60.  # 1/min
        k21 = 0.215 / 24. / 60.  # 1/min
        Q = k12 * V1  # intercompartmental clearance, mL/min
        V2 = k12 / k21 * V1

        # Find eigenvalues of system
        beta = 0.5 * (Q / V1 + Q / V2 + CL / V1 - math.sqrt(
                (Q / V1 + Q / V2 + CL / V1)**2 - 4.0 * (Q * CL / V1 / V2)))
        alpha = Q * CL / V1 / V2 / beta

        # Constants giving correct concentration at time of injecting the drug
        A = (alpha - Q / V2) / (alpha - beta)
        B = (-beta + Q / V2) / (alpha - beta)

        # Dose of Avastin used in one injection, mug
        dose = sim_database.read('dose')[3]

        # Time between drug injections given in week
        dt_drug = sim_database.read('schedule')[3]*7*24*60

        # Value at injection time
        avastin_0 = dose * self.weight * 10**3 / V1

        # Concentrations of avastin and time of the concentrations
        avastin = np.array([])
        times = np.array([])

        # Solve for all time
        n_injections = int((self.tot_time-dt_drug/2.0)/dt_drug) + 1
        time = 0
        fun = lambda t, y: self.__derivative_avastin_solution(t, time, avastin_0, A, alpha, B, beta)
        for i in range(n_injections):
            sol = solve_ivp(fun, (time, time + dt_drug - self.vac_dt_at_injection), 
                    [avastin_0], max_step=self.vac_max_dt)
            avastin = np.append(avastin, sol.y)
            avastin_0 += avastin[-1]
            times = np.append(times, sol.t)
            time += dt_drug
        return avastin, times

if __name__ == '__main__':
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    
    sim_database = sim_db.add_empty_sim(False)
    sim_database.write("tot_time", 12, "float")
    sim_database.write("vac_dt_at_injection", 0.01, "float")
    sim_database.write("chemo_dt_at_injection", [0.01, 0.01, 0.01], "float array")
    sim_database.write("vac_max_dt", 10.0, "float")
    sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
    sim_database.write("height", 160, "float")
    sim_database.write("weight", 58.2, "float")
    dose = [600, 100, 600, 15]
    sim_database.write("dose", dose, "float array")
    schedule = [3, 3, 3, 3]
    sim_database.write("schedule", schedule, "float array")

    drugs_in_vessels = DrugsInVessels(sim_database)

    print(drugs_in_vessels.get('G1',0))
    print(drugs_in_vessels.get('G2',0))
    print(drugs_in_vessels.get('G3',0))
    print(drugs_in_vessels.get('G1',30240))
    print(drugs_in_vessels.get('G2',30240))
    print(drugs_in_vessels.get('G3',30240))
    print(sum(drugs_in_vessels.concentrations['G1'] < 0))
    print(sum(drugs_in_vessels.concentrations['G2'] < 0))
    print(sum(drugs_in_vessels.concentrations['G3'] < 0))
    fig = plt.figure()
    plt.plot(drugs_in_vessels.times['G1'], drugs_in_vessels.concentrations['G1'])
    plt.savefig("figs/G1_in_vessels.png")
    plt.close(fig)
    fig = plt.figure()
    plt.plot(drugs_in_vessels.times['G2'], drugs_in_vessels.concentrations['G2'])
    plt.savefig("figs/G2_in_vessels.png")
    plt.close(fig)
    fig = plt.figure()    
    plt.plot(drugs_in_vessels.times['G3'], drugs_in_vessels.concentrations['G3'])
    plt.savefig("figs/G3_in_vessels.png")
    plt.close(fig)
    fig = plt.figure()
    plt.plot(drugs_in_vessels.times['Avastin'], drugs_in_vessels.concentrations['Avastin'])
    plt.savefig("figs/Avastin_in_vessels.png")
    plt.close(fig)    

    sim_database.delete_from_database()
    sim_database.close()
