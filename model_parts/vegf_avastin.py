import __init__
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.load_function import load_function_if_checkpoint
from model_parts.helpers.checkpoint_time import get_time_last_checkpoint
from model_parts.helpers.checkpoint_time import get_varing_max_dt_after_injection
from model_parts.helpers.init_num_vessels import calc_init_num_vessels
import model_parts.helpers.add_sim_db_to_path
import sim_db
from dolfin import *
import input_output.log as log
import numpy as np
class VegfAvastin:
    def __init__(self, sim_database, base_mesh, drugs_in_vessels):
        self.drugs_in_vessels = drugs_in_vessels

        # Read parameters from database.
        self.size = sim_database.read('size')
        self.dt_injections = sim_database.read('schedule')[3]*7*24*60
        self.dt_at_injection = sim_database.read('vac_dt_at_injection')
        self.dt_increase_factor_after_injection = sim_database.read(
                'vac_dt_increase_factor_after_injection')
        self.dt_after_cell_update = sim_database.read(
                'vac_dt_after_cell_update')
        self.dt_increase_factor_after_cell_update = sim_database.read(
                'vac_dt_increase_factor_after_cell_update')
        self.max_dt = sim_database.read('vac_max_dt')
        n_refinements = sim_database.read('n_refinements_vac')
        degree = sim_database.read('degree_vac')
        self.ktrans = sim_database.read('ktrans')
        self.init_num_vessels = calc_init_num_vessels(sim_database)
        self.res_dir = sim_database.read("results_dir")
        # Set time to last checkpoint of zero if no checkpoints exists.
        self.time = get_time_last_checkpoint(sim_database)

        # Time-step variables
        self.time_next_injection = self.dt_at_injection
        self.time_last_injection_dt_increase = 0.0
        self.time_last_cell_update_dt_increase = 0.0
        self.varing_max_dt_after_cell_update = self.dt_after_cell_update
        self.varing_max_dt_after_injection = get_varing_max_dt_after_injection(
            self.time, self.dt_injections, self.dt_at_injection, 
            self.dt_increase_factor_after_injection)

        # Using RefinedFunction allow a more accurate function and can be used
        # to check and adjust the error in spatial discritisation.
        self.VAC = RefinedFunction(base_mesh, n_refinements, degree, dim=3,
                                   create_trial_function=True)
        self.V = RefinedFunction(base_mesh, n_refinements, degree,
                                  create_trial_function=True)
        self.A = RefinedFunction(base_mesh, n_refinements, degree,
                                  create_trial_function=True)
        self.C = RefinedFunction(base_mesh, n_refinements, degree,
                                  create_trial_function=True)

        # Load from checkpoint if any previous checkpoints.
        load_function_if_checkpoint(sim_database, self.VAC.function,
                                    "vegf_avastin")
        load_function_if_checkpoint(sim_database, self.V.function,
                                    "vegf")
        # Times at which the diffusion-reaction equation was solved.
        self.times_solved = []

        # Define constants
        self.D_v = Constant(3.52e3) # Diffusion coefficient for VEGF
        self.D_a = Constant(2.4e3) # Diffusion coefficient for Avastin
        self.D_c = Constant(3.52e5) #self.D_v # Diffusion coefficient for VEGF-Avastin Complex
        self.ka = Constant(1e5) #7.4e-1) # Binding association rate
        self.kd = Constant(0.0) #1.86e-3) # Disassociation rate
        self.psi_v = Constant(0.01) # Decay rate for VEGF
        self.psi_a = Constant(0.01) # Decay rate for Avastin
        self.psi_c = self.psi_v # Decay rate for VEGF-Avastin Complex
        self.a_sup = 2.639016e-06 # VEGF supply term constant
        self.b_sup = -4.450658e-7 # VEGF supply term constant
        self.c = Constant(0.01*self.size[0]*self.size[1]) # Permeability coefficient

    def update(self, end_time, is_vessel_updated, vessels, cells, oxygen, update_avastin=True):
        """Solves a diffusion-reaction eq. for VEGF-avastin conc. for time int.

        Set of diffusion-reaction equations:
            d V/dt = Dv*nabla^2*V - V_sup*delta_func(x - x_cells) 
                    - k_a*V*A + k_d*C - psi_v*V
            d A/dt = Da*nabla^2*A - k_a*V*A + k_d*C - psi_a*A
                    - (c*ktrans/init_num_vessels*avg_radius
                       *relative_radius*(Av - A)*delta_func(x-x_vessels))
            d C/dt = Dc*nabla^2*C + k_a*V*A - k_c*C - psi_c*C
        
        where V_sup = (a*Vc + b)*heaviside_func(a*Vc + b)

        Args:
            end_time (float): Time of the final solution of G.
            is_vessels_updated (bool): Set 'True' if vessels (or cells) have 
                                       changed since last update() call. 
            update_avastin (bool): Set 'True' if Avastin is administered.
        """
        timer_update = Timer("Update VAC")
        # Define functions used in the equation.
    
        v = self.VAC.test_function
        vac_prev = self.VAC.prev_function

        # Delta function for vessels times the relative radius of the vessel.
        timer = Timer("Project")
        if is_vessel_updated:                   
            self.vessel_locations_rel_radius = interpolate(
                vessels.rel_radius_delta_function.function, self.VAC.function_space.sub(0).collapse())
        # Delta function for cells.
        self.cell_locations = interpolate(cells.delta_function.function, self.V.function_space)
        # Intracellular VEGF concentration
        log.log('Update VEGFs','a',self.res_dir)        
        cells.update_vegf(end_time, oxygen)   

        v_c = interpolate(cells.vegf.function,
                      self.V.function_space)
        timer.stop()
        # Supply term of intracellular VEGF from applying Heaviside step func.
        v_sup = v_c
        v_sup_local = v_sup.vector().get_local()
        indices_to_zero = v_sup_local < -self.b_sup/self.a_sup
        other_indices = indices_to_zero < 0.5
        v_sup_local[indices_to_zero] = 0 # Heaviside step func. at work.
        v_sup_local[other_indices] *= self.a_sup
        v_sup_local[other_indices] += self.b_sup
        v_sup.vector().set_local(v_sup_local)
        
        # Get concentration of Avastin in the vessels.
        a_v = Constant(self.drugs_in_vessels.get('Avastin', self.time))

        # Set time-step
        if (self.varing_max_dt_after_injection < self.dt_after_cell_update
                and self.varing_max_dt_after_injection < self.max_dt):
            dt = Constant(self.varing_max_dt_after_injection)
        elif (self.dt_after_cell_update < self.max_dt):
            dt = Constant(self.dt_after_cell_update)
        else:
            dt = Constant(self.max_dt)
        self.varing_max_dt_after_cell_update = self.dt_after_cell_update

        # Set up variational form of the equation for the VEGF, Avastin and
        # VEGF-Avastin complex concentrations.
        # Simplify function to update V only when no Avastin is administered
        if update_avastin == False:
            log.log('No avastin fast solving','a',self.res_dir)

            PETScOptions.set("ksp_max_it", 5000)
            PETScOptions.set("ksp_rtol", 1e-8)
            PETScOptions.set("ksp_atol", 1e-8)
            PETScOptions.set("ksp_type", "cg")
            PETScOptions.set("pc_type", "hypre")
            PETScOptions.set("pc_hypre_type", "boomeramg")
            PETScOptions.set("pc_hypre_boomeramg_strong_threshold", 0.5)  
            PETScOptions.set("pc_hypre_boomeramg_interp_type","ext+i")

            vac = self.V.trial_function
            vac_prev = self.V.function
            vac_mid = 0.5*(vac + vac_prev)
            s = self.V.test_function
            F = (-(vac - vac_prev)*s*dx()
                 - dt*inner(self.D_v*grad(vac_mid), grad(s))*dx()
                 + dt*v_sup*self.cell_locations*s*dx()
                 - dt*self.psi_v*vac_mid*s*dx()
                 )
            (a, L) = system(F)
            log.log('Assemble VEGF left','a',self.res_dir)
            A = assemble(a)
            solver = PETScKrylovSolver()
            solver.set_from_options()
            while self.time < end_time + 1e-9:
                a_v.assign(Constant(self.drugs_in_vessels.get('Avastin', self.time)))
                timer = Timer('Assemble VAC fast')
                log.log('Assemble VEGF','a',self.res_dir)
                b = assemble(L)
#                (A, b) = assemble_system(a, L, None)
                timer.stop()
                timer = Timer("Solve VAC fast")
                solver.solve(A,self.V.function.vector(),b)
                log.log(f'Solve VAC at time {self.time}','a',self.res_dir)
                timer.stop()
                self.times_solved.append(self.time)
                vac_prev.assign(self.V.function)
                next_dt = self.__get_next_dt(end_time)
                dt.assign(Constant(next_dt))      
                self.time += next_dt

            self.time -= next_dt
            timer_update.stop()
            # assign VEGF update values
            assign(self.VAC.function.sub(0),self.V.function)             
        else:
            vac = self.VAC.trial_function            
            vac_mid = 0.5*(vac + vac_prev)
            PETScOptions.set("ksp_max_it", 5000)
            PETScOptions.set("ksp_rtol", 1e-7)
            PETScOptions.set("ksp_atol", 1e-7)
            PETScOptions.set("ksp_type", "cg")
            PETScOptions.set("pc_type", "hypre")
            PETScOptions.set("pc_hypre_type", "boomeramg")
            PETScOptions.set("pc_hypre_boomeramg_strong_threshold", 0.5)  
            PETScOptions.set("ksp_converged_reason")
            PETScOptions.set("ksp_view")    
            PETScOptions.set("ksp_monitor_true_residual")   
#            PETScOptions.set("ksp_initial_guess_nonzero",True)                      
            F = (
                (- (vac[0] - vac_prev[0])*v[0]
                - (vac[1] - vac_prev[1])*v[1]
                - (vac[2] - vac_prev[2])*v[2])*dx()
            + (- dt*inner(self.D_v*grad(vac_mid[0]), grad(v[0]))
                - dt*inner(self.D_a*grad(vac_mid[1]), grad(v[1]))
                - dt*inner(self.D_c*grad(vac_mid[2]), grad(v[2])))*dx()
            + (+ dt*v_sup*self.cell_locations*v[0])*dx()
            + (+ dt*(Constant(self.c*self.ktrans/self.init_num_vessels)
                    *(a_v - vac_mid[1])*self.vessel_locations_rel_radius*v[1]))*dx()
            + (+ dt*(- self.ka*vac_prev[0]*vac_mid[1]*v[0]
                    - self.ka*vac_mid[0]*vac_prev[1]*v[1]
                    + self.ka*vac_prev[0]*vac_prev[1]*v[2]
                    + self.kd*vac_prev[2]*v[0]
                    + self.kd*vac_prev[2]*v[1]
                    - self.kd*vac_mid[2]*v[2]))*dx()
            + (+ dt*(- self.psi_v*vac_mid[0]*v[0]
                    - self.psi_a*vac_mid[1]*v[1]
                    - self.psi_c*vac_mid[2]*v[2]))*dx()
            )
            (a, L) = system(F)
            solver = PETScKrylovSolver()
            solver.set_from_options()
            while self.time < end_time + 1e-9:
                cells.update_vegf(self.time, oxygen)
                log.log('Update VEGFs','a',self.res_dir)
                a_v.assign(Constant(self.drugs_in_vessels.get('Avastin', self.time)))
                timer = Timer('Assemble VAC')
                log.log('Assemble VEGF','a',self.res_dir)             
                (A, b) = assemble_system(a, L, None)
                timer.stop()
                timer = Timer("Solve VAC")
                solver.solve(A,self.VAC.function.vector(),b)
                log.log(f'Solve VAC at time {self.time}','a',self.res_dir)
                timer.stop()
                self.times_solved.append(self.time)
                vac_prev.assign(self.VAC.function)
                next_dt = self.__get_next_dt(end_time)
                dt.assign(Constant(next_dt))
                self.time += next_dt

            self.time -= next_dt
            timer_update.stop()            

    def __get_next_dt(self, end_time):
        if (self.varing_max_dt_after_cell_update
                < self.varing_max_dt_after_injection
                and self.varing_max_dt_after_cell_update < self.max_dt):
            next_dt = self.varing_max_dt_after_cell_update
        elif (self.varing_max_dt_after_injection < self.max_dt):
            next_dt = self.varing_max_dt_after_injection
        else:
            next_dt = self.max_dt
        if (self.time + next_dt > end_time and self.time < end_time - 1e-9):
            next_dt = end_time - self.time
        elif (self.time + next_dt
                >= self.time_next_injection - self.dt_at_injection):
            next_dt = self.time_next_injection - self.dt_at_injection - self.time
            self.time_next_injection += self.dt_injections
            self.varing_max_dt_after_injection = self.dt_at_injection
        if (self.time > self.time_last_cell_update_dt_increase
                + self.varing_max_dt_after_cell_update):
            self.time_last_cell_update_dt_increase += (
                    self.varing_max_dt_after_cell_update)
            self.varing_max_dt_after_cell_update *= (
                    self.dt_increase_factor_after_cell_update)
        if (self.time > self.time_last_injection_dt_increase
                + self.varing_max_dt_after_injection):
            self.time_last_injection_dt_increase += (
                    self.varing_max_dt_after_injection)
            self.varing_max_dt_after_injection *= (
                    self.dt_increase_factor_after_injection)

        return next_dt


    def solve_steady_state(self, vessels, cells, oxygen=None,
                           time_update_first=None, first_dt=None):
        """Solves the steady-state version of the set of PDE VEGF-avastin conc.

        Also sets the previous VEGF-Avastin concentration to the same value.

        Set of diffusion-reaction equations:
            d V/dt = Dv*nabla^2*V - v_sup*delta_func(x - x_cells) 
                    - k_a*V*A + k_d*C - psi_v*V
            d A/dt = Da*nabla^2*A - k_a*V*A + k_d*C - psi_a*A
                    - (c*ktrans/init_num_vessels*avg_radius
                       *relative_radius*(Av - A)*delta_func(x-x_vessels))
            d C/dt = Dc*nabla^2*C + k_a*V*A - k_c*C - psi_c*C

        where v_sup = (a*Vc + b)*heaviside_func(a*Vc + b)

        Args:
            oxygen (Oxygen): Only needed it 'time_update_first' is not 'None'.
            time_update_first (float): Run update() until 'time_update_first' 
                                       before solving the steady-state.
            first_dt (float): Time-step used when first calling update().
        """
        # Solve the time-dependent equations untils 'time_update_first' to have
        # a vegf, avastin and complex concentration that converges easier into
        # the steady-state equations.
        if time_update_first != None:
            tmp_time = self.time
            tmp_times_solved = list(self.times_solved)
            tmp_dt_after_cell_update = self.dt_after_cell_update
            if first_dt != None:
                self.dt_after_cell_update = first_dt
            self.update(time_update_first + self.time, True, vessels, cells, oxygen)
            self.time = tmp_time
            self.times_solved = tmp_times_solved
            self.dt_after_cell_update = self.dt_after_cell_update

        # Define functions used in the equation.
        vac = self.VAC.function
        v = self.VAC.test_function

        # Delta function for vessels times the relative radius of the vessel.
        self.vessel_locations_rel_radius = interpolate(
                                    vessels.rel_radius_delta_function.function, self.VAC.function_space.sub(0).collapse())
        # Delta function for cells.
        self.cell_locations = interpolate(cells.delta_function.function, self.VAC.function_space.sub(1).collapse())
        
        # Intracellular VEGF concentration
        v_c = interpolate(cells.vegf.function,
                      self.VAC.function_space.sub(1).collapse())

        # Supply term of intracellular VEGF from applying Heaviside step func.
        v_sup = v_c
        v_sup_local = v_sup.vector().get_local()
        indices_to_zero = v_sup_local < -self.b_sup/self.a_sup
        other_indices = indices_to_zero < 0.5
        v_sup_local[indices_to_zero] = 0 # Heaviside step func. at work.
        v_sup_local[other_indices] *= self.a_sup
        v_sup_local[other_indices] += self.b_sup
        v_sup.vector().set_local(v_sup_local)
        
        # Get concentration of Avastin in the vessels.
        a_v = Constant(self.drugs_in_vessels.get('Avastin', self.time))

        # Set up variational form of the steady-state equation for the VEGF,
        # Avastin and VEGF-Avastin complex concentrations.
        F = (
            v_sup*self.cell_locations*v[0]
            - inner(self.D_v*grad(vac[0]), grad(v[0]))
            - inner(self.D_a*grad(vac[1]), grad(v[1]))
            - inner(self.D_c*grad(vac[2]), grad(v[2]))
            + (Constant(self.c*self.ktrans/self.init_num_vessels)
               *(a_v - vac[1])*self.vessel_locations_rel_radius*v[1])
            + (- self.ka*vac[0]*vac[1]*v[0]
               - self.ka*vac[0]*vac[1]*v[1]
               + self.ka*vac[0]*vac[1]*v[2]
               + self.kd*vac[2]*v[0]
               + self.kd*vac[2]*v[1]
               - self.kd*vac[2]*v[2]
               - self.psi_v*vac[0]*v[0]
               - self.psi_a*vac[1]*v[1]
               - self.psi_c*vac[2]*v[2])
        )*dx()

        dF = derivative(F, self.VAC.function, self.VAC.trial_function)
        problem = NonlinearVariationalProblem(F, self.VAC.function, None, dF)
        solver = NonlinearVariationalSolver(problem)

        prm = solver.parameters
        prm['newton_solver']["linear_solver"] = "gmres"
#        prm['newton_solver']["preconditioner"] = "jacobi"
#        prm["newton_solver"]["relaxation_parameter"] = 0.6
        prm['newton_solver']["convergence_criterion"] = "incremental"
        prm['newton_solver']["absolute_tolerance"] = 1e-8        
        prm['newton_solver']["relative_tolerance"] = 1e-7
        prm['newton_solver']["maximum_iterations"] = 25
#        prm["newton_solver"]["krylov_solver"]["nonzero_initial_guess"] = True
        prm['newton_solver']['krylov_solver']["absolute_tolerance"] = 1e-4
        prm['newton_solver']['krylov_solver']["relative_tolerance"] = 1e-5
#        prm['newton_solver']['krylov_solver']["maximum_iterations"] = 500
        # Solve steady-state
        solver.solve()

        # Update previous concentration.
        self.VAC.prev_function.assign(self.VAC.function)
        assign(self.V.function, self.VAC.function.sub(0))
    def update_new(self, end_time, is_vessel_updated, vessels, cells, oxygen):
        """Solves a diffusion-reaction eq. for VEGF-avastin conc. for time int.

        Set of diffusion-reaction equations:
            d V/dt = Dv*nabla^2*V - V_sup*delta_func(x - x_cells) 
                    - k_a*V*A + k_d*C - psi_v*V
            d A/dt = Da*nabla^2*A - k_a*V*A + k_d*C - psi_a*A
                    - (c*ktrans/init_num_vessels*avg_radius
                       *relative_radius*(Av - A)*delta_func(x-x_vessels))
            d C/dt = Dc*nabla^2*C + k_a*V*A - k_c*C - psi_c*C
        
        where V_sup = (a*Vc + b)*heaviside_func(a*Vc + b)

        Args:
            end_time (float): Time of the final solution of G.
            is_vessels_updated (bool): Set 'True' if vessels (or cells) have 
                                       changed since last update() call. 
        """
        timer_update = Timer("Update VAC")
        # Define functions used in the equation.
        vac_V = self.V.function
        vac_A = self.A.function
        vac_C = self.C.function
        v = self.VAC.test_function
        vac_prev = self.VAC.prev_function
        vac_mid = 0.5*(vac + vac_prev)

        # Delta function for vessels times the relative radius of the vessel.
        timer = Timer("Project")
        if is_vessel_updated:        
            self.vessel_locations_rel_radius = interpolate(
                vessels.rel_radius_delta_function.function, self.VAC.function_space.sub(0).collapse())
        # Delta function for cells.
        self.cell_locations = interpolate(cells.delta_function.function, self.VAC.function_space.sub(1).collapse())
        # Intracellular VEGF concentration
        v_c = interpolate(cells.vegf.function,
                      self.VAC.function_space.sub(1).collapse())
        timer.stop()
        # Supply term of intracellular VEGF from applying Heaviside step func.
        v_sup = v_c
        v_sup_local = v_sup.vector().get_local()
        indices_to_zero = v_sup_local < -self.b_sup/self.a_sup
        other_indices = indices_to_zero < 0.5
        v_sup_local[indices_to_zero] = 0 # Heaviside step func. at work.
        v_sup_local[other_indices] *= self.a_sup
        v_sup_local[other_indices] += self.b_sup
        v_sup.vector().set_local(v_sup_local)

        # Get concentration of Avastin in the vessels.
        a_v = Constant(self.drugs_in_vessels.get('Avastin', self.time))

        # Set time-step
        if (self.varing_max_dt_after_injection < self.dt_after_cell_update
                and self.varing_max_dt_after_injection < self.max_dt):
            dt = Constant(self.varing_max_dt_after_injection)
        elif (self.dt_after_cell_update < self.max_dt):
            dt = Constant(self.dt_after_cell_update)
        else:
            dt = Constant(self.max_dt)
        self.varing_max_dt_after_cell_update = self.dt_after_cell_update

        # Set up variational form of the equation for the VEGF, Avastin and
        # VEGF-Avastin complex concentrations.
        F1 = - (vac_V - vac_V_prev)*v*dx() \
            - dt*inner(self.D_v*grad(vac_V), grad(v))*dx() \
            + dt*v_sup*self.cell_locations*v*dx() \
            + dt*(- self.ka*vac_V_mid*vac_A_prev*v \
            + self.kd*vac_C*v)*dx() \
            + dt*(- self.psi_v*vac_V*v)*dx()
        F2 = - (vac_A - vac_A_prev)*v*dx() \
            - dt*inner(self.D_a*grad(vac_A_mid), grad(v))*dx() \
            + dt*(Constant(self.c*self.ktrans/self.init_num_vessels) \
                   *(a_v - vac_A_mid)*self.vessel_locations_rel_radius*v)*dx() \
            + dt*(- self.ka*vac_mid[0]*vac_mid[1]*v[1])*dx() \
            + dt* self.kd*vac_mid[2]*v[1]*dx() \
            + dt*(- self.psi_a*vac_mid[1]*v[1])*dx()
        F3 = - (vac[2] - vac_prev[2])*v[2]*dx() \
            - dt*inner(self.D_c*grad(vac_mid[2]), grad(v[2]))*dx() \
            + dt*(self.ka*vac_mid[0]*vac_mid[1]*v[2] \
                - self.kd*vac_mid[2]*v[2])*dx() \
            + dt*(- self.psi_c*vac_mid[2]*v[2])*dx()
        dF = derivative(F, self.VAC.function, self.VAC.trial_function)
        problem = NonlinearVariationalProblem(F, self.VAC.function, None, dF)
        solver = NonlinearVariationalSolver(problem)
        prm = solver.parameters
        prm['newton_solver']["linear_solver"] = "gmres"
        prm['newton_solver']["preconditioner"] = "jacobi" 
        prm["newton_solver"]["relaxation_parameter"] = 0.6
        prm['newton_solver']["convergence_criterion"] = "incremental"
        prm['newton_solver']["absolute_tolerance"] = 1e-8        
        prm['newton_solver']["relative_tolerance"] = 1e-7
        prm['newton_solver']["maximum_iterations"] = 25
        prm["newton_solver"]["krylov_solver"]["nonzero_initial_guess"] = True
        prm['newton_solver']['krylov_solver']["absolute_tolerance"] = 1e-4
        prm['newton_solver']['krylov_solver']["relative_tolerance"] = 1e-5
        prm['newton_solver']['krylov_solver']["maximum_iterations"] = 500
#        prm['newton_solver']['krylov_solver']['monitor_convergence'] = True
#       info(solver.parameters, True)


        # Solve the equations for the time interval.
        while self.time < end_time + 1e-9:
            cells.update_vegf(self.time, oxygen)
#            log.log('Update VEGFs')
            a_v.assign(Constant(self.drugs_in_vessels.get('Avastin', self.time)))
            timer = Timer("Solve VAC")
#            solver.solve(problem,self.VAC.function.vector())
            solver.solve()
#            log.log(f'Solve VAC at time {self.time}')
            timer.stop()
            self.times_solved.append(self.time)
            vac_prev.assign(vac)
            next_dt = self.__get_next_dt(end_time)
            dt.assign(Constant(next_dt))
            self.time += next_dt

        self.time -= next_dt
        timer_update.stop()
#        log.log('VAC update finished')

    def __get_next_dt(self, end_time):
        if (self.varing_max_dt_after_cell_update
                < self.varing_max_dt_after_injection
                and self.varing_max_dt_after_cell_update < self.max_dt):
            next_dt = self.varing_max_dt_after_cell_update
        elif (self.varing_max_dt_after_injection < self.max_dt):
            next_dt = self.varing_max_dt_after_injection
        else:
            next_dt = self.max_dt
        if (self.time + next_dt > end_time and self.time < end_time - 1e-9):
            next_dt = end_time - self.time
        elif (self.time + next_dt
                >= self.time_next_injection - self.dt_at_injection):
            next_dt = self.time_next_injection - self.dt_at_injection - self.time
            self.time_next_injection += self.dt_injections
            self.varing_max_dt_after_injection = self.dt_at_injection
        if (self.time > self.time_last_cell_update_dt_increase
                + self.varing_max_dt_after_cell_update):
            self.time_last_cell_update_dt_increase += (
                    self.varing_max_dt_after_cell_update)
            self.varing_max_dt_after_cell_update *= (
                    self.dt_increase_factor_after_cell_update)
        if (self.time > self.time_last_injection_dt_increase
                + self.varing_max_dt_after_injection):
            self.time_last_injection_dt_increase += (
                    self.varing_max_dt_after_injection)
            self.varing_max_dt_after_injection *= (
                    self.dt_increase_factor_after_injection)

        return next_dt

if __name__ == "__main__":
    import os
    from model_parts.drugs_in_vessels import DrugsInVessels
    from model_parts.cells import Cells
    from model_parts.vessels import Vessels
    from model_parts.oxygen import Oxygen
    from model_parts.helpers.mesh import BaseMesh

    set_log_level(LogLevel.WARNING)

    n = 10

    # Just to make sure vessel_init_filename and cell_init_filename is in the
    # database.
    sim_database = sim_db.add_empty_sim(False)
    sim_database.write("vessel_init_filename", "empty_vessel_init_file.txt", "string")
    sim_database.write("cell_init_filename", "empty_cell_init_file.txt", "string")
    sim_database.delete_from_database()
    sim_database.close()

    sim_database = sim_db.add_empty_sim(False)
    # DrugsInVessels
    sim_database.write("tot_time", 12, "float")
    sim_database.write("vac_dt_at_injection", 0.01, "float")
    sim_database.write("chemo_dt_at_injection", [0.01, 0.01, 0.01], "float array")
    sim_database.write("vac_max_dt", 10.0, "float")
    sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
    sim_database.write("height", 160, "float")
    sim_database.write("weight", 58.2, "float")
    dose = [600, 100, 600, 15]
    sim_database.write("dose", dose, "float array")
    schedule = [3, 3, 3, 3]
    sim_database.write("schedule", schedule, "float array")
    # BaseMesh
    sim_database.write("size", [n, n], "int array")
    sim_database.write("n_refinements", 2, "int")
    sim_database.write("cell_size", 10.0, "float")
    # Cells
    sim_database.write("seed", 1234, "int")
    sim_database.write("frac_cancer", 0.2, "float")
    sim_database.write("frac_normal", 0.1, "float")
    sim_database.write("n_refinements_cells", 0, "int")
    sim_database.write("t_cell", 14.69, "float")
    sim_database.write("k_phi", 1.4, "float")
    # Vessels
    sim_database.write("max_dt_drugs_in_vessels", 1.5, "float")
    sim_database.write("n_refinements_vessels", 0, "int")
    sim_database.write("n_other_rngs", 1000, "int")
    sim_database.write("vessel_sep", 3, "int")
    sim_database.write("init_vessel_density", 0.1197, "float")
    sim_database.write("avg_init_vessel_radius", 10.0, "float")
    sim_database.write("first_vessel_random", False, "bool")
    sim_database.write("p_max_sprout", 0.002, "float")
    sim_database.write("v_sprout", 0.5, "float")
    sim_database.write("vegf_low", 1e-6, "float")
    sim_database.write("vegf_high", 1e-4, "float")
    sim_database.write("p_death", 0.001, "float")
    sim_database.write("rel_radius_new_vessels", 1.0, "float")
    # Oxygen
    sim_database.write("degree_oxygen", 1, "int")
    sim_database.write("n_refinements_oxygen", 0, "int")
    sim_database.write("oxygen_dt_after_cell_update",1.0, "float")
    sim_database.write("oxygen_dt_increase_factor_after_injection", 1.05, "float")
    sim_database.write("oxygen_dt_increase_factor_after_cell_update", 1.05, "float")
    sim_database.write("oxygen_max_dt", 10.0, "float")
    # VEGF-Avastin
    sim_database.write("ktrans", 0.14, "float")
    sim_database.write("degree_vac", 1, "int")
    sim_database.write("n_refinements_vac", 0, "int")
    sim_database.write("vac_dt_after_cell_update", 1.0, "float")
    sim_database.write("vac_dt_increase_factor_after_injection", 1.05, "float")
    sim_database.write("vac_dt_increase_factor_after_cell_update", 1.05, "float")
    sim_database.write("vac_max_dt", 10.0, "float")

    base_mesh = BaseMesh(sim_database)
    drugs_in_vessels = DrugsInVessels(sim_database)
    cells = Cells(sim_database, base_mesh)
    vessels = Vessels(sim_database, base_mesh)
    oxygen = Oxygen(sim_database, base_mesh)
    vegf_avastin = VegfAvastin(sim_database, base_mesh, drugs_in_vessels)

    oxygen.update(30, True, vessels, cells)
    vegf_avastin.update(30, True, vessels, cells, oxygen)

    f = File("figs/vac.pvd")
    f << vegf_avastin.VAC.function

    vegf_avastin.update(60, False, vessels, cells, oxygen)

    f << vegf_avastin.VAC.function

    vegf_avastin.solve_steady_state(vessels, cells, oxygen)

    f = File("figs/vac_steady-state.pvd")
    f << vegf_avastin.VAC.function

    sim_database.delete_from_database()
    sim_database.close()

    list_timings(TimingClear.keep, [TimingType.wall])
