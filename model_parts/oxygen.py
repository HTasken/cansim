import __init__
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.load_function import load_function_if_checkpoint
from model_parts.helpers.checkpoint_time import get_time_last_checkpoint
from model_parts.helpers.checkpoint_time import get_varing_max_dt_after_injection
import model_parts.helpers.add_sim_db_to_path
import sim_db
import math
from dolfin import *

class Oxygen:
    def __init__(self, sim_database, base_mesh):

        # Read parameters from database.
        n_refinements = sim_database.read('n_refinements_oxygen')
        degree = sim_database.read('degree_oxygen')
        self.avg_init_vessel_radius = sim_database.read(
                'avg_init_vessel_radius')

        self.dt_after_cell_update = sim_database.read(
                'oxygen_dt_after_cell_update')
        self.dt_after_vessel_update = sim_database.read(
                'oxygen_dt_after_vessel_update')
        self.dt_increase_factor = sim_database.read(
                'oxygen_dt_increase_factor_after_cell_update')
        self.max_dt = sim_database.read('oxygen_max_dt')

        # Set time to last checkpoint of zero if no checkpoints exists.
        self.time = get_time_last_checkpoint(sim_database)
        self.varing_max_dt_after_vessel_update = self.dt_after_vessel_update

        # Using RefinedFunction allow a more accurate function and can be used
        # to check and adjust the error in spatial discritisation.
        self.concentration = RefinedFunction(base_mesh, n_refinements, degree,
                                  create_trial_function=True)

        # Load from checkpoint if any previous checkpoints.
        load_function_if_checkpoint(sim_database, self.concentration.function,
                                    "oxygen")

        # Height of model
        self.cell_size = Constant(sim_database.read('cell_size'))

        # Volume of model
        self.volume = Constant(self.cell_size**3)

        # Times at which the diffusion-reaction equation was solved.
        self.times_solved = []

        # Define Constants
        self.P = Constant(6*10**3) # Permeability
        self.D = Constant(1.05*10**(5)) # Diffusion coefficient
        self.K0 = Constant(20) # Oxygen concentration in blood
        self.K1 = Constant(2.5) # Half-maximal o2 concentration
        self.phi = Constant(15*60) # Consumption by cells

    def update(self, end_time, is_vessels_updated, vessels, cells):
        """Solves a diffusion eq. for oxygen conc. for time interval.

        Diffusion equation:
            d K/dt = D*nabla^2*K - phi*K/(K0 - K)*delta_func(x - x_cell)
                   - (2*pi*avg_init_radius*vessel_length*ktrans/volume
                      *relative_radius*delta_func(x - x_vessel))
        where the relative_radius is the radius of a vessel divided the average
        radius of the initial vessels.
        
        Args:
            end_time (float): Time of the final solution of K.
        """
        timer_update = Timer("Update Oxygen")

        # Define functions used in the equation.
        v = self.concentration.test_function
        k = self.concentration.function
        k_prev = self.concentration.prev_function
        theta = 0.5
        k_mid = (1 - theta)*k_prev + theta*k

        # Delta function for vessels times the relative radius of the vessel.
        timer = Timer("Project")
        if is_vessels_updated:
            self.vessel_location_rel_radius = interpolate(
                                    vessels.rel_radius_delta_function.function, self.concentration.function_space)
            vessel_local = self.vessel_location_rel_radius.vector().get_local()
            indices_to_one = vessel_local > 0
            vessel_local[indices_to_one] = 1
            self.vessel_location_rel_radius.vector().set_local(vessel_local)
        # Delta function for cells.
        cell_location = interpolate(cells.delta_function.function, self.concentration.function_space)
        timer.stop()
        # Set time-step parameters
        varing_max_dt = self.dt_after_cell_update
        if varing_max_dt > self.max_dt:
            varing_max_dt = self.max_dt
        dt = Constant(varing_max_dt)
        time_last_max_dt_increase = self.time

        # Set up variational form of the equation for the oxygen concentration.
        F = -(k - k_prev)*v*dx() \
            -dt*inner(self.D*grad(k_mid), grad(v))*dx() \
            +(dt*Constant(2*math.pi*self.avg_init_vessel_radius*self.cell_size) \
               *self.P/self.volume \
               *(self.K0 - k_mid)*self.vessel_location_rel_radius*v*dx()) \
            - Constant(0.5)*dt*self.phi*(k/(self.K1 + k) + k_prev/(self.K1 + k_prev))*cell_location*v*dx()
        dF = derivative(F, k, self.concentration.trial_function)

        problem = NonlinearVariationalProblem(F, k, None, dF)
        solver = NonlinearVariationalSolver(problem)

        prm = solver.parameters
        prm["newton_solver"]["linear_solver"] = "cg"
        prm["newton_solver"]["preconditioner"] = "jacobi"
        prm["newton_solver"]["relaxation_parameter"] = 1.0
        prm["newton_solver"]["convergence_criterion"] = "incremental"
        prm["newton_solver"]["krylov_solver"]["nonzero_initial_guess"] = False
        prm["newton_solver"]["krylov_solver"]["relative_tolerance"] = 1e-7
        prm["newton_solver"]["krylov_solver"]["absolute_tolerance"] = 1e-8        
        prm["newton_solver"]["relative_tolerance"] = 1e-8
        prm["newton_solver"]["absolute_tolerance"] = 1e-7
        prm["newton_solver"]["maximum_iterations"] = 25
#        prm["newton_solver"]["preconditioner"]["structure"] = "same_nonzero_pattern"
#        prm["newton_solver"]["preconditioner"]["ilu"]["fill_level"] =0
        # Solve the equation for the time interval.
        while self.time < end_time:
            timer = Timer("Solve Oxygen Full")
            solver.solve()
            timer.stop()
            k_prev.assign(k)
            self.times_solved.append(self.time)

            # Update time-step and time
            if (self.time - time_last_max_dt_increase > varing_max_dt):
                if varing_max_dt < self.max_dt:
                    varing_max_dt *= self.dt_increase_factor
                if varing_max_dt >= self.max_dt:
                    varing_max_dt = self.max_dt
                time_last_max_dt_increase = self.time
                dt.assign(Constant(varing_max_dt))
            self.time += varing_max_dt

        # Solve for concentration at end_time.
        self.time -= varing_max_dt
        dt.assign(Constant(end_time - self.time))
        timer = Timer("Solve Oxygen Full")
        solver.solve()
        timer.stop()
        k_prev.assign(k)
        self.times_solved.append(end_time)
        self.time = end_time
        timer_update.stop()

    def update_newton(self, end_time, is_vessels_updated, vessels, cells):
        """Solves a linearized diffusion eq. for oxygen conc. for time interval.

        Diffusion equation:
            d K/dt = D*nabla^2*K - phi*K/(K0 - K)*delta_func(x - x_cell)
                   - (2*pi*avg_init_radius*vessel_length*ktrans/volume
                      *relative_radius*delta_func(x - x_vessel))
        where the relative_radius is the radius of a vessel divided the average
        radius of the initial vessels.
        
        Args:
            end_time (float): Time of the final solution of K.
        """
        timer_update = Timer("Update Oxygen")

        # Define functions used in the equation.
        v = self.concentration.test_function
        k = self.concentration.function
        k_prev = self.concentration.prev_function
        theta = 0.5
        k_mid = (1 - theta)*k_prev + theta*k

        # Delta function for vessels times the relative radius of the vessel.
        timer = Timer("Project")
        if is_vessels_updated:
            self.vessel_location_rel_radius = interpolate(
                                    vessels.rel_radius_delta_function.function, self.concentration.function_space)
            vessel_local = self.vessel_location_rel_radius.vector().get_local()
            indices_to_one = vessel_local > 0
            vessel_local[indices_to_one] = 1
            self.vessel_location_rel_radius.vector().set_local(vessel_local)
        # Delta function for cells.
        cell_location = interpolate(cells.delta_function.function, self.concentration.function_space)
        timer.stop()
        # Set time-step parameters
        varing_max_dt = self.dt_after_cell_update
        if varing_max_dt > self.max_dt:
            varing_max_dt = self.max_dt
        dt = Constant(varing_max_dt)
        time_last_max_dt_increase = self.time

        # Set up variational form of the equation for the oxygen concentration.
        F = -(k - k_prev)*v*dx() \
            - dt*inner(self.D*grad(k_mid), grad(v))*dx() \
            + (dt*Constant(2*math.pi*self.avg_init_vessel_radius*self.cell_size) \
               *self.P/self.volume \
               *(self.K0 - k_mid)*self.vessel_location_rel_radius*v*dx()) \
            - Constant(0.5)*dt*self.phi*(k/(self.K1 + k) + k_prev/(self.K1 + k_prev))*cell_location*v*dx()

        dF = derivative(F, k, self.concentration.trial_function)

        problem = NonlinearVariationalProblem(F, k, None, dF)
        solver = NonlinearVariationalSolver(problem)

        prm = solver.parameters
        prm["newton_solver"]["linear_solver"] = "cg"
        prm["newton_solver"]["preconditioner"] = "jacobi"
        prm["newton_solver"]["relaxation_parameter"] = 1.0
        prm["newton_solver"]["convergence_criterion"] = "incremental"
        prm["newton_solver"]["krylov_solver"]["nonzero_initial_guess"] = False
        prm["newton_solver"]["krylov_solver"]["relative_tolerance"] = 1e-7
        prm["newton_solver"]["krylov_solver"]["absolute_tolerance"] = 1e-8        
        prm["newton_solver"]["relative_tolerance"] = 1e-7
        prm["newton_solver"]["absolute_tolerance"] = 1e-8
        prm["newton_solver"]["maximum_iterations"] = 1
        prm["newton_solver"]["error_on_nonconvergence"]=False
#        prm["newton_solver"]["preconditioner"]["structure"] = "same_nonzero_pattern"
#        prm["newton_solver"]["preconditioner"]["ilu"]["fill_level"] =0
        # Solve the equation for the time interval.
        while self.time < end_time:
            timer = Timer("Solve Oxygen 1-step")
            solver.solve()
            timer.stop()
            k_prev.assign(k)
            self.times_solved.append(self.time)

            # Update time-step and time
            if (self.time - time_last_max_dt_increase > varing_max_dt):
                if varing_max_dt < self.max_dt:
                    varing_max_dt *= self.dt_increase_factor
                if varing_max_dt >= self.max_dt:
                    varing_max_dt = self.max_dt
                time_last_max_dt_increase = self.time
                dt.assign(Constant(varing_max_dt))
            self.time += varing_max_dt

        # Solve for concentration at end_time.
        self.time -= varing_max_dt
        dt.assign(Constant(end_time - self.time))
        timer = Timer("Solve Oxygen 1-step")
        solver.solve()
        timer.stop()
        k_prev.assign(k)
        self.times_solved.append(end_time)
        self.time = end_time
        timer_update.stop()

    def update_linearized(self, end_time, is_vessels_updated, vessels, cells):
        """Solves a linearized diffusion eq. for oxygen conc. for time interval.

        Diffusion equation:
            d K/dt = D*nabla^2*K - phi*K/(K0 - K)*delta_func(x - x_cell)
                   - (2*pi*avg_init_radius*vessel_length*ktrans/volume
                      *relative_radius*delta_func(x - x_vessel))
        where the relative_radius is the radius of a vessel divided the average
        radius of the initial vessels.
        
        Args:
            end_time (float): Time of the final solution of K.
        """

        PETScOptions.set("ksp_max_it", 5000)
        PETScOptions.set("ksp_rtol", 1e-8)
        PETScOptions.set("ksp_atol", 1e-8)
#        PETScOptions.set("ksp_view")
#        PETScOptions.set("ksp_converged_reason")
#        PETScOptions.set("ksp_monitor_true_residual")
#        PETScOptions.set("ksp_initial_guess_nonzero",True)
        PETScOptions.set("ksp_type", "cg")
        PETScOptions.set("pc_type", "hypre")
        PETScOptions.set("pc_hypre_type", "boomeramg")
        PETScOptions.set("pc_hypre_boomeramg_strong_threshold", 0.5)
#        PETScOptions.set("pc_hypre_boomeramg_agg_nl",2)        
#        PETScOptions.set("pc_hypre_boomeramg_interp_type","ext+i")
        timer_update = Timer("Update Oxygen")

        # Define functions used in the equation.
        v = self.concentration.test_function
        dk = self.concentration.trial_function
        k = self.concentration.trial_function
        k_prev = self.concentration.prev_function
        theta = 0.5
        k_mid = (1 - theta)*k_prev + theta*k

        # Delta function for vessels times the relative radius of the vessel.
        timer = Timer("Project")
        if is_vessels_updated:
            self.vessel_location_rel_radius = interpolate(
                                    vessels.rel_radius_delta_function.function, self.concentration.function_space)
            vessel_local = self.vessel_location_rel_radius.vector().get_local()
            indices_to_one = vessel_local > 0
            vessel_local[indices_to_one] = 1
            self.vessel_location_rel_radius.vector().set_local(vessel_local)
        # Delta function for cells.
        cell_location = interpolate(cells.delta_function.function, self.concentration.function_space)
        timer.stop()
        # Set time-step parameters
        varing_max_dt = self.dt_after_cell_update
        if varing_max_dt > self.max_dt:
            varing_max_dt = self.max_dt
        dt = Constant(varing_max_dt)
        time_last_max_dt_increase = self.time

        # Set up variational form of the equation for the oxygen concentration.
    #    C1 = Constant(2*math.pi*self.avg_init_vessel_radius*self.cell_size)*self.P/self.volume

        F = -(k - k_prev)*v*dx() \
            - dt*inner(self.D*grad(k_mid), grad(v))*dx() \
            + (dt*Constant(2*math.pi*self.avg_init_vessel_radius*self.cell_size) \
               *self.P/self.volume \
               *(self.K0 - k_mid)*self.vessel_location_rel_radius*v*dx()) \
            - dt*self.phi*k_mid/(self.K1 + k_prev)*cell_location*v*dx()

#        a = k*v*dx + 0.5*dt*inner(self.D*grad(k),grad(v))*dx - dt*Constant(2*math.pi*self.avg_init_vessel_radius*self.cell_size) \
#               *self.P/self.volume *(self.K0 - k)*self.vessel_location_rel_radius*v*dx
#        L = k_prev*v*dx -0.5*dt*inner(self.D*grad(k_prev),grad(v))*dx \
#            - dt*self.phi*k_prev/(self.K1 + k_prev)*cell_location*v*dx  +\
#            dt*Constant(2*math.pi*self.avg_init_vessel_radius*self.cell_size) \
#            *self.P/self.volume *(self.K0 - k_prev)*self.vessel_location_rel_radius*v*dx  
#        a = (dk*v/dt + Constant(0.5)*self.D*inner(grad(dk), grad(v)) + \
#            Constant(0.5)*self.phi*cell_location*self.K1/(self.K1 + k_prev)**2*dk*v + \
#            Constant(0.5)*C1*self.vessel_location_rel_radius*dk*v)*dx
#        L = (-self.D*inner(grad(k_prev),grad(v)) + self.phi*cell_location*self.K1/(self.K1 + k_prev)*v + \
#            -C1*self.vessel_location_rel_radius*k_prev*v + C1*self.vessel_location_rel_radius*self.K0*v - self.phi*cell_location*v)*dx
        timer = Timer("Assemble Oxygen A")
        (a, L) = system(F)
        timer.stop()
        solver = PETScKrylovSolver()
        solver.set_from_options()
        while self.time < end_time + 1e-9:
            timer = Timer("Assemble Oxygen b")
            A, b = assemble_system(a, L, None)
            timer.stop()
            timer = Timer("Solve Oxygen")
#            solve(A,self.concentration.function.vector(), b)
            num_iter = solver.solve(A, self.concentration.function.vector(), b)
#            self.concentration.function.vector()[:] += dk.vector()
            timer.stop()
            self.times_solved.append(self.time)
            k_prev.assign(self.concentration.function)

            # Update time and time-step variables
            next_dt = self.dt_after_cell_update
            dt.assign(Constant(next_dt))
            self.time += next_dt

        self.time -= next_dt
        timer_update.stop()
#        return num_iter        
        
    def solve_steady_state(self, vessels, cells, time_update_first=None,
                           first_dt=None):
        """Solves the steady-state version of the PDE for oxygen concentration.

        Also sets the previous oxygen concentration to the same value.

        Diffusion equation:
            d K/dt = D*nabla^2*K - phi*K/(K0 - K)*delta_func(x - x_cell)
                   - (P*2*pi*avg_init_radius*length_vessels/volume
                      *relative_radius*delta_func(x - x_vessel))
        where the relative_radius is the radius of a vessel divided the average
        radius of the initial vessels.

        Args:
             time_update_first (float): Run update() until 'time_update_first'
                                       before solving the steady-state.
            first_dt (float): Time-step used when first calling update().
        """
        # Solve the time-dependent equation untils 'time_update_first' to have
        # a oxygen concentration that converges easier into the steady-state
        # equation.
        if time_update_first != None:
            tmp_time = self.time
            tmp_times_solved = list(self.times_solved)
            tmp_dt_after_cell_update = self.dt_after_cell_update
            if first_dt != None:
                self.dt_after_cell_update = first_dt
            self.update(time_update_first + self.time, True, vessels, cells)
            self.time = tmp_time
            self.times_solved = tmp_times_solved
            self.dt_after_cell_update = self.dt_after_cell_update

        # Define functions used in the equation.
        v = self.concentration.test_function
        k = self.concentration.function

        # Delta function for vessels times the relative radius of the vessel.
        self.vessel_location_rel_radius = interpolate(
                                    vessels.rel_radius_delta_function.function, self.concentration.function_space)
        vessel_local = self.vessel_location_rel_radius.vector().get_local()
        print('Total relative vessel {}'.format(sum(vessel_local)))
        indices_to_one = vessel_local > 0
        vessel_local[indices_to_one] = 1
        self.vessel_location_rel_radius.vector().set_local(vessel_local)
        print('Total absolute vessel {}'.format(sum(self.vessel_location_rel_radius.vector())))
        # Delta function for cells.
        cell_location = interpolate(cells.delta_function.function, self.concentration.function_space)

        # Set up variational form of the steady-state equation.
        F = (- inner(self.D*grad(k), grad(v))*dx()
            + (Constant(2*math.pi*self.avg_init_vessel_radius*self.cell_size)
               *self.P/self.volume
               *(self.K0 - k)*self.vessel_location_rel_radius*v*dx())
            - self.phi*k/(self.K1 + k)*cell_location*v*dx())

        # Solve the steady-state equation.

        solve(F==0, k, solver_parameters={"newton_solver":
                                        {"linear_solver": "cg", "preconditioner":"hypre_amg"}})

        # Update previous concentration
        self.concentration.prev_function.assign(k)
    def __get_next_dt(self, end_time):
        if (self.varing_max_dt_after_vessel_update
                < self.varing_max_dt_after_injection
                and self.varing_max_dt_after_vessel_update < self.max_dt):
            next_dt = self.varing_max_dt_after_vessel_update
        elif (self.varing_max_dt_after_injection < self.max_dt):
            next_dt = self.varing_max_dt_after_injection
        else:
            next_dt = self.max_dt
        if (self.time + next_dt > end_time and self.time < end_time - 1e-9):
            next_dt = end_time - self.time
        elif (self.time + next_dt
                >= self.time_next_injection - self.dt_at_injection):
            next_dt = self.time_next_injection - self.dt_at_injection - self.time
            self.time_next_injection += self.dt_injections
            self.varing_max_dt_after_injection = self.dt_at_injection
        if (self.time > self.time_last_vessel_update_dt_increase
                + self.varing_max_dt_after_vessel_update):
            self.time_last_vessel_update_dt_increase += (
                    self.varing_max_dt_after_vessel_update)
            self.varing_max_dt_after_vessel_update *= (
                    self.dt_increase_factor_after_vessel_update)
        if (self.time > self.time_last_injection_dt_increase
                + self.varing_max_dt_after_injection):
            self.time_last_injection_dt_increase += (
                    self.varing_max_dt_after_injection)
            self.varing_max_dt_after_injection *= (
                    self.dt_increase_factor_after_injection)

        return next_dt
if __name__ == '__main__':
    import os
    from model_parts.drugs_in_vessels import DrugsInVessels
    from model_parts.cells import Cells
    from model_parts.vessels import Vessels
    from model_parts.helpers.mesh import BaseMesh

    n = 10

    # Just to make sure vessel_init_filename and cell_init_filename is in the
    # database.
    sim_database = sim_db.add_empty_sim(False)
    sim_database.write("vessel_init_filename", "empty_vessel_init_file.txt", "string")
    sim_database.write("cell_init_filename", "empty_cell_init_file.txt", "string")
    sim_database.delete_from_database()
    sim_database.close()

    sim_database = sim_db.add_empty_sim(False)
    # DrugsInVessels
    sim_database.write("tot_time", 12, "float")
    sim_database.write("vac_dt_at_injection", 0.01, "float")
    sim_database.write("chemo_dt_at_injection", [0.01, 0.01, 0.01], "float array")
    sim_database.write("vac_max_dt", 10.0, "float")
    sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
    sim_database.write("height", 160, "float")
    sim_database.write("weight", 58.2, "float")
    dose = [600, 100, 600, 15]
    sim_database.write("dose", dose, "float array")
    schedule = [3, 3, 3, 3]
    sim_database.write("schedule", schedule, "float array")
    # BaseMesh
    sim_database.write("size", [n, n], "int array")
    sim_database.write("n_refinements", 2, "int")
    sim_database.write("cell_size", 10.0, "float")
    # Cells
    sim_database.write("seed", 1234, "int")
    sim_database.write("frac_cancer", 0.2, "float")
    sim_database.write("frac_normal", 0.1, "float")
    sim_database.write("n_refinements_cells", 0, "int")
    sim_database.write("t_cell", 14.69, "float")
    sim_database.write("k_phi", 1.4, "float")
    # Vessels
    sim_database.write("max_dt_drugs_in_vessels", 1.5, "float")
    sim_database.write("n_refinements_vessels", 0, "int")
    sim_database.write("n_other_rngs", 1000, "int")
    sim_database.write("vessel_sep", 3, "int")
    sim_database.write("init_vessel_density", 0.1197, "float")
    sim_database.write("avg_init_vessel_radius", 10.0, "float")
    sim_database.write("first_vessel_random", False, "bool")
    sim_database.write("p_max_sprout", 0.002, "float")
    sim_database.write("v_sprout", 0.5, "float")
    sim_database.write("vegf_low", 1e-6, "float")
    sim_database.write("vegf_high", 1e-4, "float")
    sim_database.write("p_death", 0.001, "float")
    sim_database.write("rel_radius_new_vessels", 1.0, "float")
    # Oxygen
    sim_database.write("degree_oxygen", 1, "int")
    sim_database.write("n_refinements_oxygen", 0, "int")
    sim_database.write("oxygen_dt_after_cell_update",1.0, "float")
    sim_database.write("oxygen_dt_increase_factor_after_cell_update", 1.10, "float")
    sim_database.write("oxygen_max_dt", 10.0, "float")

    base_mesh = BaseMesh(sim_database)
    drugs_in_vessels = DrugsInVessels(sim_database)
    cells = Cells(sim_database, base_mesh)
    vessels = Vessels(sim_database, base_mesh)
    oxygen = Oxygen(sim_database, base_mesh)
    oxygen.update(0, 30, vessels, cells)

    f = File("figs/oxygen.pvd")
    f << oxygen.concentration.function

    oxygen.update(30, 60, vessels, cells)

    f << oxygen.concentration.function

    oxygen.solve_steady_state(vessels, cells)

    f = File("figs/oxygen_steady_state.pvd")
    f << oxygen.concentration.function

    sim_database.delete_from_database()
    sim_database.close()

    list_timings(TimingClear.keep, [TimingType.wall])
