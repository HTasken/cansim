import __init__
from model_parts.helpers.neighbours_mpi import LocalAndNeighbours
from model_parts.helpers.random_numbers import random_number_generator_state
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.delta_function import DeltaFunction
from model_parts.helpers.mesh import BaseMesh
from model_parts.helpers.load_function import load_function_if_checkpoint
import model_parts.helpers.add_sim_db_to_path
import sim_db
import numpy as np
from input_output import log
from scipy.stats import beta
from scipy.integrate import odeint
import sys
import os
import warnings
from dolfin import *

class Cells:
    """Cellular automata containing cancer and normal cells.

    - Keeps track of the cell cycle of the cancer cells and calculated the time
      of proliferation.
    - Model the proliferation and death of cancer cells.
    - Provides a FEniCS function of where the cells are for use as sinks or
      sources in other PDEs in the model (cells got volume 1 with exception).
    """
    def __init__(self, sim_database, base_mesh):
        """Initialise the cellular automata from a configuration file.

        The configuration filename is sim_database.read('cell_init_filename'), 
        and contain the location of types of cells. The numbers in the file
        is stored as row-major and correspont to the gridpoints in the mesh. 
        0 -> no cell, 1 -> cancer cell, >1 -> normal cell.

        Initialise the cell cycle randomly for each cell.

        The function representing a cell has shape like a square pyramid, where
        increasing sim_database.read('n_refinements') reduses the base and 
        increases the height. The volume per cell is 1.

        Args:
            sim_database (SimDB): Contain all simulation parameters.
            base_mesh (BaseMesh): Should be refined at least twice.
        """
        # Length of cell cycle in minutes.
        self.t_cell = sim_database.read('t_cell')*24*60
        self.res_dir = sim_database.read("results_dir")
        # Used in calculationg cell cycle. Given in mmHg.
        self.k_phi = sim_database.read('k_phi')

        # Number of extra refinements done on the mesh for the cell function
        # used as source and sink in the other PDEs, where each refinement half
        # the distance between the gridpoints.
        n_refinements = sim_database.read('n_refinements_cells')

        self.comm = MPI.comm_world
        n_tot_refinements = base_mesh.n_refinements + n_refinements
        if n_tot_refinements == 0:
            warnings.warn("The volume of the cells may deviate for one, " \
                              "as the mesh is not refined.")

        seed = sim_database.read('seed')

        # Function used to represent the cellular automata, with the values at
        # each gridpoint representing the type of cell.
        # 0 -> no cell, 1 -> cancer cell, 2 -> normal cell
        self.automata = RefinedFunction(base_mesh, n_refinements=0)

        # Load from checkpoint if there exists any previous checkpoint.
        from_checkpoint = load_function_if_checkpoint(
                sim_database, self.automata.function, "cells")

        # Read the initialization values of the cellular automata if not from
        # checkpoint.
        if not from_checkpoint:
            cell_init_filename = sim_database.read('cell_init_filename')
            if cell_init_filename == None:
                size = sim_database.read('size')
                frac_cancer = sim_database.read('frac_cancer')
                frac_normal = sim_database.read('frac_normal')
                timer = Timer("Generate Cell Placement")
                cell_init = generate_cell_placement(size, frac_cancer,
                                                    frac_normal, seed)
                timer.stop()
            else:
                try:
                    with open(cell_init_filename,'r') as f:
                        cell_init = [line.strip().split(',') for line in f]
                except:
                    raise Exception(
                            "Could NOT open {}.".format(cell_init_filename))
            cell_init = np.array(cell_init, dtype=float)
            cell_init[cell_init > 1] = 2

            self.automata.set(cell_init)

        # Number and coordinates of the possible, local cell positions.
        self.n_potential_local_cells = self.automata.n_potential_local_cells
        self.x_local_cells = self.automata.local_x_at_cell_positions
        self.y_local_cells = self.automata.local_y_at_cell_positions

        # Create cell function to use as sinks and sources when solving the
        # other PDEs.
        # Each cell is represented by a delta function and modelled as a square
        # pyramid, unless number of total refinements ('n_tot_refinements') is
        # zero. In that case the cells are six sided, slighty squeezed pyramids.
        self.delta_function = DeltaFunction(self.automata, base_mesh, n_refinements)

        # Initialise location of cells from automata.
        self.delta_function.update()

        # Concentration of p53 and VEGF inside the cells.
        self.p53 = RefinedFunction(base_mesh, n_refinements=0)
        self.vegf = RefinedFunction(base_mesh, n_refinements=0)
        
        # Constants used to solve the equation with p53.
        self.k_7 = 0.0001
        self.k_77 = 0.1
        self.k_p53 = 0.01

        # Constants used to solve the equation with VEGF.
        self.k_8 = 0.002
        self.k_88 = 0.01
        self.k_888 = -0.00002
        self.j_5 = 0.04
        self.k_vegf = 0.01

        self.p53_vegf_time = 0
        self.cycle_time = 0

        # Get a unique number generator state with non-overlapping sequences
        # for each coordinate.
        self.rngs = [[None] for i in range(self.n_potential_local_cells)]

        if seed == None:
            if MPI.comm_world.Get_rank() == 0:
                seed = int(time.time())
                sim_database.write('seed', seed, 'int')
            seed = MPI.comm_world.bcast(seed, root=0)
        for i in range(self.n_potential_local_cells):
            self.rngs[i] = random_number_generator_state(self.x_local_cells[i],
                    self.y_local_cells[i], base_mesh.size[0],
                    base_mesh.cell_size, 0, seed)

        # Initialise cell cycle randomly
        self.cell_cycle = RefinedFunction(base_mesh, n_refinements=0)
        local_cellcycle_value = self.cell_cycle.get_local_at_cell_positions()
        for i in self.local_cancer_indices():
            local_cellcycle_value[i] = self.rngs[i].rand_uniform_double()
        self.cell_cycle.set_local_at_cell_positions(local_cellcycle_value)

    def local_cancer_indices(self):
        local_cancer_indices = np.arange(self.automata.n_potential_local_cells)
        local_values = self.automata.get_local_at_cell_positions()
        return local_cancer_indices[np.rint(local_values) == 1]

    def update_cell_cycle(self, end_time, oxygen):
        """Update the cell cycle of the local cancer cells to end_time."""

        local_oxygen = oxygen.concentration.get_local_at_cell_positions()
        local_values = self.cell_cycle.get_local_at_cell_positions()        
        # Vectorized loop over the local cancer indices
        i = self.local_cancer_indices()
        dt = end_time - self.cycle_time
        local_values[i] += (local_oxygen[i]
                            /(self.t_cell*(self.k_phi + local_oxygen[i]))*dt)
        self.cell_cycle.set_local_at_cell_positions(local_values)
        self.cycle_time = end_time

    def update_vegf(self, end_time, oxygen):
        """Solve the equations of VEFG and p53 concentration inside cell.

        The equtions are solved from the internal time 'self.vegf_time' to i
        'end_time' with 'dt_vegf' time steps. The internal values of 'self.p53' 
        and 'self.vegf' for each cell are used as initial values.

        scipy.integrate.ode with integrator 'vode' and method 'adams' is used 
        to solve the equation for each cancer cell.
        """
        timer = Timer("Update Subcellular")
        log.log('Update subcellular', 'a', self.res_dir)
        local_o2 = oxygen.concentration.get_local_at_cell_positions()/20.*0.045
        local_p53 = self.p53.get_local_at_cell_positions()
        local_vegf = self.vegf.get_local_at_cell_positions()
        log.log('Update subcellular p53', 'a', self.res_dir)
        update_p53 = np.vectorize(self.__p53_at_t)      
        p53_at_end_time = update_p53(end_time, local_p53, local_o2)
        log.log('Update subcellular vegf', 'a', self.res_dir)
        vegf_at_end_time = np.zeros(self.n_potential_local_cells)
        for i in np.where(self.automata.get_local_at_cell_positions() > 0.5)[0]:   
            fun=lambda y, t: self.__rhs_p53_vegf_ode(y, t, p53_at_end_time[i], local_o2[i])
            sol = odeint(fun, local_vegf[i],  [self.p53_vegf_time, end_time])
            vegf_at_end_time[i] = sol[1]

        self.p53.set_local_at_cell_positions(p53_at_end_time)
        self.vegf.set_local_at_cell_positions(vegf_at_end_time)
        self.p53_vegf_time = end_time
        timer.stop()

    def __p53_at_t(self, t, local_p53, local_o2):
        B = self.k_77/(self.k_p53/local_o2 + 1.0)       
        if B*(t - self.p53_vegf_time) < 1e-4:
            return local_p53*(1 + B*(self.p53_vegf_time - t)) - self.k_7*(self.p53_vegf_time - t)
        return self.k_7/B + (local_p53 - self.k_7/B) * \
                np.exp(B*(self.p53_vegf_time - t))

    def __rhs_p53_vegf_ode(self, vegf, t, p53, o2):
        """Right hand side of the couple ODE's for the P53 and VEGF.

        Coupled ODE's: 
            d p53/dt = k_7 - k_77*o2/(k_p53 + o2)*p53 
            d vegf/dt = k_8 + k_888*p53*vegf/(j_5 + vegf) 
                      - k_88*o2/(k_vegf + o2)*vegf
        Args:
            p53_vegf (list): [p53, vegf]
        """
    
        return self.k_8 + self.k_888*self.__p53_at_t(t, p53, o2)*vegf/(self.j_5 + vegf) - \
            self.k_88*vegf*o2/(self.k_vegf + o2)

    def solve_vegf_steady_state(self, oxygen):
        """Solve the steady-state vesion of intracellular VEFG and p53 ODEs."""
        local_o2 = oxygen.concentration.get_local_at_cell_positions()/20.*0.045

        # Steady state p53
        # 0 = k_7 - k_77*o2/(k_p53 + o2)*p53
        # => p53 = k_7*(1.0 + k_p53/o2)/k_77
        local_p53 = (self.k_7/self.k_77)*(1.0 + self.k_p53/local_o2)

        # Steady state VEGF
        # 0 = k_8 + k_888*p53*vegf/(j_5 + vegf) - k_88*o2*vegf/(k_vegf + o2)
        # *(j_5 + vegf) => 0 -k_88*o2/(k_vegf + o2)*vegf^2
        # + (k_8 + k_888*p53 - k_88*j_5*o2/(k_vegf + o2))*vegf + k_8*j_5 = 0
        # vegf = (-b + sqrt(b^2 - 4*a*c))/(2*a), where
        # a = -k_88*o2/(k_vegf + o2)
        # b = k_8 + k_888*p53 - k_88*o2*j_5/(k_vegf + o2)
        # c = k_8*j_5
        local_a = -self.k_88*local_o2/(self.k_vegf + local_o2)
        local_b = (self.k_8 + self.k_888*local_p53 + local_a*self.j_5)
        c = self.k_8*self.j_5
        vegf_root1 = (-local_b - np.sqrt(local_b*local_b - 4*local_a*c))/(2*local_a)
        vegf_root2 = (-local_b + np.sqrt(local_b*local_b - 4*local_a*c))/(2*local_a)
        # Get the positive root of the two
        local_vegf = np.maximum(vegf_root1,vegf_root2)

        self.p53.set_local_at_cell_positions(local_p53)
        self.vegf.set_local_at_cell_positions(local_vegf)

    def set_vegf_initial_values(self, index, vegf_value, time):
        """Set initial values of the VEGF ode.

        Args:
            index (int): Index of local cell the equation belong to.
            vegf_value (float): Initial VEGF value.
            time (float): Time of the inital value, 'vegf_value'.
        """
        self.vegf_ode_solvers[index].set_initial_value(vegf_value, time)

    def update_cancer_cells(self, end_time, oxygen, *chemos):
        """Proliferate and kill cancer cells.

        Start by updating cell cylce to 'end_time'.

        Cancer cell with cell cycle above 1 is at proliferation. These cancer 
        cells are killed with a probatility given by the beta_function(x, 1, b),
        where x is chemotherapy concentration fraction of highest concentration
        in the vessels and b in the chemotherapy sensitivity.
            The living cancer cells at proliferation add a new cancer cell in 
        the available neighbour spot with highest oxygen concentration, and for
        conflicts the one with highest cell cycle value is choosen. If no 
        neighbour spots are available no cancer cells are placed.
        """
        timer = Timer("Update Cells")
        # Update cell cylce
        self.update_cell_cycle(end_time, oxygen)
        # Kill proliferating cells with a certain probatility.
        # Only check the proliferating cancer cells.
        proliferation_indices = np.arange(self.automata.n_potential_local_cells)
        i = self.local_cancer_indices()
        local_cellcycle_value = self.cell_cycle.get_local_at_cell_positions()
        proliferation_indices = proliferation_indices[i][local_cellcycle_value[i] >= 1.0]
        automata_local_values = self.automata.get_local_at_cell_positions()
        for chemo in chemos:
            local_chemo_values = chemo.concentration.get_local_at_cell_positions()
            for i in proliferation_indices:
                p = beta.cdf(local_chemo_values[i]/chemo.peak_vessel_concentration,
                             1, chemo.sensitivity)
                self.rngs[i].set_binomial_int(1, p)
                is_killed = self.rngs[i].rand_binomial_int()
                if is_killed:
                    automata_local_values[i] = 0
                    local_cellcycle_value[i] = 0.0
        self.automata.set_local_at_cell_positions(automata_local_values)
        self.cell_cycle.set_local_at_cell_positions(local_cellcycle_value)
        # Add new cancer cells

        # Only check the proliferating cancer cells.
        proliferation_indices = np.arange(self.automata.n_potential_local_cells)
        i = self.local_cancer_indices()
        proliferation_indices = proliferation_indices[i][local_cellcycle_value[i] >= 1.0]
        # Scale coordinates to integers to be able to search for them.
        self.scale = self.automata.local_and_neighbours.scale
        x_scaled = np.rint(self.x_local_cells*self.scale).astype(int)
        y_scaled = np.rint(self.y_local_cells*self.scale).astype(int)
        local_coord_proliferating_scaled = list(zip(x_scaled, y_scaled))

        # Priorities the ones with highest cell cycle value.
        # Convert it to largest integer assuming it is less than 1.5.
        self.priorities_proliferating  = ((local_cellcycle_value - 1.0 + 1.0/sys.maxsize)
                                       *2*sys.maxsize).astype(int)

        # self.coord_prev_tried_proliferating[i] contain a list of coordinates
        # proliferating cancer cell 'i' previously tried to place a new cancer cell.
        self.coord_prev_tried_proliferating = (
                [[] for i in range(self.automata.n_potential_local_cells)])

        oxygen.concentration.collect_values()

        # Place new cancer cells equivalenty to doing so in order of cell cycle value.
        # New cells are tried placed with priority based on cell cycle value and
        # the ones that could not be placed are
        indices_not_yet_proliferated = proliferation_indices
        while not self.__is_empty_on_all_processes(indices_not_yet_proliferated):
            self.automata.collect_values()
            self.__add_new_cancer_cells_locally(oxygen, indices_not_yet_proliferated)
            not_set = self.automata.return_values(reset_priorities=False)

            if not_set:
                identifiers_not_set = list(list(zip(*not_set))[1]) # Unzipped
            else:
                identifiers_not_set = []
            indices_not_yet_proliferated = []

            for identifier in identifiers_not_set:
                x = identifier % (self.automata.size[0]*self.automata.cell_size)
                y = int(identifier/(self.automata.size[0]*self.automata.cell_size))
                x_scaled = int(round(x*self.scale))
                y_scaled = int(round(y*self.scale))
                i = local_coord_proliferating_scaled.index((x_scaled, y_scaled))
                indices_not_yet_proliferated.append(i)

        self.automata.reset_priorities()

        # Set automata, vegf, p53 and cell cycle value of the new cancer cell
        # equal to the parent cancer cells.
        local_automata_values = self.automata.get_local_at_cell_positions()
        local_vegf_values = self.vegf.get_local_at_cell_positions()
        local_p53_values = self.p53.get_local_at_cell_positions()
        local_cellcycle_values = self.cell_cycle.get_local_at_cell_positions()        
        self.vegf.collect_values()
        self.p53.collect_values()
        for i in np.where(local_automata_values < -0.5)[0]:
            value = -local_automata_values[i] - 1
            local_cellcycle_values[i] = value % 1.0
            value = int(round(value - local_cellcycle_values[i]))
            x_scaled = value % self.automata.size[0]
            y_scaled = (value - x_scaled)/self.automata.size[0]
            x = x_scaled/self.automata.local_and_neighbours.scale
            y = y_scaled/self.automata.local_and_neighbours.scale
            local_vegf_values[i] = self.vegf.get_value_at(x, y)
            local_p53_values[i] = self.p53.get_value_at(x, y)
            local_automata_values[i] = 1
        self.automata.set_local_at_cell_positions(local_automata_values)
        self.vegf.set_local_at_cell_positions(local_vegf_values)
        self.p53.set_local_at_cell_positions(local_p53_values)
        # Reset cell cycle
        local_cellcycle_values[proliferation_indices] = local_cellcycle_values[proliferation_indices] % 1
        self.cell_cycle.set_local_at_cell_positions(local_cellcycle_values)
        # Update the location function.
        self.delta_function.update()

        timer.stop()

    def __is_empty_on_all_processes(self, array):
        arrays = self.comm.gather(array, root=0)
        tot_len_arrays = 0
        if self.comm.Get_rank() == 0:
            for array in arrays:
                tot_len_arrays += len(array)
        tot_len_arrays = self.comm.bcast(tot_len_arrays, root=0)
        return tot_len_arrays == 0


    def __add_new_cancer_cells_locally(self, oxygen, indices_not_yet_proliferated):
        """Add new cancer cells to self.automata without calling return_values().
        
        The cells are placed in the neighbour spots with highest oxygen 
        concentration that are origianlly free and not previously tried.
        
        self.automata is set to -(1 + index_of_parent) for new cancer cells.
        """
        cell_size = self.automata.cell_size
        size = self.automata.size
        for i in indices_not_yet_proliferated:
            x = self.x_local_cells[i]
            y = self.y_local_cells[i]
            highest_o2 = -1.0
            new_coord = None
            for dx in [-cell_size, 0 , cell_size]:
                for dy in [-cell_size, 0, cell_size]:
                    if ((-cell_size/2.0 < x + dx
                         < cell_size*(size[0] - 1) + cell_size/2.0)
                            and (-cell_size/2.0 < y + dy
                                 < cell_size*(size[1] - 1) + cell_size/2.0)
                            and ( dx != 0 or dy != 0)):
                        if (oxygen.concentration.get_value_at(x + dx, y + dy) > highest_o2
                             and self.automata.get_value_at(x + dx, y + dy) < 0.5
                             and not (x+dx, y+dy) in self.coord_prev_tried_proliferating[i]):
                            highest_o2 = oxygen.concentration.get_value_at(x + dx, y + dy)
                            new_coord = (x + dx, y + dy)
            if new_coord:
                # Construct unique and invertable identifier from coordinates.
                identifier = y*self.automata.cell_size*self.automata.size[0] + x

                # Set automata value so both parent coordinates and cell cycle
                # can be retrived.
                local_cellcycle_value = self.cell_cycle.get_local_at_cell_positions()
                value = (y*self.automata.local_and_neighbours.scale*self.automata.size[0]
                         + x*self.automata.local_and_neighbours.scale
                         + local_cellcycle_value[i] % 1.0)
                self.automata.set_value_at(new_coord[0], new_coord[1], - value - 1,
                                           self.priorities_proliferating[i], identifier)
                self.coord_prev_tried_proliferating[i].append(new_coord)

def generate_cell_placement(size, frac_cancer, frac_normal, seed):
    """Generate a random cell configuration used to initialise the cells.

    Args:
        size (int array): Define size of the vessel grid, nx, ny.
        frac_cancer (float): Fraction of potensial cell positions that are 
            filled with cancer cells.
        frac_normal (float): Fraction of potensial cell positions that are 
            filled with normal cells.
        seed (int): Seed for the random number generator.
    """
    n_potential_cells = size[0]*size[1]
    n_cancer_cells = int(round(n_potential_cells*frac_cancer))
    n_normal_cells = int(round(n_potential_cells*frac_normal))
    n_cells = n_normal_cells + n_cancer_cells

    np.random.seed(seed)
    rand_non_repeat = np.random.choice(n_potential_cells, n_cells, replace=False)
    cell_init = np.zeros(n_potential_cells)
    for i in rand_non_repeat[0:n_cancer_cells]:
        cell_init[i] = 1
    for i in rand_non_repeat[n_cancer_cells:]:
        cell_init[i] = 2

    return cell_init

def generate_cell_config(filename, size, frac_cancer, frac_normal, seed):
    """Generate a random cell configuration file used to initialise the cells.

    Args:
        filename (string): Name of file generated.
        size (int array): Define size of the vessel grid, nx, ny.
        frac_cancer (float): Fraction of potensial cell positions that are 
            filled with cancer cells.
        frac_normal (float): Fraction of potensial cell positions that are 
            filled with normal cells.
        seed (int): Seed for the random number generator.
    """
    cell_init = generate_cell_placement(size, frac_cancer, frac_normal, seed)
    np.savetxt(filename, cell_init, fmt='%d', delimiter='\n')

if __name__ == '__main__':
    # Tests the initialisation of the cell by plotting a cancer cell and a
    # normal cell both for automata and the cell_func. The automata should
    # have different hights, while cell_func should have the same. The automata
    # should also show the cells as six sided pyramids, while the cell_func
    # should show them as square pyramids for n_refinement > 0. The cell_func
    # cells show get a smaller base and become higher is n_refinement is
    # increased.
    # Also checks that the volume of the two cells is 2.0.
    #
    # update_cancer_cells() method is tested if test_update_cancer_cells is set.
    import os

    test_update_cancer_cells = True

    n = 5

    sim_database = sim_db.add_empty_sim(False)
    sim_database.write("cell_init_filename", "test_cell_init.txt", "string")
    sim_database.write("size", [n, n], "int array")
    sim_database.write("seed", 1234, "int")
    sim_database.write("n_refinements", 2, "int")
    sim_database.write("n_refinements_cells", 0, "int")
    sim_database.write("t_cell", 14.69, "float")
    sim_database.write("k_phi", 1.4, "float")
    sim_database.write("cell_size", 10.0, "float")
    chemo_sensitivity = [10000, 10000, 10000]
    sim_database.write("chemo_sensitivity", chemo_sensitivity, "float array")

    f = open("test_cell_init.txt", 'w')
    j = 0
    for i in range(n*n):
        if i == int(round(n*n/3)):
            j = 1
        elif i == int(round(2*n*n/3)):
            j = 2
        else:
            j = 0
        f.write("{}\n".format(j))
    f.close()

    base_mesh = BaseMesh(sim_database)
    cells = Cells(sim_database, base_mesh)
    volume = assemble(cells.delta_function.function*dx)
    sim_database.delete_from_database()
    sim_database.close()

    if abs(volume - 2.0) < 1e-8:
        print("The volume of the cells is correct.")
    else:
        print("The volume of the cells is {} and INCORRECT.".format(volume))


    f = File("figs/cell_automata.pvd")
    f << cells.automata.function

    f2 = File("figs/cell_delta_func.pvd")
    f2 << cells.delta_function.function

    try:
        os.remove("test_cell_init.txt")
    except OSError:
        print("'test_cell_init.txt' should have been removed by another process.")

    if test_update_cancer_cells:
        from model_parts.helpers.mesh import BaseMesh
        from model_parts.drugs_in_vessels import DrugsInVessels
        from model_parts.vessels import Vessels
        from model_parts.oxygen import Oxygen
        from model_parts.chemo import Chemotherapy

        n = 10

        # Just to make sure vessel_init_filename and cell_init_filename is in the
        # database.
        sim_database = sim_db.add_empty_sim(False)
        sim_database.write("vessel_init_filename", "empty_vessel_init_file.txt", "string")
        sim_database.write("cell_init_filename", "empty_cell_init_file.txt", "string")
        sim_database.delete_from_database()
        sim_database.close()

        sim_database = sim_db.add_empty_sim(False)
        # DrugsInVessels
        sim_database.write("tot_time", 12, "float")
        sim_database.write("vac_dt_at_injection", 0.01, "float")
        sim_database.write("chemo_dt_at_injection", [0.01, 0.01, 0.01], "float array")
        sim_database.write("vac_max_dt", 10.0, "float")
        sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
        sim_database.write("height", 160, "float")
        sim_database.write("weight", 58.2, "float")
        dose = [600, 100, 600, 15]
        sim_database.write("dose", dose, "float array")
        schedule = [3, 3, 3, 3]
        sim_database.write("schedule", schedule, "float array")
        # BaseMesh
        sim_database.write("size", [n, n], "int array")
        sim_database.write("n_refinements", 2, "int")
        sim_database.write("cell_size", 10.0, "float")
        # Cells
        sim_database.write("seed", 1235, "int")
        sim_database.write("frac_cancer", 0.2, "float")
        sim_database.write("frac_normal", 0.1, "float")
        sim_database.write("n_refinements_cells", 0, "int")
        sim_database.write("t_cell", 14.69, "float")
        sim_database.write("k_phi", 1.4, "float")
        # Vessels
        sim_database.write("n_refinements_vessels", 0, "int")
        sim_database.write("n_other_rngs", 1000, "int")
        sim_database.write("vessel_sep", 3, "int")
        sim_database.write("init_vessel_density", 0.1197, "float")
        sim_database.write("avg_init_vessel_radius", 10.0, "float")
        sim_database.write("first_vessel_random", False, "bool")
        sim_database.write("p_max_sprout", 0.002, "float")
        sim_database.write("v_sprout", 0.5, "float")
        sim_database.write("vegf_low", 1e-6, "float")
        sim_database.write("vegf_high", 1e-4, "float")
        sim_database.write("p_death", 0.001, "float")
        sim_database.write("rel_radius_new_vessels", 1.0, "float")
        # Oxygen
        sim_database.write("degree_oxygen", 1, "int")
        sim_database.write("n_refinements_oxygen", 0, "int")
        sim_database.write("oxygen_dt_after_cell_update",1.0, "float")
        sim_database.write("oxygen_dt_increase_factor_after_injection", 1.05, "float")
        sim_database.write("oxygen_dt_increase_factor_after_cell_update", 1.05, "float")
        sim_database.write("oxygen_max_dt", 10.0, "float")
        # Chemotherapy
        sim_database.write("ktrans", 0.14, "float")
        chemo_sensitivity = [10000, 10000, 10000]
        sim_database.write("degree_chemo", [1, 1, 1], "int array")
        sim_database.write("n_refinements_chemo", [0, 0, 0], "int array")
        sim_database.write("chemo_sensitivity", chemo_sensitivity, "float array")
        sim_database.write("chemo_dt_at_injection", [1.0, 1.0, 1.0], "float array")
        sim_database.write("chemo_dt_after_vessel_update", [1.0, 1.0, 1.0], "float array")
        sim_database.write("chemo_dt_increase_factor_after_injection", [1.05, 1.05, 1.05], "float array")
        sim_database.write("chemo_dt_increase_factor_after_vessel_update", [1.05, 1.05, 1.05], "float array")
        sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")

        base_mesh = BaseMesh(sim_database)
        drugs_in_vessels = DrugsInVessels(sim_database)
        vessels = Vessels(sim_database, base_mesh)
        cells = Cells(sim_database, base_mesh)
        oxygen = Oxygen(sim_database, base_mesh)
        G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')
        G2 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G2')
        G3 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G3')

        cells.update_cancer_cells(0, oxygen, G1, G2, G3)

        time = 10
        oxygen.update(time, True, vessels, cells)
        G1.update(time, True, vessels)
        G2.update(time, True, vessels)
        G3.update(time, True, vessels)

        vegf_init = np.zeros(cells.n_potential_local_cells)
        cells.vegf.set_local_at_cell_positions(vegf_init)
        p53_init = np.zeros(cells.n_potential_local_cells)
        cells.p53.set_local_at_cell_positions(p53_init)

        cells.update_cancer_cells(time, oxygen, G1, G2, G3)
        cells.update_vegf(time, oxygen)
        
        sim_database.delete_from_database()
        sim_database.close()
