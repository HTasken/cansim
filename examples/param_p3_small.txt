# Example parameters

name (string): example

description (string): "Run 12 wk patient 3 with original biopsy."

run_command (string): mpirun -n # python3 root/main.py

# Include default biological parameters
include_parameter_file: root/input_output/default_bio_parameters.txt

# Include default numerical parameters
include_parameter_file: root/input_output/default_num_parameters.txt

# Size of cellular automata.
size (int array): [31,23]

# Duration of simulation given in weeks.
tot_time (float): 12

# Intentionally left empty.
cell_init_filename (string): examples/cell_p3.txt

# The radius of the new vessels created relative to the average of the initial
# vessels. (rel_radius = r_new/r_avg_init)
rel_radius_new_vessels (float): 10.0

# Maximum probability of creating a vessel per vessel surface areal per time.
p_max_sprout (float): 0.02

# Probability of deleting a vessel per time.
p_death (float): 0.001

# Volume transfer coefficient
ktrans (float): 0.014

# Intentionally left empty.
cell_init_filename (string):

# Cell cycle length in days at a oxygen concentration of k_phi.
t_cell (float): 3.74

# Time-steps used
vac_dt_at_injection (float): 1.0
chemo_dt_at_injection (float array): [1.0, 1.0, 1.0]

vac_dt_increase_factor_after_injection (float): 1.00
chemo_dt_increase_factor_after_injection (float array): [1.00, 1.00, 1.00]

oxygen_dt_after_cell_update (float): 1.0
vac_dt_after_cell_update (float): 1.0
chemo_dt_after_vessel_update (float array): [1.0, 1.0, 1.0]

oxygen_dt_increase_factor_after_cell_update (float): 1.00
vac_dt_increase_factor_after_cell_update (float): 1.00
chemo_dt_increase_factor_after_vessel_update (float array): [1.00, 1.00, 1.00]

oxygen_max_dt (float): 10.0
vac_max_dt (float): 10.0
chemo_max_dt (float array): [10.0, 10.0, 10.0]

#chemo sensitivity
chemo_sensitivity (float array): [8000, 8000, 8000]

# In hours
dt_visualise (float): 2.0
dt_checkpoint (float): 0.5
