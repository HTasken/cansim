Collection of tests
===================

These test use pytest to test the code. pytest can be install with: 

    pip install -U pytest

More information about pytest can be found [here](https://docs.pytest.org/en/latest/getting-started.html).

Run the tests
-------------

All the test can be run with:

    py.test

To see the standard output of the tests use:

    py.test -s

To test the parallisation of the code run the test with:

    mpiexec -n N py.test

or 

    mpirun -n N py.test

where N is the number of processes used to run the test with. Test may fail is
N is so big that some processes have an empty mesh. N equal to 4 or less should
certainly be fine, N equal to 8 is usually also fine.

To run a spesific test add the name of the test file at the end of the commands. 
