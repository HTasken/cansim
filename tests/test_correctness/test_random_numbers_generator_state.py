import __init__
from  model_parts.helpers.random_numbers import random_number_generator_state
from dolfin import *
import pytest

@pytest.mark.parametrize("seed", [
                         (1234),
                         (1010),
                         (98765432)
])
def test_random_number_generator_state(seed):
    rank = MPI.comm_world

    if rank == 0:
        print("Check that different parameters give different states and equal" \
            + "parameters give the same.")
    rng1 = random_number_generator_state(1, 1, 3, 1, 0, seed)
    rng2 = random_number_generator_state(1, 1, 3, 1, 0, seed)
    rng3 = random_number_generator_state(0, 1, 3, 1, 0, seed)
    rng4 = random_number_generator_state(1, 1, 3, 1, 9, seed)
    i11 = rng1.rand_uniform_long()
    i12 = rng2.rand_uniform_long()
    i13 = rng3.rand_uniform_long()
    i14 = rng4.rand_uniform_long()
    i21 = rng1.rand_uniform_long()

    assert (i11 == i12 and i11 != i13 and i11 != i14 and i11 != i21)

    if rank == 0:
        print("Check that a distribution can be set.")
    rng1.set_uniform_double(10.1, 10.5)
    for i in range(10):
        rand = rng1.rand_uniform_double()
        assert not (rand < 10.1 or rand > 10.5)
    
    if rank == 0:
        print("Check that advancing does not crash.")
    rng1.advance(1000000)

    if rank == 0:
        print("Check that convertion of coordinates work.")

    rng5 = random_number_generator_state(7.5, 7.5, 3, 7.5, 0, seed)
    rng6 = random_number_generator_state(1.2, 3.6, 4, 1.2, 0, seed)
    rng7 = random_number_generator_state(1, 3, 4, 1, 0, seed)
    i15 = rng5.rand_uniform_long()
    i16 = rng6.rand_uniform_long()
    i17 = rng7.rand_uniform_long()

    assert (i15 == i11 and i16 == i17)
    

if __name__ == '__main__':
    test_random_number_generator_state(1234)
