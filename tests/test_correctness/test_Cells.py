import __init__
from model_parts.cells import Cells
from model_parts.helpers.mesh import BaseMesh
from model_parts.oxygen import Oxygen
from model_parts.chemo import Chemotherapy
from model_parts.drugs_in_vessels import DrugsInVessels
from model_parts.helpers.refined_function import RefinedFunction
import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
import os
import numpy as np
from dolfin import *
import pytest

@pytest.mark.filterwarnings('ignore')
def test_Cells_update_cancer_cells(print_progress=True):
    comm = MPI.comm_world
    rank = comm.Get_rank()
    comm.barrier()
    if print_progress and rank == 0:
        print("\nTest Cells.")

    cell_init_array = [[0, 0, 0, 2, 1],
                       [0, 0, 0, 2, 2],
                       [0, 1, 0, 1, 0],
                       [0, 0, 0, 0, 1],
                       [0, 0, 0, 1, 0]]

    cell_init_array = np.array(cell_init_array)
    if rank == 0:
        np.savetxt('cell_init.txt', cell_init_array.reshape(-1), fmt='%d', delimiter='\n')
    comm.barrier()

    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
                argv="add -f "
                "root/tests/test_correctness/parameter_file.txt".split(),
                print_ids_added=False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    if rank == 0:
        sim_database.write("size", [5, 5], "int array")
        sim_database.write("n_refinements", 2, "int")
        sim_database.write("cell_init_filename", "cell_init.txt", "string")
        sim_database.write("dt_cell_update", 30, "float")
        sim_database.write("tot_time", 0.5, "float")
        sim_database.write("schedule", [0.5, 0.5, 0.5, 0.5], "float array")
    comm.barrier()

    base_mesh = BaseMesh(sim_database)

    drugs_in_vessels = DrugsInVessels(sim_database)
    G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')
    G2 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G2')
    G3 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G3')
    oxygen = Oxygen(sim_database, base_mesh)
    oxygen.concentration.set(np.arange(cell_init_array.size))

    cells = Cells(sim_database, base_mesh)

    if rank == 0:
        os.remove('cell_init.txt')
    comm.barrier()

    if print_progress and rank == 0:
        print("Test initialization of cells.")

    refined_func = RefinedFunction(base_mesh, 0, create_test_function=False,
                                   allow_neighbour_access=False)
    refined_func.set(cell_init_array)

    comm.barrier()

    assert np.array_equal(refined_func.get_local_at_cell_positions(),
                          cells.automata.get_local_at_cell_positions())

    if print_progress and rank == 0:
        print("Test placement of new cancer cells.")
    x_local = cells.automata.local_x_at_cell_positions
    y_local = cells.automata.local_y_at_cell_positions
    local_cellcycle_value = np.zeros(np.shape(x_local))

    for i, (x, y) in enumerate(zip(x_local, y_local)):
        cell_size = cells.automata.cell_size
        if (abs(x - 1*cell_size) + abs(y - 2*cell_size) < 0.001):
            local_cellcycle_value[i] = 1.0
        elif (abs(x - 3*cell_size) + abs(y - 2*cell_size) < 0.001):
            local_cellcycle_value[i] = 1.1
        elif (abs(x - 4*cell_size) + abs(y - 3*cell_size) < 0.001):
            local_cellcycle_value[i] = 1.2
        else:
            local_cellcycle_value[i] = 1.3
    cells.cell_cycle.set_local_at_cell_positions(local_cellcycle_value)
    dt_cell_update = sim_database.read('dt_cell_update')
    cells.update_cancer_cells(0, oxygen, G1, G2, G3)

    correct_cell_array = [[0, 0, 0, 2, 1],
                          [0, 0, 0, 2, 2],
                          [0, 1, 0, 1, 0],
                          [0, 1, 1, 1, 1],
                          [0, 0, 0, 1, 1]]

    refined_func.set(correct_cell_array)

    assert np.array_equal(refined_func.get_local_at_cell_positions(),
                          cells.automata.get_local_at_cell_positions())

    if print_progress and rank == 0:
        print("Test killing cancer cells with chemotherapies.")
    G1_values_at_cells = np.zeros((5,5))
    G1_values_at_cells[3,3] = G1.peak_vessel_concentration
    G1_values_at_cells[3,4] = G1.peak_vessel_concentration
    G1_values_at_cells[4,3] = G1.peak_vessel_concentration
    G1_values_at_cells[4,4] = G1.peak_vessel_concentration
    G1.concentration.set(G1_values_at_cells)

    x_local = cells.automata.local_x_at_cell_positions
    y_local = cells.automata.local_y_at_cell_positions
    local_cellcycle_value = cells.cell_cycle.get_local_at_cell_positions()
    for i, (x, y) in enumerate(zip(x_local, y_local)):
        cell_size = cells.automata.cell_size
        if (abs(x - 3*cell_size) + abs(y - 3*cell_size) < 0.001):
            local_cellcycle_value[i] = 1.0
        elif (abs(x - 4*cell_size) + abs(y - 4*cell_size) < 0.001):
            local_cellcycle_value[i] = 1.0
        else:
            local_cellcycle_value[i] = 0.0
    cells.cell_cycle.set_local_at_cell_positions(local_cellcycle_value)
    cells.update_cancer_cells(0, oxygen, G1, G2, G3)

    correct_cell_array = [[0, 0, 0, 2, 1],
                          [0, 0, 0, 2, 2],
                          [0, 1, 0, 1, 0],
                          [0, 1, 1, 0, 1],
                          [0, 0, 0, 1, 0]]

    refined_func.set(correct_cell_array)

    assert np.array_equal(refined_func.get_local_at_cell_positions(),
                          cells.automata.get_local_at_cell_positions())

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

@pytest.mark.parametrize("size, o2_scale, seed", [
                         ([4, 4], 0.005, 1234),
                         ([4, 4], 0.02, 4321),
                         ([6, 6], 0.5, 3333),
                         ([10, 10], 0.01, 1111)
])
def test_Cells_update_vegf(size, o2_scale, seed, print_progress=True):
    comm = MPI.comm_world
    rank = comm.Get_rank()
    if print_progress and rank == 0:
        print("\nTest Cells.")
        print("Test that the update_vegf() give the steady state solution after enought time.")

    np.random.seed(seed)
    cell_init_array = np.random.randint(3, size=tuple(size))

    cell_init_array = np.array(cell_init_array)
    if rank == 0:
        np.savetxt('cell_init.txt', cell_init_array.reshape(-1), fmt='%d', delimiter='\n')
    comm.barrier()

    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
                argv="add -f "
                "root/tests/test_correctness/parameter_file.txt".split(),
                print_ids_added=False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    if rank == 0:
        sim_database.write("size", size, "int array")
        sim_database.write("n_refinements", 2, "int")
        sim_database.write("cell_init_filename", "cell_init.txt", "string")
    comm.barrier()

    base_mesh = BaseMesh(sim_database)

    cells = Cells(sim_database, base_mesh)

    if rank == 0:
        os.remove('cell_init.txt')
    comm.barrier()

    oxygen = Oxygen(sim_database, base_mesh)
    oxygen.concentration.set(o2_scale*(np.arange(cell_init_array.size) + 1.0))

    vegf_init = np.zeros(cells.n_potential_local_cells)
    cells.vegf.set_local_at_cell_positions(vegf_init)
    p53_init = np.zeros(cells.n_potential_local_cells)
    cells.p53.set_local_at_cell_positions(p53_init)

    end_time = 20000000 # Choosen to be long enought to be close to steady state.

    values_at_potential_cells = np.ones(size)*0.25
    cells.vegf.set(values_at_potential_cells)
    cells.p53.set(values_at_potential_cells)

    cells.update_vegf(end_time, oxygen)

    # Local cell indices
    lci = np.where(cells.automata.get_local_at_cell_positions() > 0.5)[0]

    local_vegf = cells.vegf.get_local_at_cell_positions()
    local_p53 = cells.p53.get_local_at_cell_positions()

    cells.solve_vegf_steady_state(oxygen)

    steady_state_local_vegf = cells.vegf.get_local_at_cell_positions()
    steady_state_local_p53 = cells.p53.get_local_at_cell_positions()

    if len(steady_state_local_vegf) > 0:
        assert np.max(np.abs(local_vegf[lci] - steady_state_local_vegf[lci])) < 0.1
        assert np.max(np.abs(local_p53[lci] - steady_state_local_p53[lci])) < 0.1

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

if __name__ == '__main__':
    test_Cells_update_cancer_cells()
    #test_Cells_update_vegf([100,100], 0.01, 1234)
