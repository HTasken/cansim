import __init__
from model_parts.helpers.refined_function import RefinedFunction 
from model_parts.helpers.mesh import BaseMesh
import model_parts.helpers.add_sim_db_to_path
import sim_db
import numpy as np
import time
import cProfile
import pytest
from dolfin import *


@pytest.mark.parametrize("size, n_refinements, n_time_steps", [
                         ([4, 4], 0, 10),
                         ([3, 4], 1, 20),
                         ([4, 4], 2, 10),
])
def test_RefinedFunction_on_heat_equation(size, n_refinements, n_time_steps,
                       print_progress=True, profile_dir=None, barrier_on=True):
    """Test solving the heat equation with RefinedFunction.

    Test the regular, trial and test function of a RefinedFunction as well as
    collect_value(), get_value_at(), set_value_at() and return_values().

    A modified example from the FEniCS homepage is used.
    """
    dt = 10e-5          # time step size
    T = dt*n_time_steps # final time
    alpha = 10          # parameter alpha
    beta = 1.2          # parameter beta

    def barrier():
        if barrier_on:
            comm.barrier()

    if profile_dir:
        profile = cProfile.Profile()
        profile.enable()

    comm = MPI.comm_world
    rank = comm.Get_rank()

    if rank == 0:
        print("\nTest solving the heat equation with RefinedFunction.")

    db_id = None
    if rank == 0:
        sim_database = sim_db.add_empty_sim(False)
        sim_database.write("size", size, "int array")
        sim_database.write("n_refinements", 2, "int")
        sim_database.write("cell_size", 10.0, "float")
        db_id = sim_database.get_id()
        sim_database.close()
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)
    base_mesh = BaseMesh(sim_database)

    # Scale the coordinates and force term, f, so conteract the cell size while 
    # avoiding having to refine to get accurate result.
    scale = Constant((1.0/base_mesh.cell_size)**2)

    # Define correct answer that is also used as initial value and boundary condition.
    u_D = Expression('1 + scale*x[0]*x[0] + scale*alpha*x[1]*x[1] + beta*t',
                     degree=2, alpha=alpha, beta=beta, t=0, scale=scale)

    # Define initial value
    u = RefinedFunction(base_mesh, n_refinements, degree=1, create_trial_function=True)
    u.function = interpolate(u_D, u.function_space)

    # Define boundary
    def boundary(x, on_boundary):
        return on_boundary

    bc = DirichletBC(u.function_space, u_D, boundary)

    # Define variational problem
    f = Constant(beta - (2 + 2*alpha)*scale)

    F = u.trial_function*u.test_function*dx \
        + dt*dot(grad(u.trial_function), grad(u.test_function))*dx \
        - (u.function + dt*f)*u.test_function*dx

    a, L = lhs(F), rhs(F)

    # Time-stepping
    u_n = Function(u.function_space)
    t = 0
    local_x_coord = u.local_x_at_cell_positions
    local_y_coord = u.local_y_at_cell_positions
    original_values = np.zeros(u.n_potential_local_cells)
    for n in range(n_time_steps):

        # Update current time
        t += dt
        u_D.t = t

        # Compute solution
        solve(a == L, u_n, bc)

        # Compute error at vertices
        u_e = interpolate(u_D, u.function_space)
        error = np.abs(u_e.vector().get_local() - u.function.vector().get_local()).max()
        
        if print_progress:
            barrier()
            print("Max error at rank {0} at time {1:.4f} is {2}.".format(rank, t, error))
        
        assert error < 0.001

        # Update previous solution
        u.function.assign(u_n)

        # Get original values at positions equivalent to cell positions, 
        # set values them to -1 and back to original values.
        u.collect_values()
        for i, (x, y) in enumerate(zip(local_x_coord, local_y_coord)):
            original_values[i] = u.get_value_at(x, y)
            for dy_n in [-u.cell_size, 0, u.cell_size]:
                for dx_n in [-u.cell_size, 0, u.cell_size]:
                    if (-u.cell_size/2.0 < x + dx_n < u.cell_size*(size[0] + 0.5) 
                      and (-u.cell_size/2.0 < y + dy_n < u.cell_size*(size[1] + 0.5))):
                        u.set_value_at(x, y, -1, rank + 1)
        u.return_values()
        new_local = u.get_local_at_cell_positions()
        tot_error = np.sum(np.abs(new_local + np.ones(u.n_potential_local_cells)))
        
        if print_progress:
            barrier()
            print("Set values at 'cell positions' to -1 with total error {}.".format(tot_error))

        assert tot_error < 0.0001

        u.collect_values()
        for i, (x, y) in enumerate(zip(local_x_coord, local_y_coord)):
            u.set_value_at(x, y, original_values[i], 1 + rank)
        u.return_values()

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

    if profile_dir:
        profile.disable()
        profile.dump_stats("{0}/test_RefinedFunction_on_heat_equation_".format(profile_dir) \
          + "nx{0}_ny{1}_r{2}_steps{3}_".format(size[0], size[1], n_refinements, n_time_steps) \
          + "p{0}_rank{1}.prof".format(comm.Get_size(), rank))

if __name__ == '__main__':
    import sys
    test_RefinedFunction_on_heat_equation([int(sys.argv[1]), int(sys.argv[2])], 
                                          int(sys.argv[3]), int(sys.argv[4]), profile_dir="profiles")
