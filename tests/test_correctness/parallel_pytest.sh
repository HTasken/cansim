#!/bin/bash
# Runs each of the tests in parallel individually.
# That is 'mpirun -n 'num_processes' python -m pytest 'file_name', 
# is called for each of the files.
# Takes longer than running all at once.
#
# Usage: ./parallel_pytest.sh [-n 'num_processes'] [-s]

n=4
s=""
while [[ $# -gt 0 ]]; do
    key=$1
    case $key in
	-n)
	n=$2
	shift
	shift
	;;
	-s)
	s="-s"
	shift
	;;
    esac
done
find . -name "test_*.py" -exec mpirun -n $n python -m pytest --disable-warnings $s {} \;
