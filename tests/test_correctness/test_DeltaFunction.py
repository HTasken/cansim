import __init__
from model_parts.helpers.mesh import BaseMesh
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.delta_function import DeltaFunction
import model_parts.helpers.add_sim_db_to_path
import sim_db
import numpy as np
import cProfile
from dolfin import *
import pytest

@pytest.mark.parametrize("size, n_base_mesh_refinements, n_additional_refinements", [
                         ([4, 4], 2, 0), 
                         ([4, 4], 2, 1),
                         #([4, 4], 3, 2),
                         ([4, 5], 2, 2),
                         ([8, 7], 2, 0),
                         ([8, 7], 2, 1),
                         ([5, 5], 2, 1)
])
def test_DeltaFunction(size, n_base_mesh_refinements, n_additional_refinements, 
                         print_progress=True, profile_dir=None, barrier_on=True):
    """Test all of DeltaFunction's methods."""

    def barrier():
        if barrier_on:
            comm.barrier()

    if profile_dir:
        profile = cProfile.Profile()
        profile.enable()

    comm = MPI.comm_world
    rank = comm.Get_rank()

    db_id = None
    if rank == 0:
        sim_database = sim_db.add_empty_sim(False)
        sim_database.write("size", size, "int array")
        sim_database.write("n_refinements", n_base_mesh_refinements, "int")
        sim_database.write("cell_size", 10.0, "float")
        db_id = sim_database.get_id()
        sim_database.close()
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    base_mesh = BaseMesh(sim_database)

    V = FunctionSpace(base_mesh.mesh, 'CG', 1)
    n_intermediate = 2**(n_base_mesh_refinements + n_additional_refinements) - 1
    if print_progress and rank == 0:
        print("\nTest all of DeltaFunction's methods.")
    barrier()

    refined_func = RefinedFunction(base_mesh, 0)

    init_array = None
    if rank == 0:
        if print_progress:
            print("\nTesting the volume of each delta function is 1.")
    np.random.seed(0)
    init_array = np.random.randint(3, size=tuple(size))
    correct_volume = np.sum(init_array > 0)
    refined_func.set(init_array)

    delta_func = DeltaFunction(refined_func, base_mesh, n_additional_refinements)

    volume = assemble(delta_func.location.function*dx)
    if print_progress and rank == 0:
        print("Volume is {0} and should be {1}.".format(volume, correct_volume))
    assert abs(volume - correct_volume) < 0.0001

    barrier()
    if print_progress and rank == 0:
        print("\nTesting update() method five times.")
    barrier()

    for i in range(5):
        if rank == 0:
            init_array = np.random.randint(3, size=tuple(size))
        init_array = comm.bcast(init_array, root = 0)
        correct_volume = np.sum(init_array > 0)
        refined_func.set(init_array)

        delta_func.update()

        volume = assemble(delta_func.location.function*dx)
        if print_progress and rank == 0:
            print("Volume is {0} after update and should be {1}.".format(volume, correct_volume))
        assert abs(volume - correct_volume) < 0.0001

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

    if profile_dir:
        profile.disable()
        profile.dump_stats("{0}test_DeltaFunction_nx{1}".format(profile_dir, size[0]) \
                         + "_ny{0}_p{1}_rank{2}.prof".format(size[1], comm.Get_size(), rank))
 
if __name__ == '__main__':
    test_DeltaFunction([5,5], 2, 1)
