import __init__
from model_parts.drugs_in_vessels import DrugsInVessels
import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
from dolfin import *

def test_DrugsInVessel():
    comm = MPI.comm_world
    rank = comm.Get_rank()
    
    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
                argv="add -f "
                "root/tests/test_correctness/parameter_file.txt".split(),
                print_ids_added=False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    dt_drug = 0.5
    if rank == 0:
        sim_database.write("tot_time", dt_drug, "float")
        sim_database.write("schedule", [dt_drug, dt_drug, dt_drug, dt_drug], "float array")
    comm.barrier()

    drugs_in_vessels = DrugsInVessels(sim_database)

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

if __name__ == '__main__':
    test_DrugsInVessel()
