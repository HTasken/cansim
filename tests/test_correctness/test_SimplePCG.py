import __init__
from model_parts.helpers.pcg.simple_pcg import SimplePCG
import pytest

@pytest.mark.parametrize("n, seed", [
                         (10, 0),
                         (1000, 1234),
                         (100000, 4321)
])
def test_SimplePCG(n, seed, print_progress=True):

    pcg = SimplePCG(seed)

    pcg.advance(10000000)

    assert type(pcg.rand_uniform_long()) == int

    pcg.set_uniform_long(5, 10)
    for i in range(n):
        rand = pcg.rand_uniform_long()
        assert (rand > 4 and rand < 11)
    
    assert type(pcg.rand_uniform_double()) == float

    pcg.set_uniform_double(5.5, 6.0)
    for i in range(n):
        rand = pcg.rand_uniform_double()
        assert (rand >= 5.5 and rand < 6.0)

    assert type(pcg.rand_binomial_int()) == int

    pcg.set_binomial_int(1, 0.5)
    for i in range(n):
        rand = pcg.rand_binomial_int()
        assert (rand == 1 or rand == 0)

if __name__ == '__main__':
    test_SimplePCG(1000000, 1234)
