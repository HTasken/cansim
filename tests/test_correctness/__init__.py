import sys
import os

sys.path.append(os.getcwd())
sys.path.append(os.pardir)
sys.path.append(os.path.abspath(os.path.join(os.pardir, "..")))
