import __init__
from model_parts.helpers.mesh import BaseMesh
from model_parts.oxygen import Oxygen
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.drugs_in_vessels import DrugsInVessels
from model_parts.chemo import Chemotherapy
import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
import os
import numpy as np
from dolfin import *
import pytest
import matplotlib
import matplotlib.pyplot as plt

set_log_level(LogLevel.INFO)

@pytest.mark.parametrize("size, o2_scale, seed", [
    ([10,10], 1.0, 1234),
])
def test_Oxygen(size, o2_scale, seed, print_progress=True):
    comm = MPI.comm_world
    rank = comm.Get_rank()
    if print_progress and rank == 0:
        print("\nTest Oxygen.")
        print("Test that update() == solve_steady_state() after enought time.")
        if comm.Get_size() > 2:
            print("Might take a long time with this many processes!")
    comm.barrier()

    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
                argv="add -f "
                "root/tests/test_correctness/parameter_file.txt".split(),
                print_ids_added=False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    if rank == 0:
        sim_database.write("size", size, "int array")
        sim_database.write("seed", seed, "int")
        sim_database.write("oxygen_max_dt", 10.0, "float")
        sim_database.write("oxygen_dt_after_cell_update", 1.0, "float")
        sim_database.write("oxygen_dt_after_vessel_update", 10.0, "float")
        sim_database.write("oxygen_dt_increase_factor_after_cell_update", 1.00, "float")
        sim_database.write("degree_oxygen", 1, "int")
        sim_database.write("n_refinements_oxygen", 0, "int")
        sim_database.write("tot_time", 0.00001, "float") # To make calc. drug fast
    comm.barrier()

    base_mesh = BaseMesh(sim_database)
    drugs_in_vessels = DrugsInVessels(sim_database)
    cells = Cells(sim_database, base_mesh)
    vessels = Vessels(sim_database, base_mesh)
    oxygen = Oxygen(sim_database, base_mesh)    
    if rank == 0:
        sim_database.write("oxygen_dt_after_cell_update", 1.0, "float")
    comm.barrier()
    oxygen1 = Oxygen(sim_database, base_mesh)
    oxygen2 = Oxygen(sim_database, base_mesh)
    e = Expression("constant", constant=o2_scale, degree=1)

    oxygen.concentration.function.interpolate(e)
    oxygen.concentration.prev_function.interpolate(e)

    oxygen1.concentration.function.interpolate(e)
    oxygen1.concentration.prev_function.interpolate(e)

    oxygen2.concentration.function.interpolate(e)
    oxygen2.concentration.prev_function.interpolate(e)

    #err_update = {'dt':oxygen1.dt_after_cell_update, 'max':[],'min':[],'avg':[]}
    
#    end_time = 1000 # Choosen to be long enough to be close to steady state.
    update_interval = 30
    end_time = 30
    # linearized solver solution
    oxygen1.update_linearized(update_interval, True, vessels, cells)    
    updated_oxygen_linearized = np.copy(oxygen1.concentration.function.vector().get_local())  
    f = File("figs/oxygen_linear.pvd")
    f << oxygen1.concentration.function      
    # non-linear solver solution
    oxygen.update(update_interval, True, vessels, cells)
    updated_oxygen = np.copy(oxygen.concentration.function.vector().get_local())
    f = File("figs/oxygen_nonlinear.pvd")
    f << oxygen.concentration.function
      
    # 1 iter newton solver solution
    oxygen2.update_newton(update_interval, True, vessels, cells)              
    updated_oxygen_newton = np.copy(oxygen1.concentration.function.vector().get_local())
    f = File("figs/oxygen_newton.pvd")
    f << oxygen2.concentration.function    
    temp = errornorm(oxygen1.concentration.function, oxygen.concentration.function)
    temp1 = errornorm(oxygen2.concentration.function, oxygen.concentration.function)
    temp2 = errornorm(oxygen1.concentration.function, oxygen2.concentration.function)
    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

#    if np.all(np.abs(steady_state_oxygen) > 1e-10):
#        assert (np.max(np.abs((updated_oxygen - steady_state_oxygen)
#                             /steady_state_oxygen)) < 0.001)
#        assert (np.max(np.abs((updated_oxygen - updated_oxygen_linearized)
#                             /updated_oxygen)) < 0.001)                             
#    else:
#        assert (np.max(np.abs(updated_oxygen - steady_state_oxygen)) < 0.001)
#        assert (np.max(np.abs(updated_oxygen - updated_oxygen_linearized))<0.001)
    return temp, temp1, temp2, temp/norm(oxygen.concentration.function), temp1/norm(oxygen.concentration.function), temp2/norm(oxygen2.concentration.function)
if __name__ == '__main__':
    import csv
    err_update = test_Oxygen([100, 100], 10, 1111)
    # with open('err_update.csv','w') as f
    #     err_writer = csv.writer(f,delimiter=',')
    #     for key, val in err_update.items():
    #         err_writer.writerow([key,val])
    print(err_update)
    list_timings(TimingClear.keep, [TimingType.wall])

    #dump_timings_to_xml("timings.xml", TimingClear.clear)