# Parameters used for testing.

seed (int): 1234

vac_dt_at_injection (float): 0.1
chemo_dt_at_injection (float array): [0.1, 0.1, 0.1]

vac_dt_increase_factor_after_injection (float): 1.05
chemo_dt_increase_factor_after_injection (float array): [1.05, 1.05, 1.05]

oxygen_dt_after_cell_update (float): 1.0
vac_dt_after_cell_update (float): 1.0
chemo_dt_after_vessel_update (float array): [1.0, 1.0, 1.0]

oxygen_dt_increase_factor_after_cell_update (float): 1.05
vac_dt_increase_factor_after_cell_update (float): 1.05
chemo_dt_increase_factor_after_vessel_update (float array): [1.05, 1.05, 1.05]

oxygen_max_dt (float): 10.0
vac_max_dt (float): 10.0
chemo_max_dt (float array): [10.0, 10.0, 10.0]

n_refinements (int): 2

n_refinements_cells (int): 0
n_refinements_vessels (int): 0
n_refinements_oxygen (int): 0
n_refinements_vac (int): 0
n_refinements_chemo (int array): [0, 0, 0]

degree_oxygen (int): 1
degree_vac (int): 1
degree_chemo (int array): [1, 1, 1]

# In weeks
tot_time (float): 12.0

# In minutes
dt_cell_update (float): 30

# In hours
dt_vessel_update (float): 12

size (int array): [10, 10]

# Intentionally left empty
cell_init_filename (string):
frac_cancer (float): 0.2
frac_normal (float): 0.1

# Intentionally left empty
vessel_init_filename (string):
is_vessel_radius_relative (bool): True
avg_init_vessel_radius (float): 10.0
init_vessel_density (float): 0.1197
vessel_sep (int): 6
first_vessel_random (bool): False

v_sprout (float): 27e3
k_phi (float): 1.4
t_cell (float): 14.69
ktrans (float): 0.14
p_max_sprout (float): 0.002
p_death (float): 0.0001
vegf_low (float): 1e-6
vegf_high (float): 1e-4
rel_radius_new_vessels (float): 1.0
schedule (float array): [3, 3, 3, 3]
dose (float array): [600, 100, 600, 15]
chemo_sensitivity (float array): [10000, 10000, 10000]
height (float): 160
weight (float): 58.2
cell_size (float): 10.0
