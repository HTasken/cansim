import __init__
from model_parts.helpers.mesh import BaseMesh
from model_parts.oxygen import Oxygen
from model_parts.vegf_avastin import VegfAvastin
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.drugs_in_vessels import DrugsInVessels
from model_parts.chemo import Chemotherapy
import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
import os
import numpy as np
from dolfin import *
import pytest

set_log_level(LogLevel.INFO)

@pytest.mark.parametrize("size, vac_scale, seed", [([4, 4],
                                                    {'V':1e-6,'A':1e-5,'C':1e-7}, 1234)])
def test_VegfAvastin(size, vac_scale, seed, print_progress=True):
    comm = MPI.comm_world
    rank = comm.Get_rank()
    if print_progress and rank == 0:
        print("\nTest VegfAvastin.")
        print("Test that update() == solve_steady_state() after enought time.")
    comm.barrier()

    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
            argv="add -f " "root/tests/test_correctness/parameter_file.txt".split(),
            print_ids_added=False,
        )
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    if rank == 0:
        sim_database.write("size", size, "int array")
        sim_database.write("seed", seed, "int")
        sim_database.write("vac_max_dt", 10.0, "float")
        sim_database.write("vac_dt_after_cell_update", 1, "float")
        sim_database.write("vac_dt_increase_factor_after_cell_update", 1.0, "float")
        sim_database.write("degree_vac", 1, "int")
        sim_database.write("n_refinements", 2, "int")
        sim_database.write("n_refinements_vac", 0, "int")
    comm.barrier()

    base_mesh = BaseMesh(sim_database)
    drugs_in_vessels = DrugsInVesselsDummy(10.0)
    cells = Cells(sim_database, base_mesh)
    vessels = Vessels(sim_database, base_mesh)
    oxygen = Oxygen(sim_database, base_mesh)
    vegf_avastin = VegfAvastin(sim_database, base_mesh, drugs_in_vessels)

    cell_init_array = np.random.randint(3, size=tuple(size))
    print(cell_init_array)
    cells.automata.set(cell_init_array)
    cells.delta_function.update()
    oxygen.concentration.function.vector().set_local(oxygen.concentration.function.vector()[:]+ 3.8)
    f = File("figs/vegf_oxygen.pvd")
    f << oxygen.concentration.function    
    cells.solve_vegf_steady_state(oxygen)
    print(sum(cells.automata.get_local_at_cell_positions()))

    cell_location = interpolate(cells.delta_function.function, cells.vegf.function_space)
    f = File("figs/vegf_cell.pvd")
    f << cells.vegf.function 
    f = File("figs/vegf_cell_loc.pvd")
    f << cell_location

    e = Expression(("c1", "c2", "c3"), c1=vac_scale['V'], c2=vac_scale['A'], c3=vac_scale['C'], degree=1)    
#    e = Expression(("constant", "constant", "constant"), constant=vac_scale, degree=1)

    vegf_avastin.VAC.function.interpolate(e)
    vegf_avastin.VAC.prev_function.interpolate(e)

    end_time = 20  # Choosen to be long enough to be close to steady state.
    time_step = 10

    # save for debugging purposes
#    vtk_file = File("vac_update.pvd")
#    vtk_file << vegf_avastin.VAC.function

    vegf_avastin.solve_steady_state(vessels, cells, oxygen)
    steady_state_vegf_avastin = vegf_avastin.VAC.function.copy()
    vegf_avastin.VAC.function.interpolate(e)
    vegf_avastin.VAC.prev_function.interpolate(e)

    i = 0
    for dt in range(time_step, end_time, time_step):
        if np.any(vegf_avastin.VAC.function.vector().get_local() < 0):
            print(vegf_avastin.VAC.function.vector().get_local())
            assert False
        i = i + 1
        print("update no.:", i, " at time", dt)
        vegf_avastin.update(end_time, True, vessels, cells, oxygen, False)
        f = File("figs/vegf_update.pvd")
        f << vegf_avastin.VAC.function
                
#        vtk_file << vegf_avastin.VAC.function
        if np.any(vegf_avastin.VAC.function.vector().get_local() < 0):
            print(vegf_avastin.VAC.function.vector().get_local())
            assert False
    updated_vegf = vegf_avastin.VAC.function.copy()
    vegf_avastin.VAC.function.interpolate(e)
    vegf_avastin.VAC.prev_function.interpolate(e)

    i = 0
    vegf_avastin.time = 0        
    for dt in range(time_step, end_time, time_step):
        if np.any(vegf_avastin.VAC.function.vector().get_local() < 0):
            print(vegf_avastin.VAC.function.vector().get_local())
            assert False
        i = i + 1
        print("update no.:", i, " at time", dt)
        vegf_avastin.update(end_time, True, vessels, cells, oxygen, True)
#        vtk_file << vegf_avastin.VAC.function
#        if np.any(vegf_avastin.VAC.function.vector().get_local() < 0):
#            print(vegf_avastin.VAC.function.vector().get_local())
#            assert False
        f = File("figs/vac_update.pvd")
        f << vegf_avastin.VAC.function
    updated_vegf_avastin = vegf_avastin.VAC.function.copy()




    # save for debugging purposes
    # vtk_file = File("vac_steady_state.pvd")
    # vtk_file << vegf_avastin.VAC.function
    # vtk_file = File("cells.pvd")
    # vtk_file << cells.automata.function
    # vtk_file = File("cells_vegf.pvd")
    # vtk_file << cells.vegf.function
    # vtk_file = File("vessels.pvd")
    # vtk_file << vessels.relative_radius.function
    # vtk_file = File("oxygen.pvd")
    # vtk_file << oxygen.concentration.function

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

    # debugging purposes
    abs_err = errornorm(updated_vegf_avastin, steady_state_vegf_avastin)
    abs_err1 = errornorm(updated_vegf_avastin, updated_vegf)
#   rel_err = abs_err/norm(steady_state_vegf_avastin)
#    rel_err1 = abs_err1/norm(updated_vegf_avastin)

    f = File("figs/vegf_steady_state.pvd")
    f << steady_state_vegf_avastin
#    print(rel_err)
    print(abs_err)
#    print(rel_err1)
    print(abs_err1)    
    # expected check
#    assert np.all(updated_vegf_avastin.vector().get_local() > 0)
#    assert np.all(updated_vegf.vector().get_local() > 0)
#    assert np.all(steady_state_vegf_avastin.vector().get_local() > 0)

    if np.all(np.abs(steady_state_vegf_avastin.vector().get_local()) > 1e-10):
        assert (
            rel_err < 0.01
        )
    else:
        assert (
            abs_err < 0.01
        )

class DrugsInVesselsDummy:
    def __init__(self, value):
        self.value = value

    def get(self, name, time):
        return self.value

if __name__ == "__main__":
    test_VegfAvastin([100, 100], {'V':0.0, 'A':0.0,'C':0.0}, 1234)
    list_timings(TimingClear.keep, [TimingType.wall])
