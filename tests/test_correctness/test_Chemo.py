import __init__
from model_parts.helpers.mesh import BaseMesh
from model_parts.chemo import Chemotherapy
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.drugs_in_vessels import DrugsInVessels
import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
import os
import numpy as np
from dolfin import *
import pytest

set_log_level(LogLevel.WARNING)

@pytest.mark.parametrize("size, chemo_scale, seed", [
    ([10,10], 1e-5, 1234)
])
def test_Chemotherapies(size, chemo_scale, seed, print_progress=True):
    comm = MPI.comm_world
    rank = comm.Get_rank()
    if print_progress and rank == 0:
        print("\nTest Chemotherapies.")
        print("Test that update() == solve_steady_state() after enough time.")
    comm.barrier()

    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
            argv="add -f "
            "root/tests/test_correctness/parameter_file.txt".split(),
            print_ids_added=False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    if rank == 0:
        sim_database.write("size", size, "int array")
        sim_database.write("seed", seed, "int")
        sim_database.write("chemo_dt_at_injection", [1,1,1], "float array")
        sim_database.write("chemo_dt_increase_factor_after_injection", [1.00, 1.00, 1.00], "float array")
        sim_database.write("chemo_dt_increase_factor_after_vessel_update", [1.00, 1.00, 1.00], "float array")
        sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
        chemo_sensitivity = [10000, 10000, 10000]
        sim_database.write("degree_chemo", [1, 1, 1], "int array")
        sim_database.write("n_refinements_chemo", [0, 0, 0], "int array")
        sim_database.write("chemo_sensitivity", chemo_sensitivity, "float array")
    comm.barrier()

    base_mesh = BaseMesh(sim_database)
    drugs_in_vessels = DrugsInVesselsDummy('G1',10.0) 
    cells = Cells(sim_database, base_mesh)
    vessels = Vessels(sim_database, base_mesh)
    G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')

    e = Expression("constant", constant=chemo_scale, degree=1)

    G1.concentration.function.interpolate(e)
    G1.concentration.prev_function.interpolate(e)

    end_time = 1000 # Choosen to be long enought to be close to steady state.
    time_step = 1

    # save for debugging purposes
#    vtk_file = File("G1_update.pvd")
#    vtk_file << G1.concentration.function

    i = 0
    
    for dt in range(time_step, end_time, time_step):
        if np.any(G1.concentration.function.vector().get_local() < 0):
            print(G1.concentration.function.vector().get_local())
            assert False
        i = i + 1
        print("update no.:", i, " at time", dt)
        G1.update(end_time, True, vessels, cells)
#        vtk_file << G1.concentration.function
        if np.any(G1.concentration.function.vector().get_local() < 0):
            print(G1.concentration.function.vector().get_local())
            assert False
    updated_G1 = np.copy(G1.concentration.function.vector().get_local())

    G1.concentration.function.interpolate(e)
    
    G1.solve_steady_state(vessels, cells)
    steady_state_G1 = np.copy(G1.concentration.function.vector().get_local())

    # save for debugging purposes
#    vtk_file = File("G1_steady_state.pvd")
#    vtk_file << G1.concentration.function
#    vtk_file = File("cells.pvd")
#    vtk_file << cells.automata.function
#    vtk_file = File("cells_vegf.pvd")
#    vtk_file << cells.vegf.function
#    vtk_file = File("vessels.pvd")
#    vtk_file << vessels.relative_radius.function

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

    # debugging purposes
    set_log_level(LogLevel.INFO)
    list_timings(TimingClear.keep, [TimingType.wall, TimingType.system])
    print("\nErr", np.max(np.abs(updated_G1 - steady_state_G1)), np.average(np.abs(updated_G1 - steady_state_G1)))
    print("Err", np.max(np.abs(updated_G1 - steady_state_G1)/np.abs(steady_state_G1)))

    # expected check
    print(updated_G1)
    print(steady_state_G1)    
    assert np.all(updated_G1 > 0)
    assert np.all(steady_state_G1 > 0)
    
    if np.all(np.abs(steady_state_G1) > 1e-10):
        assert (np.max(np.abs((updated_G1 - steady_state_G1)
                              /steady_state_G1)) < 0.001)
    else:
        assert (np.max(np.abs(updated_G1 - steady_state_G1)) 
                < 0.001)

class DrugsInVesselsDummy:
    def __init__(self, name, value):
        self.name = name
        self.concentrations = {name:value}

    def get(self, name, time):
        return self.concentrations[name]

if __name__ == '__main__':
    test_Chemotherapies([4, 4], 1e-2, 1234)
