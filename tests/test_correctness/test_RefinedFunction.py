import __init__
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.mesh import BaseMesh
from model_parts.helpers.neighbours_mpi import AllNeighbourVerticesMap
import model_parts.helpers.add_sim_db_to_path
import sim_db
import numpy as np
import cProfile
from dolfin import *
import pytest

@pytest.mark.parametrize("size, n_base_mesh_refinements, n_additional_refinements, degree", [
                         ([4, 4], 0, 0, 1), 
                         ([4, 4], 1, 0, 1),
                         ([4, 4], 2, 2, 1),
                         ([4, 4], 2, 2, 2),
                         ([8, 7], 0, 0, 1),
                         ([8, 7], 2, 0, 1),
                         ([5, 5], 2, 1, 1)
])
@pytest.mark.filterwarnings('ignore')
def test_RefinedFunction(size, n_base_mesh_refinements, n_additional_refinements, 
                         degree, print_progress=True, profile_dir=None, barrier_on=True):
    """Test all of RefinedFunction's methods."""

    def barrier():
        if barrier_on:
            comm.barrier()

    if profile_dir:
        profile = cProfile.Profile()
        profile.enable()

    comm = MPI.comm_world
    rank = comm.Get_rank()

    db_id = None
    if rank == 0:
        sim_database = sim_db.add_empty_sim(False)
        sim_database.write("size", size, "int array")
        sim_database.write("n_refinements", n_base_mesh_refinements, "int")
        sim_database.write("cell_size", 10.0, "float")
        db_id = sim_database.get_id()
        sim_database.close()
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    base_mesh = BaseMesh(sim_database)

    V = FunctionSpace(base_mesh.mesh, 'CG', 1)
    n_intermediate = 2**(n_base_mesh_refinements + n_additional_refinements) - 1
    all_neighbour_vertices_map = AllNeighbourVerticesMap(V, n_intermediate)
    if print_progress and rank == 0:
        print("\nTest all of RefinedFunction's methods.")

        print(("\nMap over which rank that owns each vertex for the possible cell "\
              + "locations:"))
        print((all_neighbour_vertices_map.coord_to_owner_rank))
    barrier()

    refined_func = RefinedFunction(base_mesh, n_additional_refinements, 
                                   degree=degree, allow_neighbour_access=True)

    init_array = None
    if rank == 0:
        if print_progress:
            print("\nTesting the set(init_array) and get_local_at_cell_positions() functions.")
        np.random.seed(0)
        init_array = np.random.randint(100, size=tuple(size))
    init_array = comm.bcast(init_array, root = 0)
    refined_func.set(init_array)
    x_local = refined_func.local_x_at_cell_positions
    y_local = refined_func.local_y_at_cell_positions
    local_size = len(x_local)

    local_indices = size[0]*np.rint(y_local/refined_func.cell_size).astype(int) \
                    + np.rint(x_local/refined_func.cell_size).astype(int)
    correct_values = init_array.reshape(-1)[local_indices]

    assert np.array_equal(refined_func.get_local_at_cell_positions(), correct_values)


    barrier()
    if print_progress and rank == 0:
        print("\nTesting functions from LocalAndNeighbours.")
        print("Testing collect_values() once and get_value_at().")
    refined_func.collect_values()
    if local_size > 0:
        for i in range(local_size):
            value = refined_func.get_value_at(x_local[i], y_local[i])
            if print_progress:
                x = x_local[i]
                y = y_local[i]
                print(("get_value_at({0}, {1}) return {2} for rank ".format(x, y, value) \
                    + "{0}, and it should be {1}.".format(rank, correct_values[i])))
            assert abs(value - correct_values[i]) < 0.001

    barrier()
    if print_progress and rank == 0:
        print("\nTesting set_value_at(), return_values() and reset_priorities.")
    barrier()
    local_size = local_size
    if local_size > 0:
        new_values = np.zeros(local_size)
        values = np.zeros(local_size)
        new_values = np.random.rand(local_size)
        for i in range(local_size):
            refined_func.set_value_at(x_local[i], y_local[i], new_values[i], rank + 1)
        not_set = refined_func.return_values(reset_priorities=False)
        refined_func.reset_priorities()
        refined_func.collect_values()
        for i in range(local_size):
            values[i] = refined_func.get_value_at(x_local[i], y_local[i])
            if print_progress:
                x = x_local[i]
                y = y_local[i]
                print(("{0} was set at ({1}, {2}) from rank ".format(new_values[i], x, y) \
                    + "{0} and the global value became {1}.".format(rank, values[i])))
            if len(not_set) == 0:
                assert values[i] - new_values[i] < 0.0001

    barrier()
    if print_progress and rank == 0:
        print("\nTesting set_local_at_cell_positions().")
    barrier()
    new_local_values = rank*np.ones(len(refined_func.get_local_at_cell_positions()))
    refined_func.set_local_at_cell_positions(new_local_values)
    for value in refined_func.get_local_at_cell_positions():
        assert abs(value - rank) < 0.001

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

    if profile_dir:
        profile.disable()
        profile.dump_stats("{0}test_RefinedFunction_nx{1}".format(profile_dir, size[0]) \
                         + "_ny{0}_p{1}_rank{2}.prof".format(size[1], comm.Get_size(), rank))
                               
if __name__ == '__main__':
    import __init__
    test_RefinedFunction([4,4], 2, 0, 1)
    test_RefinedFunction([5,5], 2, 1, 1)
    test_RefinedFunction([4,8], 2, 3, 1)
    test_RefinedFunction([4,5], 2, 0, 1)
