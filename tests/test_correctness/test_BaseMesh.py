import __init__
from  model_parts.helpers.mesh import BaseMesh
import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
import numpy as np
from dolfin import *
import pytest

@pytest.mark.parametrize("size, n_refinements", [
                          ([3,3], 1),
                          ([4,4], 0),
                          ([3,3], 2),
                          ([4,5], 0),
                          ([5,5], 3)
])
@pytest.mark.filterwarnings('ignore')
def test_BaseMesh(size, n_refinements):
    """Test if an instance of BaseMesh contains the coordinate (0, 0)."""

    comm = MPI.comm_world
    rank = comm.Get_rank()

    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool("sim_db", "add -f "
                "root/tests/test_correctness/parameter_file.txt".split(), False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    if rank == 0:
        sim_database.write("size", size, "int array")
        sim_database.write("n_refinements", n_refinements, "int")
    comm.barrier()

    if rank == 0:
        print("Test if an instance of BaseMesh contains the coordinate (0, 0).")
    base_mesh = BaseMesh(sim_database)
    mesh_size = base_mesh.mesh.hmax()/sqrt(2)

    n_origos = 0 # Number of gridpoints with coordinates (0, 0)
    for coord in base_mesh.mesh.coordinates():
        if abs(coord[0]) < mesh_size/2.0 and abs(coord[1]) < mesh_size/2.0:
            n_origos += 1

    n_origos = comm.gather(n_origos, root=0)
    
    if rank == 0:
        assert sum(n_origos) == 1

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

if __name__ == '__main__':
    test_BaseMesh([3,3], 1)
