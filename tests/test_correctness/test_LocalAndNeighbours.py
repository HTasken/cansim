import __init__
from model_parts.helpers.mesh import BaseMesh 
from model_parts.helpers.neighbours_mpi import LocalAndNeighbours, AllNeighbourVerticesMap
import model_parts.helpers.add_sim_db_to_path
import sim_db
import cProfile
from dolfin import *
import pytest

import sys

@pytest.mark.parametrize("size, n_refinements", [
                         ([4, 4], 1),
                         ([5, 5], 3),
                         ([3, 4], 2),
                         ([8, 7], 0)
])
@pytest.mark.filterwarnings('ignore')
def test_LocalAndNeighbours_with_BaseMesh(size, n_refinements, print_progress=True, 
                                          profile_dir=None):
    """Test all of LocalAndNeighbours' methods using BaseMesh as input."""

    if profile_dir:
        profile = cProfile.Profile()
        profile.enable()

    comm = MPI.comm_world
    rank = comm.Get_rank()

    comm.barrier()
    if print_progress and rank == 0:
        print("\nTest all of LocalAndNeighbours' methods using BaseMesh as input.")

    db_id = None
    if rank == 0:
        sim_database = sim_db.add_empty_sim(False)
        sim_database.write("size", size, "int array")
        sim_database.write("n_refinements", n_refinements, "int")
        sim_database.write("cell_size", 10.0, "float")
        db_id = sim_database.get_id()
        sim_database.close()
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    base_mesh = BaseMesh(sim_database)
    mesh_size = base_mesh.mesh.hmax()/sqrt(2)
    cell_size = float(base_mesh.cell_size)

    V = FunctionSpace(base_mesh.mesh, 'CG', 1)
    u = interpolate(Expression('100*x[0] + x[1]', element=V.ufl_element()), V)

    n_intermediate = 2**n_refinements - 1
    all_neighbour_vertices_map = AllNeighbourVerticesMap(V, n_intermediate)
    if print_progress and rank == 0:
        print("Print owner rank of cells.")
        print(all_neighbour_vertices_map.coord_to_owner_rank)

    comm.barrier()
    if print_progress and rank == 0:
        print("\nTesting collect_values() and get_value_at()")
    local_and_neighbours = LocalAndNeighbours(V, n_intermediate)
    local_and_neighbours.collect_values(u)
    coord = V.tabulate_dof_coordinates()
    x_coord = coord[:,0] # Changed struc of return to tuple in dolfin 2018.1.0
    y_coord = coord[:,1] # Changed struc of return to tuple in dolfin 2018.1.0
    min_x = -cell_size/2**(n_refinements + 1)
    min_y = -cell_size/2**(n_refinements + 1)
    max_x = cell_size * (base_mesh.size[0] - 1) + cell_size/2**(n_refinements + 1)
    max_y = cell_size * (base_mesh.size[1] - 1) + cell_size/2**(n_refinements + 1)
    relative_coord_neighbours = [(-cell_size, -cell_size), (0, -cell_size), \
        (cell_size, -cell_size), (-cell_size, 0), (0, 0), (cell_size, 0), \
        (-cell_size, cell_size), (0, cell_size), (cell_size, cell_size)]
    cell_coord = []
    for (x, y) in zip(x_coord, y_coord):
        if (abs(x % cell_size < cell_size/2**(n_refinements + 1)) \
           or abs(x % cell_size > cell_size - cell_size/2**(n_refinements + 1))) \
           and (abs(y % cell_size < cell_size/2**(n_refinements + 1)) \
           or abs(y % cell_size > cell_size + cell_size/2**(n_refinements + 1))):
            for (dx, dy) in relative_coord_neighbours:
                if (min_x < x + dx < max_x and min_y < y + dy < max_y 
                        and not (x + dx, y + dy) in cell_coord):
                    cell_coord.append((x + dx, y + dy))
                    value = local_and_neighbours.get_value_at(x + dx, y + dy)
                    if print_progress:
                        print("Value at ({0}, {1}) ".format(x + dx, y + dy) \
                            + "is {0} for rank {1} and ".format(value, rank) \
                            + "should be {}.".format(100*(x + dx) + (y + dy)))

                    assert (abs(value - (100*(x + dx) + (y + dy))) < 0.001)

    comm.barrier()
    if print_progress and rank == 0:
        print("\nTesting set_value_at() and return_values().")
    for coord in cell_coord:
        local_and_neighbours.set_value_at(coord[0], coord[1], -rank, rank + 1)
    not_set = local_and_neighbours.return_values(u)
    if len(not_set) > 0:
        coord_not_set = list(list(zip(*not_set))[0]) # Unzipped
    else:
        coord_not_set = []
    local_and_neighbours.collect_values(u)
    for coord in cell_coord:
        value = local_and_neighbours.get_value_at(coord[0], coord[1])
        if print_progress:
            print("The value at ({0}, {1}) was set to {2} on rank {3}, and "
                  "the globel value have become {4}."
                  .format(coord[0], coord[1], -rank, rank, value))

        assert (abs(rank + value) < 0.001) or (coord in coord_not_set) 

    for coord in cell_coord:
        for priority in [2, 3, 1]:
            local_and_neighbours.set_value_at(coord[0], coord[1], \
                                 priority*(100*coord[0] + coord[1]), priority)
    local_and_neighbours.return_values(u, reset_priorities=False)
    for coord in cell_coord:
        value = local_and_neighbours.get_value_at(coord[0], coord[1])
        correct_value = 3*(100*coord[0] + coord[1])
        if print_progress:
            print("The value at ({0}, {1}) on rank {2} ".format(coord[0], coord[1], rank) \
                + "is {0} and should be {1}.".format(value, correct_value))
 
        assert (abs(value - correct_value) < 0.001)

    comm.barrier()
    if print_progress and rank == 0:
        print("\nTesting reset_priorities().")
 
    priority = 2
    for coord in cell_coord:
        local_and_neighbours.set_value_at(coord[0], coord[1],
                             priority*(100*coord[0] + coord[1]), priority, 
                             identifier=(rank+1)*int(100*coord[0] + coord[1] + 1))
    not_set = local_and_neighbours.return_values(u, reset_priorities=False)
    for coord in cell_coord:
        value = local_and_neighbours.get_value_at(coord[0], coord[1])
        correct_value = 3*(100*coord[0] + coord[1])
        if print_progress:
            print("The value at ({0}, {1}) on rank {2} ".format(coord[0], coord[1], rank) \
                + "is {0} and should be {1}.".format(value, correct_value))
 
        assert (abs(value - correct_value) < 0.001)

    for coord, identifier in not_set:
        correct_identifier = (rank + 1)*int(100*coord[0] + coord[1] + 1)
        if print_progress:
            print("The returned identifier for the value not set at ({0}, "
                "{1}) on rank {2} is {3} and should be {4}.".format(coord[0], 
                coord[1], rank, identifier, correct_identifier))

        assert identifier == correct_identifier

    priority = 4
    for coord in cell_coord:
        local_and_neighbours.set_value_at(coord[0], coord[1], \
                             priority*(100*coord[0] + coord[1]), priority)

    local_and_neighbours.return_values(u, reset_priorities=False)
    for coord in cell_coord:
        value = local_and_neighbours.get_value_at(coord[0], coord[1])
        correct_value = priority*(100*coord[0] + coord[1])
        if print_progress:
            print("The value at ({0}, {1}) on rank {2} ".format(coord[0], coord[1], rank) \
                + "is {0} and should be {1}.".format(value, correct_value))
 
        assert (abs(value - correct_value) < 0.001)

    local_and_neighbours.reset_priorities()
    for coord in cell_coord:
        for priority in [2, 3, 1]:
            local_and_neighbours.set_value_at(coord[0], coord[1], \
                                 priority*(100*coord[0] + coord[1]), priority)
    local_and_neighbours.return_values(u, reset_priorities=True)
    for coord in cell_coord:
        value = local_and_neighbours.get_value_at(coord[0], coord[1])
        correct_value = 3*(100*coord[0] + coord[1])
        if print_progress:
            print("The value at ({0}, {1}) on rank {2} ".format(coord[0], coord[1], rank) \
                + "is {0} and should be {1}.".format(value, correct_value))
 
        assert (abs(value - correct_value) < 0.001)

    V = VectorFunctionSpace(base_mesh.mesh, 'CG', 1, dim=3)
    u = interpolate(Expression(('100*x[0] + x[1]', '-100*x[0] - x[1]', '0'), 
                    element=V.ufl_element()), V)

    n_intermediate = 2**n_refinements - 1
    all_neighbour_vertices_map = AllNeighbourVerticesMap(V, n_intermediate)
    if print_progress and rank == 0:
        print("Print owner rank of cells.")
        print(all_neighbour_vertices_map.coord_to_owner_rank)

    comm.barrier()
    if print_progress and rank == 0:
        print("\nTesting collect_values() and get_value_at() with three dim. "
              "vector function.")
    local_and_neighbours = LocalAndNeighbours(V, n_intermediate, 3)
    local_and_neighbours.collect_values(u)
    coord = V.tabulate_dof_coordinates()
    x_coord = coord[:,0] # Changed struc of return to tuple in dolfin 2018.1.0
    y_coord = coord[:,1] # Changed struc of return to tuple in dolfin 2018.1.0
    min_x = -cell_size/2**(n_refinements + 1)
    min_y = -cell_size/2**(n_refinements + 1)
    max_x = cell_size * (base_mesh.size[0] - 1) + cell_size/2**(n_refinements + 1)
    max_y = cell_size * (base_mesh.size[1] - 1) + cell_size/2**(n_refinements + 1)
    relative_coord_neighbours = [(-cell_size, -cell_size), (0, -cell_size), \
        (cell_size, -cell_size), (-cell_size, 0), (0, 0), (cell_size, 0), \
        (-cell_size, cell_size), (0, cell_size), (cell_size, cell_size)]
    cell_coord = []
    for (x, y) in zip(x_coord, y_coord):
        if (abs(x % cell_size < cell_size/2**(n_refinements + 1)) \
           or abs(x % cell_size > cell_size - cell_size/2**(n_refinements + 1))) \
           and (abs(y % cell_size < cell_size/2**(n_refinements + 1)) \
           or abs(y % cell_size > cell_size + cell_size/2**(n_refinements + 1))):
            for (dx, dy) in relative_coord_neighbours:
                if (min_x < x + dx < max_x and min_y < y + dy < max_y):
                    cell_coord.append((x + dx, y + dy))
                    value = local_and_neighbours.get_value_at(x + dx, y + dy)
                    if print_progress:
                        print("Value at ({0}, {1}) ".format(x + dx, y + dy) \
                            + "is {0} for rank {1} and ".format(value, rank) \
                            + "should be {}.".format(100*(x + dx) + (y + dy)))

                    assert ((abs(value[0] - (100*(x + dx) + (y + dy))) 
                            + abs(value[1] + (100*(x + dx) + (y + dy))))
                            < 0.001)
                    assert abs(value[2]) < 0.001

    comm.barrier()
    if print_progress and rank == 0:
        print("\nTesting set_value_at() and return_values() with three dim. "
              "vector function.")
    for coord in cell_coord:
        local_and_neighbours.set_value_at(coord[0], coord[1], (-rank, rank, 0), rank + 1)
    not_set = local_and_neighbours.return_values(u)
    if len(not_set) > 0:
        coord_not_set = list(list(zip(*not_set))[0]) # Unzipped
    else:
        coord_not_set = []
    local_and_neighbours.collect_values(u)
    for coord in cell_coord:
        value = local_and_neighbours.get_value_at(coord[0], coord[1])
        if print_progress:
            print("The value at ({0}, {1}) was set to {2} on rank {3}, and "
                  "the globel value have become {4}."
                  .format(coord[0], coord[1], (-rank, rank, 0), rank, value))

        assert ((abs(rank + value[0]) < 0.001 and abs(rank - value[1]) < 0.001)
                or (coord in coord_not_set))

    for priority in [2, 3, 1]:
        for coord in cell_coord:
            v = priority*(100*coord[0] + coord[1])
            local_and_neighbours.set_value_at(coord[0], coord[1], \
                                 (v, -v, 0), priority)
    local_and_neighbours.return_values(u, reset_priorities=False)
    for coord in cell_coord:
        value = local_and_neighbours.get_value_at(coord[0], coord[1])
        v = 3*(100*coord[0] + coord[1])
        correct_value = (v, -v, 0)
        if print_progress:
            print("The value at ({0}, {1}) on rank {2} ".format(coord[0], coord[1], rank) \
                + "is {0} and should be {1}.".format(value, correct_value))
 
        assert (abs(value[0] - correct_value[0]) 
                + abs(value[1] - correct_value[1])
                + abs(value[2] - correct_value[2]) < 0.001)

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

    if profile_dir:
        profile.disable()
        profile.dump_stats("{0}test_LocalAndNeighbours_nx{1}".format(profile_dir, size[0]) \
                         + "_ny{0}_p{1}_rank{2}.prof".format(size[1], comm.Get_size(), rank))
 
if __name__ == '__main__':
    test_LocalAndNeighbours_with_BaseMesh([3,3], 0, print_progress=True)
