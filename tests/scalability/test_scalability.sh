#!/bin/bash

name_tested=$1

max_n_processes=$2

size_strong=$3 # 100 is usually a good number

size_weak_closest_rounded_to=$4

printf "Test %s up to %d with size %d " $name_tested $max_n_processes $size_strong
printf "for strong scaling\nand weak size rounded to closest %d.\n\n" $size_weak_closest_rounded_to

function round_to_int(){
    local res=0
    LC_NUMERIC="en_US.UTF-8" res=$(printf "%.0f" $1)
    echo $res
}

# Take logaritme and round to integer.
log2_max_n=$(echo "l($max_n_processes)/l(2)" | bc -l)
log2_max_n=$(round_to_int $log2_max_n)

# Calculate size per process used to testing weak scaling for the 
# weak and strong scaling tests to take rougkly the same amount of time given
# good scaling.
(( size_per_process_weak=$size_strong*2/($log2_max_n + 1) ))

# Round to closest 'closest_rounded_to'
(( size_per_process_weak=$size_per_process_weak/$size_weak_closest_rounded_to ))
size_per_process_weak=$(round_to_int $size_per_process_weak)
(( size_per_process_weak=$size_per_process_weak*$size_weak_closest_rounded_to ))

if [ "$size_per_process_weak" == "0" ]; then
    echo "size_per_process_weak is 0"
    exit
fi

printf "Test strong scalability of the ${name_tested}.\n"
i=0
until [ $i -gt $log2_max_n ]; do
    (( n_processors=2**$i ))
    if [ "$name_tested" == "LocalAndNeighbours" ]; then
        mpirun -n $n_processors python run_tests.py LocalAndNeighbours $size_strong_lan $size_strong_lan
    elif [ "$name_tested" == "RefinedFunction" ]; then
        mpirun -n $n_processors python run_tests.py RefinedFunction $size_strong_rf $size_strong_rf
    elif [ "$name_tested" == "RefinedFunction_heat_eq" ]; then
        mpirun -n $n_processors python run_tests.py RefinedFunction_heat_eq $size_strong $size_strong 0 10
    fi
    (( i++ ))
done

printf "\nTest weak scalability of the ${name_tested}.\n"
i=0
until [ $i -gt $log2_max_n ]; do
    (( n_processors=2**$i ))
    (( i_x=($i + 2)/2 ))
    (( i_y=($i + 3)/2 ))
    (( n_x=$size_per_process_weak*$i_x ))
    (( n_y=$size_per_process_weak*$i_y ))
     if [ "$name_tested" == "LocalAndNeighbours" ]; then
        mpirun -n $n_processors python run_tests.py LocalAndNeighbours $n_x $n_y
    elif [ "$name_tested" == "RefinedFunction" ]; then
        mpirun -n $n_processors python run_tests.py RefinedFunction $n_x $n_y
    elif [ "$name_tested" == "RefinedFunction_heat_eq" ]; then
        mpirun -n $n_processors python run_tests.py RefinedFunction_heat_eq $n_x $n_y 0 10
    fi
    (( i++ ))
done
