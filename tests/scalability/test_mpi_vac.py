import __init__
from model_parts.helpers.mesh import BaseMesh
from model_parts.oxygen import Oxygen
from model_parts.vegf_avastin import VegfAvastin
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.drugs_in_vessels import DrugsInVessels

import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
import os
import numpy as np
import input_output.log as log
from dolfin import *
import pytest
set_log_level(30)


parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True

def test_mpi_vac(target_dofs, seed, print_progress=True):
    comm = MPI.comm_world
    rank = MPI.rank(comm)
    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
            argv="add -f "
            "root/tests/scalability/parameter_file.txt".split(),
            print_ids_added=False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    num_processes = int(comm.Get_size())
    N = num_processes*target_dofs
    Nx = 1
    while (Nx*Nx < N):
        Nx += 1
        Ny = Nx - 1
        size = [Nx, Ny]
    if rank == 0:
        sim_database.write("size", size, "int array")
        sim_database.write("seed", seed, "int")
        sim_database.write("vac_max_dt", 10.0, "float")
        sim_database.write("vac_dt_after_cell_update", 1, "float")
        sim_database.write("vac_dt_increase_factor_after_cell_update", 1.00, "float")
        sim_database.write("degree_vac", 1, "int")
        sim_database.write("n_refinements", 2, "int")
        sim_database.write("n_refinements_vac", 0, "int")
    comm.barrier()
    log.log('Begin Initialisation','w')
    base_mesh = BaseMesh(sim_database)
    log.log('Create Mesh')    
    drugs_in_vessels = DrugsInVesselsDummy(0.0)
    log.log('Initialise drug in vessels')        
    cells = Cells(sim_database, base_mesh)
    log.log('Initialise cell')
    vessels = Vessels(sim_database, base_mesh)
    log.log('Initalise vessels')
    oxygen = Oxygen(sim_database, base_mesh)
    log.log('Initalise oxygen')
    vegf_avastin = VegfAvastin(sim_database, base_mesh, drugs_in_vessels)
    log.log('Initalise VAC')

    oxygen.solve_steady_state(vessels, cells)
    log.log('Solve Oxygen Steady State')
    cells.solve_vegf_steady_state(oxygen)
    log.log('Solve VEGFs')    

#    e = Expression(("c1", "c2", "c3"), c1=1,c2=0,c3=0, degree=1)

#   vegf_avastin.VAC.function.interpolate(e)
#    vegf_avastin.VAC.prev_function.interpolate(e)

    end_time = 30 # Choosen to be long enough to be close to steady state.
    for t_update in range(0,end_time*2,end_time):
        vegf_avastin.update(t_update, True, vessels, cells, oxygen)
    updated_vegf_avastin = np.copy(vegf_avastin.VAC.function.vector().get_local())

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

    casedir = './VAC_MPI_timing/VAC.' + str(num_processes)
    if rank == 0:
        for i in range(100):
            try:
                os.makedirs(casedir + '.' + str(i))
                casedir = casedir + '.' + str(i)
                break
            except FileExistsError as error:
                print(f"Directory {casedir}.{str(i)} already exists")
    casedir = comm.bcast(casedir, root=0)
    
    if rank == 0:
        with open(casedir+'/summary.out','w+') as f:
            f.write('Number of Processes:{}\n'.format(num_processes))
            f.write('Total dofs:{}\n'.format(vegf_avastin.VAC.function_space.dim()))
    with open(casedir+'/summary.out','a') as f:
        f.write('dof on processor {} :{}\n'.format(rank, len(vegf_avastin.VAC.function_space.dofmap().dofs())))
    log.log('Simulation ends')
    dump_timings_to_xml(f"{casedir}/timings.xml", TimingClear.clear)    
class DrugsInVesselsDummy:
    def __init__(self, value):
        self.value = value

    def get(self, name, time):
        return self.value

if __name__ == "__main__":
    for i in range(1):
        test_mpi_vac(1e4, 1234+i, False)
        print('Rep {} finished'.format(i))
