import matplotlib.pyplot as plt
import numpy as np

n_processes = [1, 2, 4, 8, 16, 32]
lan_strong_time = [43.3, 36.8, 21.8, 12.6, 9.5, 8.9]
lan_weak_time = [5.1, 7.2, 9.2, 9.0, 8.4, 11.1]
rf_strong_time = [23.3, 26.3, 17.5, 10.7, 7.1, 6.3]
rf_weak_time = [2.9, 5.0, 6.9, 8.1, 6.9, 7.3]

n = len(n_processes)
lan_strong_speedup = np.zeros(n)
lan_weak_speedup = np.zeros(n)
rf_strong_speedup = np.zeros(n)
rf_weak_speedup = np.zeros(n)
for i in range(n):
    lan_strong_speedup[i] = lan_strong_time[0]/float(lan_strong_time[i])
    lan_weak_speedup[i] = lan_weak_time[0]/float(lan_weak_time[i])
    rf_strong_speedup[i] = rf_strong_time[0]/float(rf_strong_time[i])
    rf_weak_speedup[i] = rf_weak_time[0]/float(rf_weak_time[i])

plt.figure()
plt.loglog(n_processes, lan_strong_speedup)
plt.loglog(n_processes, n_processes)
plt.title('lan strong')

plt.figure()
plt.plot(n_processes, lan_weak_speedup)
plt.plot(n_processes, np.ones(n))
plt.title('lan weak')

plt.figure()
plt.loglog(n_processes, rf_strong_speedup)
plt.loglog(n_processes, n_processes)
plt.title('rf strong')

plt.figure()
plt.loglog(n_processes, rf_weak_speedup)
plt.loglog(n_processes, np.ones(n))
plt.title('lan weak')

plt.show()
