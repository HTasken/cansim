import __init__
from model_parts.helpers.mesh import BaseMesh
from model_parts.chemo import Chemotherapy
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.drugs_in_vessels import DrugsInVessels
import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
import os
import numpy as np
from dolfin import *
import pytest
import input_output.log as log
set_log_level(30)

def test_mpi_chemo(target_dofs, seed, print_progress=True):
    comm = MPI.comm_world
    rank = MPI.rank(comm)
    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
            argv="add -f "
            "root/tests/scalability/parameter_file.txt".split(),
            print_ids_added=False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    num_processes = int(comm.Get_size())
    N = num_processes*target_dofs
    Nx = 1
    while (Nx*Nx < N):
        Nx += 1
        Ny = Nx - 1
    size = [Nx, Ny]

    if rank == 0:
        sim_database.write("size", size, "int array")
        sim_database.write("seed", seed, "int")
        sim_database.write("chemo_dt_at_injection", [1, 1, 1], "float array")
        sim_database.write("chemo_dt_increase_factor_after_injection", [1.00, 1.00, 1.00], "float array")
        sim_database.write("chemo_dt_increase_factor_after_vessel_update", [1.00, 1.00, 1.00], "float array")
        sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
        chemo_sensitivity = [10000, 10000, 10000]
        sim_database.write("degree_chemo", [1, 1, 1], "int array")
        sim_database.write("n_refinements_chemo", [0, 0, 0], "int array")
        sim_database.write("chemo_sensitivity", chemo_sensitivity, "float array")
    comm.barrier()
    log.log('Begin Initialisation','w')    
    timer = Timer('Whole test')
    base_mesh = BaseMesh(sim_database)
    log.log('Initialise mesh')
    drugs_in_vessels = DrugsInVesselsDummy('G1',10.0)
    cells = Cells(sim_database, base_mesh)
    log.log('Initialise cells')       
    vessels = Vessels(sim_database, base_mesh)
    log.log('Initialise vessels')    
    G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')
    log.log('Initialise drugs')

    e = Expression("constant", constant=1e-4, degree=1)

    G1.concentration.function.interpolate(e)
    G1.concentration.prev_function.interpolate(e)
    log.log('Initalise')
    end_time = 10
    time_step = 10
    
    i = 0
    
#    for dt in range(time_step, end_time, time_step):
    num_iter = G1.update(end_time, True, vessels, cells)

    updated_G1 = np.copy(G1.concentration.function.vector().get_local())

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()
    timer.stop()
    casedir = './chemo_MPI_timing/chemo.' + str(num_processes)
    if rank == 0:
        for i in range(100):
            try:
                os.makedirs(casedir + '.' + str(i))
                casedir = casedir + '.' + str(i)
                break
            except FileExistsError as error:
                print(f"Directory {casedir}.{str(i)} already exists")
    casedir = comm.bcast(casedir, root=0)
    
    if rank == 0:
        with open(casedir+'/summary.out','w+') as f:
            f.write('Number of Krylov iterations:{}\n'.format(num_iter))
            f.write('Number of Processes:{}\n'.format(num_processes))
            f.write('Total dofs:{}\n'.format(G1.concentration.function_space.dim()))
            f.write('Average dof per process:{}\n'.format(int(G1.concentration.function_space.dim()/num_processes)))
    with open(casedir+'/summary.out','a') as f:
        f.write('dof on processor {} :{}\n'.format(rank, len(G1.concentration.function_space.dofmap().dofs())))
    dump_timings_to_xml(f"{casedir}/timings.xml", TimingClear.clear)
    log.log('Simulation ends')
class DrugsInVesselsDummy:
    def __init__(self, name, value):
        self.name = name
        self.concentrations = {name:value}

    def get(self, name, time):
        return self.concentrations[name]

if __name__ == '__main__':
    for i in range(1):    
        test_mpi_chemo(1e4, 1234+i, False)
