#!/bin/bash

max_n_processes=$1

# Size used to test strong scaling for the LocalAndNeighbours class.
size_strong_lan=100

# Size used to test strong scaling for the RefinedFunction class.
size_strong_rf=100

function round_to_int(){
    local res=0
    LC_NUMERIC="en_US.UTF-8" res=$(printf "%.0f" $1)
    echo $res
}

# Take logaritme and round to integer.
log2_max_n=$(echo "l($max_n_processes)/l(2)" | bc -l)
log2_max_n=$(round_to_int $log2_max_n)

# Calculate size per process used to testing weak scaling for the 
# LocalAndNeighbours and RefinedFunction class respectfully, such that the weak 
# and strong scaling test takes roughly the same amount of time given good scaling.
(( size_per_process_weak_lan=$size_strong_lan*2/($log2_max_n + 1) ))
(( size_per_process_weak_rf=$size_strong_rf*2/($log2_max_n + 1) ))

# Round to closest 'closest_rounded_to'
closest_rounded_to=1
(( size_per_process_weak_lan=$size_per_process_weak_lan/$closest_rounded_to ))
size_per_process_weak_lan=$(round_to_int $size_per_process_weak_lan)
(( size_per_process_weak_lan=$size_per_process_weak_lan*$closest_rounded_to ))
(( size_per_process_weak_rf=$size_per_process_weak_rf/$closest_rounded_to ))
size_per_process_weak_rf=$(round_to_int $size_per_process_weak_rf)
(( size_per_process_weak_rf=$size_per_process_weak_rf*$closest_rounded_to ))

if [ "$size_per_process_weak_lan" == "0" -o "$size_per_process_weak_rf" == "0" ]; then
    echo "size_per_process_weak_lan or size_per_process_weak_rf is 0"
    exit
fi

printf "Test strong scalability of the LocalAndNeighbours class.\n"
i=0
until [ $i -gt $log2_max_n ]; do
    (( n_processors=2**$i ))
    mpirun -n $n_processors python run_tests.py LocalAndNeighbours $size_strong_lan $size_strong_lan
    (( i++ ))
done

printf "\nTest weak scalability of the LocalAndNeighbours class.\n"
i=0
until [ $i -gt $log2_max_n ]; do
    (( n_processors=2**$i ))
    (( i_x=($i + 2)/2 ))
    (( i_y=($i + 3)/2 ))
    (( n_x=$size_per_process_weak_lan*$i_x ))
    (( n_y=$size_per_process_weak_lan*$i_y ))
    mpirun -n $n_processors python run_helpers_tests.py LocalAndNeighbours $n_x $n_y
    (( i++ ))
done

printf "\nTest strong scalability of the RefinedFunction class.\n"
i=0
until [ $i -gt $log2_max_n ]; do
    (( n_processors=2**$i ))
    mpirun -n $n_processors python run_helpers_tests.py RefinedFunction $size_strong_rf $size_strong_rf

    (( i++ ))
done

printf "\nTest weak scalability of RefinedFunction class.\n"
i=0
until [ $i -gt $log2_max_n ]; do
    (( n_processors=2**$i ))
    (( i_x=($i + 2)/2 ))
    (( i_y=($i + 3)/2 ))
    (( n_x=$size_per_process_weak_lan*$i_x ))
    (( n_y=$size_per_process_weak_lan*$i_y ))
    mpirun -n $n_processors python run_helpers_tests.py RefinedFunction $n_x $n_y
    (( i++ ))
done
