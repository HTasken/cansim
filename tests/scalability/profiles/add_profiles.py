# Add together profile files.
# Run with: python add_profiles.py result_file.prof profile_name_with_wildcard

import pstats
import os
import fnmatch
import sys

first = True
for i in range(2, len(sys.argv)):
    filename = str(sys.argv[i])
    if first:
        try:
            p = pstats.Stats(filename)
            first = False
        except:
            pass
    else:
        try:
            p.add(filename)
        except:
            pass
try:
    p.dump_stats(str(sys.argv[1]))
except:
    print "Could not create {}.".format(str(sys.argv[1]))
    
