#!/bin/bash

# Add profiles from test_LocalAndNeighbours and test_RefinedFunction and 
# remove the individual profiles.
# Does also open snakeviz.

python add_profiles.py test_LocalAndNeighbours.prof test_LocalAndNeighbours_nx*
rm test_LocalAndNeighbours_nx* || true

python add_profiles.py test_RefinedFunction_heat_eq.prof test_RefinedFunction_on_heat_equation_nx*
rm test_RefinedFunction_on_heat_equation_nx* || true

python add_profiles.py test_RefinedFunction.prof test_RefinedFunction_nx*
rm test_RefinedFunction_nx* || true

(
    snakeviz test_LocalAndNeighbours.prof
) &
(
    snakeviz test_RefinedFunction.prof
) &
(
    snakeviz test_RefinedFunction_heat_eq.prof
)
