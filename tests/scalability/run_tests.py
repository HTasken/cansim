import __init__
import numpy as np
import sys
from test_correctness.test_LocalAndNeighbours import test_LocalAndNeighbours_with_BaseMesh
from test_correctness.test_RefinedFunction import test_RefinedFunction
from test_correctness.test_RefinedFunction_on_heat_equation import test_RefinedFunction_on_heat_equation
from dolfin import *
import time

# Directory to store cProfiles. Set to None to not take cProfiles
profile_directory="profiles/"

set_log_level(ERROR)

comm = mpi_comm_world().tompi4py()
n_processes = comm.Get_size()
name_tested = str(sys.argv[1])
start_time = time.time()
if name_tested == "LocalAndNeighbours":
    nx = int(sys.argv[2])
    ny = int(sys.argv[3])
    test_LocalAndNeighbours_with_BaseMesh([n_x, n_y], 2, print_progress=False, 
                                          profile_dir=profile_directory)
elif name_tested == "RefinedFunction":
    nx = int(sys.argv[2])
    ny = int(sys.argv[3])
    test_RefinedFunction([n_x, n_y], 2, 0, 1, print_progress=False,
                         profile_dir=profile_directory)
elif name_tested == "RefinedFunction_heat_eq":
    nx = int(sys.argv[2])
    ny = int(sys.argv[3])
    n_refinements = int(sys.argv[4])
    n_time_steps = int(sys.argv[5])
    test_RefinedFunction_on_heat_equation([nx, ny], n_refinements, n_time_steps, 
                           print_progress=False, profile_dir=profile_directory)
else:
    print "No matching 'name_tested' found."
    exit(-1)
used_time = time.time() - start_time
time_per_process = used_time/float(n_processes)
if comm.Get_rank() == 0:
    if name_tested == "LocalAndNeighbours" or name_tested == "RefinedFunction":
        print "{0} possible cells, {1} processes,".format(nx*ny, n_processes), \
          "{0:.1f} sec {1:.1f} sec/process.".format(used_time, time_per_process)
    elif name_tested == "RefinedFunction_heat_eq":
        print "{0} processes, {1} size, ".format(n_processes, nx*ny), \
          "{0} refinements, {1} time steps, ".format(n_refinements, n_time_steps), \
          "{0:.1f} sec.".format(used_time)
