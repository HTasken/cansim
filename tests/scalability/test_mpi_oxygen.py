import __init__
from model_parts.helpers.mesh import BaseMesh
from model_parts.oxygen import Oxygen
from model_parts.cells import Cells
from model_parts.vessels import Vessels
import model_parts.helpers.add_sim_db_to_path
import sim_db
import sim_db.src_command_line_tool.command_line_tool as sdb
import os
import numpy as np
from dolfin import *
import pytest
import input_output.log as log

set_log_level(30)

def test_mpi_oxygen(target_dofs, seed, print_progress=True):
    comm = MPI.comm_world
    rank = MPI.rank(comm)
    db_id = None
    if rank == 0:
        db_id = sdb.command_line_tool(
            argv="add -f "
            "root/tests/scalability/parameter_file.txt".split(),
            print_ids_added=False)
    db_id = comm.bcast(db_id, root=0)
    comm.barrier()

    sim_database = sim_db.SimDB(db_id=db_id)

    num_processes = int(comm.Get_size())
    N = num_processes*target_dofs
    Nx = 1
    while (Nx*Nx < N):
        Nx += 1
        Ny = Nx - 1
    size = [Nx, Ny]

    if rank == 0:
        sim_database.write("size", size, "int array")
        sim_database.write("seed", seed, "int")
        sim_database.write("oxygen_max_dt", 10.0, "float")
        sim_database.write("oxygen_dt_after_cell_update", 1.0, "float")
        sim_database.write("oxygen_dt_increase_factor_after_cell_update", 1.0, "float")
        sim_database.write("degree_oxygen", 1, "int")
        sim_database.write("n_refinements_oxygen", 0, "int")
        sim_database.write("tot_time", 0.00001, "float") # To make calc. drug fast
    comm.barrier()
    log.log('Begin Initialisation','w')    
    base_mesh = BaseMesh(sim_database)
    cells = Cells(sim_database, base_mesh)
    vessels = Vessels(sim_database, base_mesh)
    oxygen = Oxygen(sim_database, base_mesh)

    e = Expression("constant", constant=1e-4, degree=1)

    oxygen.concentration.function.interpolate(e)
    oxygen.concentration.prev_function.interpolate(e)

    end_time = 30

    oxygen.update_linearized(end_time, True, vessels, cells)
    updated_oxygen = np.copy(oxygen.concentration.function.vector().get_local())
    #run_time = timings(TimingClear.clear, [TimingType.wall,TimingType.system])

    comm.barrier()
    sim_database.delete_from_database()
    comm.barrier()

    sim_database.close()

    casedir = './oxygen_MPI_timing/oxygen.' + str(num_processes)
    if rank == 0:
        for i in range(100):
            try:
                os.makedirs(casedir + '.' + str(i))
                casedir = casedir + '.' + str(i)
                break
            except FileExistsError as error:
                print(f"Directory {casedir}.{str(i)} already exists")
    casedir = comm.bcast(casedir, root=0)
#    ff = File(MPI.comm_self,
#              "%s/timings_rank_%d.xml" % (casedir, rank))
#    ff << run_time
    dump_timings_to_xml(f"{casedir}/timings.xml", TimingClear.clear)

    if rank == 0:
        with open(casedir+'/summary.out','w+') as f:
            f.write('Number of Processes:{}\n'.format(num_processes))
            f.write('Total dofs:{}\n'.format(oxygen.concentration.function_space.dim()))
#            f.write('Average dof per process:{}\n'.format(int(oxygen.concentration.function_space.dim()/num_processes)))
    with open(casedir+'/summary.out','a') as f:
        f.write('dof on processor {} :{}\n'.format(rank, len(oxygen.concentration.function_space.dofmap().dofs())))
    log.log('Simulation ends')
class DrugsInVesselsDummy:
    def __init__(self, name, value):
        self.name = name
        self.concentrations = {name:value}

    def get(self, name, time):
        return self.concentrations[name]

if __name__ == '__main__':
    for i in range(1):
        test_mpi_oxygen(1e4, 1234+i, False)
