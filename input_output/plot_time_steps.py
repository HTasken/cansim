import __init__
import model_parts.helpers.add_sim_db_to_path
import sim_db
import argparse
import matplotlib.pyplot as plt
import numpy as np
import warnings

parser = argparse.ArgumentParser(description='Plot ')
parser.add_argument('--id', '-i', required=True, type=int, help="'ID' of the simultion in 'sim_db' database for which to the time-steps between the different PDE's.")
args = parser.parse_args()

sd = sim_db.SimDB(db_id=args.id)

oxygen_solve_times = np.loadtxt(sd.read('results_dir') + '/oxygen_solve_times.txt')
G1_solve_times = np.loadtxt(sd.read('results_dir') + '/G1_solve_times.txt')
G2_solve_times = np.loadtxt(sd.read('results_dir') + '/G2_solve_times.txt')
G3_solve_times = np.loadtxt(sd.read('results_dir') + '/G3_solve_times.txt')
vegf_avastin_solve_times = np.loadtxt(sd.read('results_dir') + '/vegf_avastin_solve_times.txt')
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    G1_nonlinear_times = np.loadtxt(sd.read('results_dir') + '/G1_nonlinear_times.txt')
    G2_nonlinear_times = np.loadtxt(sd.read('results_dir') + '/G2_nonlinear_times.txt')
    G3_nonlinear_times = np.loadtxt(sd.read('results_dir') + '/G3_nonlinear_times.txt')

oxygen_dt = oxygen_solve_times[1:] - oxygen_solve_times[:-1]
G1_dt = G1_solve_times[1:] - G1_solve_times[:-1]
G2_dt = G2_solve_times[1:] - G2_solve_times[:-1]
G3_dt = G3_solve_times[1:] - G3_solve_times[:-1]
vegf_avastin_dt = vegf_avastin_solve_times[1:] - vegf_avastin_solve_times[:-1]
G1_zeros = np.zeros(len(G1_nonlinear_times))
G2_zeros = np.zeros(len(G2_nonlinear_times))
G3_zeros = np.zeros(len(G3_nonlinear_times))

plt.plot(oxygen_solve_times[:-1], oxygen_dt, 'b', label='oxygen')
plt.plot(G1_solve_times[:-1], G1_dt, 'r', label='G1')
plt.plot(G2_solve_times[:-1], G2_dt, 'm', label='G2')
plt.plot(G3_solve_times[:-1], G3_dt, 'y', label='G3')
plt.plot(vegf_avastin_solve_times[:-1], vegf_avastin_dt, 'g', label='vegf_avastin')
plt.plot(G1_nonlinear_times, G1_zeros, 'rx')
plt.plot(G2_nonlinear_times, G2_zeros, 'mx')
plt.plot(G3_nonlinear_times, G3_zeros, 'yx', label='nonlinear')
plt.legend()
plt.savefig("time_steps_{0}.png".format(args.id))
