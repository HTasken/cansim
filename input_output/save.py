import __init__
import model_parts.cells as cells
import model_parts.vessels as vesssels
import model_parts.oxygen as oxygen
import model_parts.chemo as chemo
import model_parts.vegf_avastin as vegf_avastin
import model_parts.helpers.add_sim_db_to_path
import sim_db
from dolfin import *
import numpy as np
import os.path


class Save:
    def __init__(self, sim_database, results_dir):
        self.unique_res_dir = sim_database.read("results_dir")
        if self.unique_res_dir == None and MPI.comm_world.Get_rank() == 0:
            self.unique_res_dir = sim_database.unique_results_dir(results_dir)
        self.unique_res_dir = MPI.comm_world.bcast(self.unique_res_dir, root=0)

        self.save_vtk_files = False
        if isinstance(sim_database.read('dt_visualise'), float):
            if sim_database.read('dt_visualise') > 0.001:
                self.save_vtk_files = True
        self.save_hdf5_files = False
        if isinstance(sim_database.read('dt_checkpoint'), float):
            if sim_database.read('dt_checkpoint') > 0.001:
                self.save_hdf5_files = True

        if self.save_vtk_files:
            self.vtk_cells = File(
                    os.path.join(self.unique_res_dir, "cells.pvd"))
            self.vtk_vessels = File(
                    os.path.join(self.unique_res_dir, "vessels.pvd"))
            self.vtk_oxygen = File(
                    os.path.join(self.unique_res_dir, "oxygen.pvd"))
            self.vtk_G1 = File(os.path.join(self.unique_res_dir, "G1.pvd"))
            self.vtk_G2 = File(os.path.join(self.unique_res_dir, "G2.pvd"))
            self.vtk_G3 = File(os.path.join(self.unique_res_dir, "G3.pvd"))
            self.vtk_vegf_avastin = File(
                    os.path.join(self.unique_res_dir, "vegf_avastin.pvd"))
            self.vtk_cellcycle = File(
                    os.path.join(self.unique_res_dir, "cellcycle.pvd"))
            self.vtk_v = File(
                    os.path.join(self.unique_res_dir, "vegf.pvd"))
            self.vtk_p53 = File(
                    os.path.join(self.unique_res_dir, "p53.pvd"))
            self.vtk_vegfs = File(
                    os.path.join(self.unique_res_dir, "vegfs.pvd"))        
        if self.save_hdf5_files:
            self.n_checkpoints = 0

    def vtk(self, vessels, cells, oxygen, G1, G2, G3, vegf_avastin):
        if self.save_vtk_files:
            self.vtk_vessels << vessels.relative_radius.function
            self.vtk_cells << cells.automata.function
            self.vtk_cellcycle << cells.cell_cycle.function
            self.vtk_oxygen << oxygen.concentration.function
            self.vtk_G1 << G1.concentration.function
            self.vtk_G2 << G2.concentration.function
            self.vtk_G3 << G3.concentration.function
            self.vtk_vegf_avastin << vegf_avastin.VAC.function
            self.vtk_v << vegf_avastin.V.function
            self.vtk_p53 << cells.p53.function
            self.vtk_vegfs << cells.vegf.function

    def hdf5(self, sim_database, time, vessels, cells, oxygen, G1, G2, G3,
             vegf_avastin, filemode='a'):
        with HDF5File(MPI.comm_world,
                      os.path.join(self.unique_res_dir, "functions.hdf5"), filemode) as f:
            f.write(vessels.relative_radius.function, "vessels",
                    time)
            f.write(cells.automata.function, "cells", time)
            f.write(cells.cell_cycle.function, "cell_cycle", time)
            f.write(oxygen.concentration.function, "oxygen", time)
            f.write(G1.concentration.function, "G1", time)
            f.write(G2.concentration.function, "G2", time)
            f.write(G3.concentration.function, "G3", time)
            f.write(vegf_avastin.VAC.function, "vegf_avastin",
                    time)
            f.write(vegf_avastin.V.function, "vegf",
                    time)
            f.write(cells.p53.function, 'p53', time)
            f.write(cells.vegf.function, 'vegfs', time)
        if filemode=='a':
            self.n_checkpoints += 1
        else:
            self.n_checkpoints = 1
        if MPI.comm_world.Get_rank() == 0:
            sim_database.write("n_checkpoints", self.n_checkpoints, "int")
            sim_database.write("time_last_checkpoint", time, "float")

    def timings(self):
        dump_timings_to_xml(
                os.path.join(self.unique_res_dir, "timings.xml"),
                TimingClear.clear)

    def solve_times(self, oxygen, G1, G2, G3, vegf_avastin):
        if MPI.comm_world.Get_rank() == 0:
            np.savetxt(
                    os.path.join(self.unique_res_dir,
                                 "oxygen_solve_times.txt"),
                    np.array(oxygen.times_solved))
            np.savetxt(
                    os.path.join(self.unique_res_dir, "G1_solve_times.txt"),
                    np.array(G1.times_solved))
            np.savetxt(
                    os.path.join(self.unique_res_dir, "G2_solve_times.txt"),
                    np.array(G2.times_solved))
            np.savetxt(
                    os.path.join(self.unique_res_dir, "G3_solve_times.txt"),
                    np.array(G3.times_solved))
            np.savetxt(
                    os.path.join(self.unique_res_dir,
                                 "vegf_avastin_solve_times.txt"),
                    np.array(vegf_avastin.times_solved))
            np.savetxt(
                    os.path.join(self.unique_res_dir,
                                 "G1_nonlinear_times.txt"),
                    np.array(G1.times_solved_nonlinearly))
            np.savetxt(
                    os.path.join(self.unique_res_dir,
                                 "G2_nonlinear_times.txt"),
                    np.array(G2.times_solved_nonlinearly))
            np.savetxt(
                    os.path.join(self.unique_res_dir,
                                 "G3_nonlinear_times.txt"),
                    np.array(G3.times_solved_nonlinearly))


if __name__ == '__main__':
    import os
    from model_parts.drugs_in_vessels import DrugsInVessels
    from model_parts.cells import Cells
    from model_parts.vessels import Vessels
    from model_parts.oxygen import Oxygen
    from model_parts.chemo import Chemotherapy
    from model_parts.vegf_avastin import VegfAvastin
    from model_parts.helpers.mesh import BaseMesh

    n = 5

    timer = Timer("test")

    # Just to make sure vessel_init_filename and cell_init_filename is in the
    # database.
    sim_database = sim_db.add_empty_sim(False)
    sim_database.write("vessel_init_filename", "empty_vessel_init_file.txt", "string")
    sim_database.write("cell_init_filename", "empty_cell_init_file.txt", "string")
    sim_database.delete_from_database()
    sim_database.close()

    sim_database = sim_db.add_empty_sim(False)
    # The sim_database.make_unique_subdir function needs a name.
    sim_database.write("name", "save_test", "string")
    # DrugsInVessels
    sim_database.write("tot_time", 12, "float")
    sim_database.write("vac_dt_at_injection", 0.01, "float")
    sim_database.write("chemo_dt_at_injection", [0.01, 0.01, 0.01], "float array")
    sim_database.write("vac_max_dt", 10.0, "float")
    sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
    sim_database.write("height", 160, "float")
    sim_database.write("weight", 58.2, "float")
    dose = [600, 100, 600, 15]
    sim_database.write("dose", dose, "float array")
    schedule = [3, 3, 3, 3]
    sim_database.write("schedule", schedule, "float array")
    # BaseMesh
    sim_database.write("size", [n, n], "int array")
    sim_database.write("n_refinements", 2, "int")
    sim_database.write("cell_size", 10.0, "float")
    # Cells
    sim_database.write("seed", 1234, "int")
    sim_database.write("frac_cancer", 0.2, "float")
    sim_database.write("frac_normal", 0.1, "float")
    sim_database.write("n_refinements_cells", 0, "int")
    sim_database.write("t_cell", 14.69, "float")
    sim_database.write("k_phi", 1.4, "float")
    # Vessels
    sim_database.write("max_dt_drugs_in_vessels", 1.5, "float")
    sim_database.write("n_refinements_vessels", 0, "int")
    sim_database.write("n_other_rngs", 1000, "int")
    sim_database.write("vessel_sep", 3, "int")
    sim_database.write("init_vessel_density", 0.1197, "float")
    sim_database.write("avg_init_vessel_radius", 10.0, "float")
    sim_database.write("first_vessel_random", False, "bool")
    sim_database.write("p_max_sprout", 0.002, "float")
    sim_database.write("v_sprout", 0.5, "float")
    sim_database.write("vegf_low", 1e-6, "float")
    sim_database.write("vegf_high", 1e-4, "float")
    sim_database.write("p_death", 0.001, "float")
    sim_database.write("rel_radius_new_vessels", 1.0, "float")
    # Oxygen
    sim_database.write("degree_oxygen", 1, "int")
    sim_database.write("n_refinements_oxygen", 0, "int")
    sim_database.write("oxygen_dt_after_cell_update",1.0, "float")
    sim_database.write("oxygen_dt_increase_factor_after_injection", 1.05, "float")
    sim_database.write("oxygen_dt_increase_factor_after_cell_update", 1.05, "float")
    sim_database.write("oxygen_max_dt", 10.0, "float")
    # VEGF-Avastin
    sim_database.write("ktrans", 0.14, "float")
    sim_database.write("degree_vac", 1, "int")
    sim_database.write("n_refinements_vac", 0, "int")
    sim_database.write("vac_dt_after_cell_update", 1.0, "float")
    sim_database.write("vac_dt_increase_factor_after_injection", 1.05, "float")
    sim_database.write("vac_dt_increase_factor_after_cell_update", 1.05, "float")
    sim_database.write("vac_max_dt", 10.0, "float")
    # Chemotherapy
    sim_database.write("ktrans", 0.14, "float")
    chemo_sensitivity = [10000, 10000, 10000]
    sim_database.write("degree_chemo", [1, 1, 1], "int array")
    sim_database.write("n_refinements_chemo", [0, 0, 0], "int array")
    sim_database.write("chemo_sensitivity", chemo_sensitivity, "float array")
    sim_database.write("chemo_dt_after_vessel_update", [0.1, 0.1, 0.1], "float array")
    sim_database.write("chemo_dt_increase_factor_after_injection", [1.00, 1.00, 1.00], "float array")
    sim_database.write("chemo_dt_increase_factor_after_vessel_update", [1.00, 1.00, 1.00], "float array")
    sim_database.write("chemo_max_dt", [10.0, 10.0, 10.0], "float array")
    # Save
    sim_database.write('dt_visualise', 0.5, "float")
    sim_database.write('dt_checkpoint', 0.5, "float")


    base_mesh = BaseMesh(sim_database)
    drugs_in_vessels = DrugsInVessels(sim_database)
    cells = Cells(sim_database, base_mesh)
    vessels = Vessels(sim_database, base_mesh)
    oxygen = Oxygen(sim_database, base_mesh)
    G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')
    G2 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G2')
    G3 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G3')
    vegf_avastin = VegfAvastin(sim_database, base_mesh, drugs_in_vessels)

    save = Save(sim_database, "root/input_output/")
    save.vtk(vessels, cells, oxygen, G1, G2, G3, vegf_avastin)
    save.hdf5(sim_database, 0, vessels, cells, oxygen, G1, G2, G3, vegf_avastin,'w')    
    #checking if the file is overwritten with new results everytime
    for tt in range(1,20):
        save.hdf5(sim_database, tt, vessels, cells, oxygen, G1, G2, G3, vegf_avastin,'a')

    timer.stop()
    save.timings()

    sim_database.delete_from_database()
    sim_database.close()
