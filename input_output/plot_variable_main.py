import __init__
import model_parts.helpers.add_sim_db_to_path
from model_parts.helpers.checkpoint_time import get_time_last_checkpoint
from model_parts.helpers import load_function
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.mesh import BaseMesh
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.chemo import Chemotherapy
from model_parts.oxygen import Oxygen
from model_parts.vegf_avastin import VegfAvastin
from model_parts.drugs_in_vessels import DrugsInVessels

import sim_db
import numpy as np
import argparse
import matplotlib
from matplotlib import gridspec,lines
import matplotlib.pyplot as plt
from matplotlib.patches import ConnectionPatch
import matplotlib.ticker as ticker
from mpl_toolkits import axes_grid1

from dolfin import *
import os

def corner_to_point(point_ax,corner_ax,point,corners):

    for elem in corners:
        x0,x1 = corner_ax.get_xlim()
        y0,y1 = corner_ax.get_ylim()
        if elem == 1: # upper left
            c = [x0,y1]
        elif elem == 2: # upper right
            c = [x1,y1]
        elif elem == 3: # lower right
            c = [x1,y0]
        else: # lower left
            c = [x0,y0]
        con = ConnectionPatch(xyA=c, xyB=point, coordsA="data", coordsB="data",
                        axesA=corner_ax, axesB=point_ax, color="black")
        corner_ax.add_artist(con)

def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)

parser = argparse.ArgumentParser(description='Plot ')
parser.add_argument('--id', '-i', required=True, type=int, help="'ID' of the simultion in 'sim_db' database for which to the time-steps between the different PDE's.")
parser.add_argument('--time', '-t', type=float, nargs='+', help="Timepoints to plot spatial dist in WEEK. Default is the start and the end of the simulation.")
args = parser.parse_args()

sim_database = sim_db.SimDB(db_id=args.id)

res_dir = sim_database.read("results_dir")

t_n = get_time_last_checkpoint(sim_database)
n_checkpoints = sim_database.read('n_checkpoints')
dt_checkpoint = sim_database.read('dt_checkpoint')
tot_time = sim_database.read('tot_time')

if args.time is not None:
    timepoints = args.time
else:
    timepoints = [dt_checkpoint/24/7,(n_checkpoints-1)*dt_checkpoint/24/7]

base_mesh = BaseMesh(sim_database)
mesh = Mesh()
filename = os.path.join(res_dir, "functions.hdf5")
hdf5_file = HDF5File(MPI.comm_world, filename, "r")
hdf5_file.read(mesh,"mesh",False)
base_mesh.mesh = mesh
cell_cycle = RefinedFunction(base_mesh, n_refinements=0)
vegfs = RefinedFunction(base_mesh, n_refinements=0)
p53 = RefinedFunction(base_mesh, n_refinements=0)
hdf5_file.read(vegfs.function, "{0}/vector_{1}".format('vegfs', n_checkpoints-1))
local_vegfs_value=vegfs.get_local_at_cell_positions()

drugs_in_vessels = DrugsInVessels(sim_database)
cells = Cells(sim_database, base_mesh)
hdf5_file.read(cells.automata.function, "{0}/vector_{1}".format('cells', 0))
vessels = Vessels(sim_database, base_mesh)
oxygen = Oxygen(sim_database, base_mesh)

ny, nx = sim_database.read("size")

vegfavastin = VegfAvastin(sim_database,base_mesh,drugs_in_vessels)

#g1 = Chemotherapy(sim_database,base_mesh, drugs_in_vessels, 'G1')
G2 = Chemotherapy(sim_database,base_mesh, drugs_in_vessels, 'G3')

def cancer_lineplot(plt_gs, n_checkpoints = n_checkpoints, 
        dt_checkpoint = dt_checkpoint, tot_time = tot_time, cells=cells):
    cancer_density = []
    for i in range(0,n_checkpoints,2):
        hdf5_file.read(cells.automata.function, "{0}/vector_{1}".format('cells', i))
        cancer_idx = cells.local_cancer_indices()
        cancer_coor = np.unravel_index(cancer_idx,(nx,ny),order='C')        
        cancer_map = np.zeros((nx,ny))
        cancer_map[cancer_coor] = 1
        cancer_density.append([np.sum(cancer_map)/nx/ny,
                            np.sum(cancer_map[:,:int(ny/2)])/nx/ny*2,
                            np.sum(cancer_map[:,int(ny/2):])/nx/ny*2])

    ax1 = plt.subplot(plt_gs[0,0])
    box = ax1.get_position()
    ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax1.set_ylabel('Cancer cell\ndensity')    
    ax1.set_xlabel('Time (weeks)')
    # Put a legend to the right of the current axis
    labels=['Whole biopsy','Edge','Core']
    c = ['r','g','b']
    for i in range(3):  
        plt1= plt.plot(np.arange(0,n_checkpoints,2)/n_checkpoints*tot_time,[x[i] for x in cancer_density],c[i],label=labels[i])
    ax1.axes.set_ylim([0,1])
    ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    ax1.xaxis.tick_top()
    ax1.xaxis.set_label_position('top') 
    return ax1

def biopsy_heatmap(plt_gs, pos_column, t, direction = 'right', show_colormap = True,
        n_checkpoints=n_checkpoints, oxygen=oxygen, vegfavastin=vegfavastin, 
        chemo=G2, cells=cells, vessels=vessels, vegfs=vegfs):
    #find closest checkpoint to t
    t_checkpoint = min(int(t*7*24/dt_checkpoint), n_checkpoints-1)
    # read data at time-point t
    hdf5_file.read(cells.automata.function, "{0}/vector_{1}".format('cells', t_checkpoint))
    hdf5_file.read(oxygen.concentration.function, "{0}/vector_{1}".format('oxygen', t_checkpoint))
    hdf5_file.read(vegfavastin.VAC.function, "{0}/vector_{1}".format('vegf_avastin', t_checkpoint))
    hdf5_file.read(vegfavastin.V.function, "{0}/vector_{1}".format('vegf', t_checkpoint))
    hdf5_file.read(chemo.concentration.function, "{0}/vector_{1}".format('G3', t_checkpoint))
    hdf5_file.read(vessels.relative_radius.function, "{0}/vector_{1}".format('vessels', t_checkpoint))
    hdf5_file.read(vegfs.function, "{0}/vector_{1}".format('vegfs', t_checkpoint))
    local_vegfs_value=vegfs.get_local_at_cell_positions()
    np.savetxt('vegfs.csv',local_vegfs_value,delimiter=',')

    ## get scatter plot for cancer cells
    cancer_idx = cells.local_cancer_indices()
    cancer_coor = np.unravel_index(cancer_idx,(nx,ny),order='C')
    ## get scatter plot for vessels
    vessel_radius = vessels.relative_radius.get_local_at_cell_positions()
    vessel_coor = np.unravel_index(np.nonzero(vessel_radius),(nx,ny),order='C')
    vessel_map = vessel_radius.reshape([nx,ny])

    ax0 = plt.subplot(plt_gs[0,pos_column])
    ax0.scatter(cancer_coor[1]*10, cancer_coor[0]*10, alpha=0.3, marker='.', s= 0.5, c = 'black')
    divider = axes_grid1.make_axes_locatable(ax0)
    cax = divider.append_axes(direction, size="5%", pad=0.05)
    cbar0 = matplotlib.colorbar.ColorbarBase(cax,orientation='vertical')
    cbar0.ax.locator_params(nbins=3)
    cbar0.ax.set_ylabel('cell')
    plt.setp(ax0.get_yticklabels(), visible=False)
    ax0.tick_params(axis='both', which='both', length=0)
    ax0.axes.get_xaxis().set_visible(False)
    ax0.axes.set_xlim([0,ny*10])
    ax0.axes.set_ylim([0,nx*10])
    if (direction=='left'):
        ax0.set_ylabel('Cancer cells')
    cbar0.remove()


#    cbar1.ax.set_yticklabels(['0', '20 mmHg'])
    #p1_scatter = plt.scatter(cancer_coor[1]*10, cancer_coor[0]*10, alpha=0.5, marker='.', c = local_vegfs_value[cancer_idx], s= 0.5,cmap='YlGn')
    # overlay markers for chosen points
#    ax0.get_yaxis().set_label_coords(-10,0.5)
    
    ax1 = plt.subplot(plt_gs[1,pos_column])
#    ax1.set_title('t = {:.3f} week'.format(t_checkpoint/n_checkpoints))
    p1 = plot(oxygen.concentration.function, cmap='plasma')
#    p1.set_clim(0,20)   
    divider = axes_grid1.make_axes_locatable(ax1)
    cax = divider.append_axes(direction, size="5%", pad=0.05)
    if (direction=='left'):
        ax1.set_ylabel('Oxygen')
        cax.remove()    
    if (direction=='right'):
#    cbar1=plt.colorbar(p1, cax=cax, ticks=[0,20])
        cbar1 = matplotlib.colorbar.ColorbarBase(cax, cmap='plasma',
                                norm=matplotlib.colors.Normalize(vmin=0, vmax=20),
                                orientation='vertical')
        cbar1.ax.locator_params(nbins=3)
        cbar1.ax.set_ylabel('[mmHg]')
    plt.setp(ax1.get_yticklabels(), visible=False)
    ax1.tick_params(axis='both', which='both', length=0)
    ax1.axes.get_xaxis().set_visible(False)
    ax1.axes.set_xlim([0,ny*10])
    ax1.axes.set_ylim([0,nx*10])

#    cbar1.ax.set_yticklabels(['0', '20 mmHg'])
    #p1_scatter = plt.scatter(cancer_coor[1]*10, cancer_coor[0]*10, alpha=0.5, marker='.', c = local_vegfs_value[cancer_idx], s= 0.5,cmap='YlGn')
    # overlay markers for chosen points


#    ax1.get_yaxis().set_label_coords(-10,0.5)
    ax2 = plt.subplot(plt_gs[2,pos_column])
    u1,u2,u3 = vegfavastin.VAC.function.split()
    np.savetxt('vegf.csv',vegfavastin.V.get_local_at_cell_positions(),delimiter=',')
#    p2 = plot(vegfavastin.V.function, norm=matplotlib.colors.LogNorm(vmin=1e-14,vmax=1e-3), cmap='PuBu_r')
    p2 = plot(u1, cmap='plasma')
#    p2.set_clim(0,1e-3)
    divider = axes_grid1.make_axes_locatable(ax2)
    cax = divider.append_axes(direction, size="5%", pad=0.05)
    if (direction=='left'):
        ax2.set_ylabel('VEGF')
        cax.remove()    
    if (direction=='right'):
#    cbar2=plt.colorbar(p2, cax=cax, format=ticker.FuncFormatter(fmt))
        cbar2 = matplotlib.colorbar.ColorbarBase(cax, cmap='plasma',
                                norm=matplotlib.colors.Normalize(vmin=0, vmax=0.1),
                                orientation='vertical')    
        cbar2.ax.locator_params(nbins=3)
        cbar2.ax.set_ylabel('[pg/mL]')
    ax2.tick_params(axis='both', which='both', length=0)
    plt.setp(ax2.get_yticklabels(), visible=False)
    ax2.axes.get_xaxis().set_visible(False)
    ax2.axes.set_xlim([0,ny*10])


#    ax2.get_yaxis().set_label_coords(-10,0.5)
    ax3 = plt.subplot(plt_gs[3,pos_column])
    temptemp = chemo.concentration.function.vector().get_local()
    temptemp[temptemp <0] = 1e-11
    chemo.concentration.function.vector().set_local(temptemp)
#    p3 = plot(chemo.concentration.function, norm=matplotlib.colors.LogNorm(vmin=1e-14,vmax=1e-4), cmap='PuBu_r')
    p3 = plot(chemo.concentration.function, cmap='plasma')
#   p3.set_clim(0,1e-4)
    plt.setp(ax3.get_yticklabels(), visible=False)
    ax3.tick_params(axis='both', which='both', length=0)
    ax3.axes.get_xaxis().set_visible(False)

    ax3.axes.set_xlim([0,ny*10])
    p31 = ax3.scatter(vessel_coor[1]*10, vessel_coor[0]*10, alpha=0.5, marker='.', s= 0.5, cmap = 'YlGn', c = vessel_map[vessel_coor])
    divider = axes_grid1.make_axes_locatable(ax3)
    cax_31= divider.append_axes('bottom', size="10%", pad=0.1)
    cbar31=plt.colorbar(p31, cax=cax_31, orientation='horizontal', format=ticker.FuncFormatter(fmt))
    cbar31.ax.locator_params(nbins=3)
    cbar31.ax.tick_params(labelsize=10)
    cbar31.ax.set_xlabel('Permeability[/min]')

    divider = axes_grid1.make_axes_locatable(ax3)
    cax = divider.append_axes(direction, size="5%", pad=0.05)  
    if (direction=='right'):
        cbar3 = matplotlib.colorbar.ColorbarBase(cax, cmap='plasma',
                                norm=matplotlib.colors.Normalize(vmin=0, vmax=1e-3),
                                orientation='vertical')            
#    cbar3=plt.colorbar(p3, cax=cax, format=ticker.FuncFormatter(fmt))
        cbar3.ax.locator_params(nbins=3)
        cbar3.ax.set_ylabel('[pg/mL]')
#    ax3.get_yaxis().set_label_coords(-10,0.5)
    if (direction=='left'):
        ax3.set_ylabel('Chemotherapy')    
        cax.remove()
    corner_to_point(line_ax,ax0,[t,0],[1,2])
# Read mesh for plotting
hdf5_file.read(mesh,"mesh",False)
## get scatter plot for cancer cells
cancer_idx = cells.local_cancer_indices()
cancer_coor = np.unravel_index(cancer_idx,(nx,ny),order='C')
cancer_map = np.zeros((nx,ny))
cancer_map[cancer_coor] = 1

print(t_n)
print(len(cancer_idx))

#print(vessels.p_death)
#u_vec = u.vector()[:]
#print(sum(u_vec))
#print(len(u_vec))
#sim_database.close()

#fig, axes = plt.subplots()
#fig.suptitle('Overview at Week ' + str(int(t_n/24/7)))

plt.rcParams['font.size'] = '13'
fig = plt.figure(figsize=(16,10))

## set up the figure layout
outer = gridspec.GridSpec(3,2,height_ratios = [1.5,8,1],
                            width_ratios=[9,1],
                          hspace = 0.3, wspace = 0.1)
gs1 = gridspec.GridSpecFromSubplotSpec(1,1,
                                       subplot_spec = outer[0,:],
                                       hspace = 0.2)  
gs2 = gridspec.GridSpecFromSubplotSpec(4,2,
                                       subplot_spec = outer[1,0],
                                       hspace = 0.2,
                                       wspace = 0.1 )
gs3 = gridspec.GridSpecFromSubplotSpec(1,3,
                                       subplot_spec = outer[2,:],
                                       hspace = 0.2)
                                     
#xs = np.linspace(-np.pi,np.pi)
#funcs = [np.sin,np.cos,np.tan,np.sinh,np.cosh,np.tanh]
#plt.set_cmap('coolwarm')

line_ax = cancer_lineplot(gs1)

for i in range(len(timepoints)):
    direction='left' if i==0 else 'right'
    biopsy_heatmap(gs2, i, timepoints[i], direction)

ax31 = plt.subplot(gs3[0])
ax31.plot(drugs_in_vessels.times['G1'][:1010]/60/24/7, drugs_in_vessels.concentrations['G1'][:1010])
ax31.set_xlabel('time [week]')  
ax31.text(0.05, 0.95, 'intra G1',
    verticalalignment='top', horizontalalignment='left',
    transform=ax31.transAxes,fontsize=10)   
ax31.set_ylabel('Concentration \n [μg/mL]')
ax32 = plt.subplot(gs3[1])
ax32.plot(drugs_in_vessels.times['G2'][:1010]/60/24/7, drugs_in_vessels.concentrations['G2'][:1010])
ax32.set_xlabel('time [week]')  
ax32.text(0.05, 0.95, 'intra G2',
    verticalalignment='top', horizontalalignment='left',
    transform=ax32.transAxes,fontsize=10)
ax33 = plt.subplot(gs3[2])
ax33.plot(drugs_in_vessels.times['G3'][:1010]/60/24/7, drugs_in_vessels.concentrations['G3'][:1010])
ax33.set_xlabel('time [week]')  
ax33.text(0.05, 0.95, 'intra G3',
    verticalalignment='top', horizontalalignment='left',
    transform=ax33.transAxes,fontsize=10)


fig.savefig('figs/p3_main.pdf',bbox_inches='tight')
