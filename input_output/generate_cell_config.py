# Generate a random cell configuration file to be used for initialisation of 
# the cells.
#
# 0 -> no cell, 1 -> cancer cell, 2 -> normal cell
#
# Use: python generate_cell_config.py --filename 'f.txt' --size nx ny --frac_cancer 0.5
#                                     --frac_normal 0.2 --seed 1234

import __init__
import model_parts.cells 
import numpy as np
import argparse

# Read in parameters from the commandline
parser = argparse.ArgumentParser(description="Generate a random cell configuration file to be used for initialising the cells.")
parser.add_argument('--filename', help='<Required> Name of file generated.', type=str, required=True)
parser.add_argument('--size', nargs='+', help='<Required> Define size of the vessel grid nx, ny', type = int, required=True)
parser.add_argument('--frac_cancer', help='<Required> Fraction of potensial cell positions that are filled with cancer cells.', type=float, required=True)
parser.add_argument('--frac_normal', help='<Required> Fraction of potensial cell positions that are filled with normal cells.', type=float, required=True)
parser.add_argument('--seed', help='<Required> Seed for the random number generator.', type=int, required=True)
args = parser.parse_args()

model_parts.cells.generate_cell_config(args.filename, args.size, 
        args.frac_cancer, args.frac_normal, args.seed)
