import __init__
import model_parts.helpers.add_sim_db_to_path
import sim_db
import argparse
import xml.etree.ElementTree as ET

parser = argparse.ArgumentParser(description='Print timings done by FEniCS and dumped in xml-file.')
parser.add_argument('--filename', '-f', default=None, help='XML-file of timings from FEniCS.')
parser.add_argument('--id', '-i', default=None, type=int, help="'ID' of the simultion in 'sim_db' database for which to print the resulting xml-file of timing.")
parser.add_argument('-n', default=None, type=int, help="Number of timings to print.")
args = parser.parse_args()

if args.filename == None and args.id == None:
    print("ERROR: Either '--filename'/'-f' or '--id'/'-i' MUST be passed as a flag to 'print_timings_xml.py'.")
    exit(1)
elif args.filename != None:
    filename = args.filename
else:
    sd = sim_db.SimDB(db_id=args.id)
    filename = sd.read('results_dir') + '/timings.xml'

tree = ET.parse(filename)
root = tree.getroot()
table = []
for timing in root[0]:
    table.append([timing.get('key')])
    for col in timing:
        table[-1].append(col.get('value'))

table.sort(key=lambda x: float(x[3]), reverse=True)

if args.n != None:
    n = args.n
else:
    n = len(table)

for i in range(n):
    entry = table[i]
    print(entry[0])
    print("\twall tot                  wall avg                   reps    user tot")
    print("\t{0:25} {1:22} {2:>8}    {3:22}".format(entry[3], entry[2], entry[1], entry[5]))
