import __init__
import model_parts.helpers.add_sim_db_to_path
from model_parts.helpers.checkpoint_time import get_time_last_checkpoint
from model_parts.helpers import load_function

from model_parts.helpers.mesh import BaseMesh
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.chemo import Chemotherapy
from model_parts.oxygen import Oxygen
from model_parts.vegf_avastin import VegfAvastin

import sim_db
import argparse

import matplotlib.pyplot as plt
import numpy as np
from dolfin import *
import os

parser = argparse.ArgumentParser(description='Plot ')
parser.add_argument('--id', '-i', required=True, type=int, help="'ID' of the simultion in 'sim_db' database for which to the time-steps between the different PDE's.")
args = parser.parse_args()

sim_database = sim_db.SimDB(db_id=args.id)

res_dir = sim_database.read("results_dir")
t_n = get_time_last_checkpoint(sim_database)

n_checkpoints = sim_database.read('n_checkpoints')
nx, ny = sim_database.read("size")
print(n_checkpoints)

base_mesh = BaseMesh(sim_database)
mesh = Mesh()
filename = os.path.join(res_dir, "functions.hdf5")
hdf5_file = HDF5File(MPI.comm_world, filename, "r")
hdf5_file.read(mesh,"mesh",False)
base_mesh.mesh = mesh

cells = Cells(sim_database, base_mesh)
vessels = Vessels(sim_database, base_mesh)

#vegf_avastin = VegfAvastin(sim_database, base_mesh, drugs_in_vessels)
#G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')
#G2 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G2')
#G3 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G3')
#oxygen = Oxygen(sim_database, base_mesh)

def calculate_density(t_end = n_checkpoints, resolution=100, cells = cells, vessels = vessels):
    hdf5_file.read(cells.automata.function, "{0}/vector_{1}".format('cells', t_end-1))
    hdf5_file.read(vessels.relative_radius.function, "{0}/vector_{1}".format('vessels', t_end-1))

    print(vessels.p_death)
    cancer_idx = cells.local_cancer_indices()
    cancer_coor = np.unravel_index(cancer_idx,(ny,nx),order='C')
    cancer_map = np.zeros((ny,nx))
    cancer_map[cancer_coor] = 1
    cancer_density = []
    j = 0
    while j < 101:
        for i in range(0,nx,resolution):
            cancer_density.append(np.sum(cancer_map[j:resolution - 1 + j,i:resolution - 1 + i])/resolution**2)
        j = j + resolution

    ## Calculate vessel density and relative vessel vol.
    vessel_radius = vessels.relative_radius.get_local_at_cell_positions()
    vessel_coor = np.unravel_index(np.nonzero(vessel_radius),(ny,nx),'C')
    vessel_radius = np.reshape(vessel_radius, [ny,nx])
    vessel_map = np.zeros((ny,nx))
    vessel_map[vessel_coor] = 1
    vessel_density = []    
    j = 0
    while j < 101:
        for i in range(0,nx,resolution):
            vessel_density.append(np.sum(vessel_map[j:resolution - 1 + j,i:resolution - 1 + i]==1)*pi/resolution**2)
        j = j + resolution
    return (cancer_density, vessel_density)

#c = numpy.r_[0, A.cumsum()][indices]
#sums = c[:,1] - c[:,0]

vc1,vp1 = calculate_density(n_checkpoints-1, resolution=100, cells = cells, vessels = vessels)
vc0,vp0 = calculate_density(1, resolution=100, cells = cells, vessels = vessels)
sim_database.close()

print(vp1)
print([vc1[i]-vc0[i] for i in range(len(vc1))])
print([vp1[i]-vp0[i] for i in range(len(vp1))])

#plt.figure()
#p = plot(u,mode='color')
#plt.title("Oxygen solution")
#plt.colorbar(p)
#plt.savefig("demo.png")
