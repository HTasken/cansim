from dolfin import *
import datetime
import os
import resource

def log(statement, filemode='a', directory=None):
    """Print 'statement' on all ranks and max memory usage for each rank and
    write to 'proc{rank}.log' in 'directory'.

    'filemode' need to be set to 'w' or 'a' for it to be written to 'log.txt'.
    If 'filemode' is 'w' the previous content of 'log.txt' is overwritten.

    """
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss    
    rank = MPI.comm_world.Get_rank()
    if rank == 0:
        print(statement)
    if (filemode == 'w' or filemode == 'a'):
        filename = 'proc{}.log'.format(rank)
        if directory != None:
            if directory[-1] == '/':
                directory = directory[:-1]
            filename = directory + '/' + filename
        with open(filename, filemode) as f:
            # Dump timestamp, PID and amount of RAM.
            f.write(statement + '\n')
            f.write('{} {} {}M\n'.format(datetime.datetime.now(), os.getpid(), mem/1024))
