import __init__
from model_parts.helpers.mesh import BaseMesh
from model_parts.drugs_in_vessels import DrugsInVessels
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.chemo import Chemotherapy
from model_parts.oxygen import Oxygen
from model_parts.vegf_avastin import VegfAvastin
from input_output.save import Save
import model_parts.helpers.add_sim_db_to_path
import sim_db
import argparse
from dolfin import *

def command_line_argument_parser():
    parser = argparse.ArgumentParser(description='Print error norm (L2 norm of error) functions in two specified simulations.')
    parser.add_argument('--ids', '-i', nargs='+', required=True, type=int, help="<Required> 'ID' of two simultion in 'sim_db' database to complare functions with error norm.")
    parser.add_argument('--L2_norm', action="store_true", help="Print L2 norm of the functions instead of the errors.")
    return parser.parse_args()

def get_simulation_functions(db_id):
    sim_database = sim_db.SimDB(db_id=db_id)
    drugs_in_vessels = DrugsInVessels(sim_database)
    base_mesh = BaseMesh(sim_database)

    cells = Cells(sim_database, base_mesh)
    cells_func = cells.automata.function

    vessels = Vessels(sim_database, base_mesh)
    vessels_func = vessels.relative_radius.function
                      
    G1 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G1')
    G1_func = G1.concentration.function
    G2 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G2')
    G2_func = G2.concentration.function
    G3 = Chemotherapy(sim_database, base_mesh, drugs_in_vessels, 'G3')
    G3_func = G3.concentration.function

    oxygen = Oxygen(sim_database, base_mesh)
    oxygen_func = oxygen.concentration.function

    vegf_avastin = VegfAvastin(sim_database, base_mesh, drugs_in_vessels)
    vegf_avastin_func = vegf_avastin.VAC.function

    sim_database.end()

    return [vessels_func, cells_func, oxygen_func, G1_func, G2_func, G3_func, 
            vegf_avastin_func]
    
def calc_error_norms(id_1, id_2):
    functions_1 = get_simulation_functions(id_1)
    functions_2 = get_simulation_functions(id_2)
    error_norms = []
    for func_1, func_2 in zip(functions_1, functions_2):
        func_1 = project(func_1, func_2.function_space())
        error_norms.append(errornorm(func_1, func_2))
    return error_norms

def calc_l2_norms(db_id):
    functions = get_simulation_functions(db_id)
    l2_norms = []
    for func in functions:
        l2_norms.append(assemble(inner(func, func)*dx()))
    return l2_norms

def print_error_norms(error_norms):
    print("Vessels L2 error: ", error_norms[0])
    print("Cells L2 error: ", error_norms[1])
    print("Oxygen L2 error: ", error_norms[2])
    print("G1 L2 error: ", error_norms[3])
    print("G2 L2 error: ", error_norms[4])
    print("G3 L2 error: ", error_norms[5])
    print("VEGF-Avastin L2 error: ", error_norms[6])

def print_l2_norms(error_norms):
    print("Vessels L2 norm: ", error_norms[0])
    print("Cells L2 norm: ", error_norms[1])
    print("Oxygen L2 norm: ", error_norms[2])
    print("G1 L2 norm: ", error_norms[3])
    print("G2 L2 norm: ", error_norms[4])
    print("G3 L2 norm: ", error_norms[5])
    print("VEGF-Avastin L2 norm: ", error_norms[6])
    
if __name__ == "__main__":
    args = command_line_argument_parser()
    if args.L2_norm:
        if len(args.ids) != 1:
            print("Need to pass a single ID's as command line arguments to "
                  "calculate L2 norm.")
        print_l2_norms(calc_l2_norms(args.ids[0]))
    else:
        if len(args.ids) != 2:
            print("Need to pass a two ID's as command line arguments to "
                  "calculate L2 error norm.")
        print_error_norms(calc_error_norms(args.ids[0], args.ids[1]))
