import __init__
import model_parts.helpers.add_sim_db_to_path
from model_parts.helpers.checkpoint_time import get_time_last_checkpoint
from model_parts.helpers import load_function
from model_parts.helpers.refined_function import RefinedFunction
from model_parts.helpers.mesh import BaseMesh
from model_parts.cells import Cells
from model_parts.vessels import Vessels
from model_parts.chemo import Chemotherapy
from model_parts.oxygen import Oxygen
from model_parts.vegf_avastin import VegfAvastin
from model_parts.drugs_in_vessels import DrugsInVessels

import sim_db
import numpy as np
import argparse
import matplotlib
from matplotlib import gridspec,lines
import matplotlib.pyplot as plt
from matplotlib.patches import ConnectionPatch
import matplotlib.ticker as ticker
from mpl_toolkits import axes_grid1

from dolfin import *
import os

def corner_to_point(point_ax,corner_ax,point,corners):

    for elem in corners:
        x0,x1 = corner_ax.get_xlim()
        y0,y1 = corner_ax.get_ylim()
        if elem == 1: # upper left
            c = [x0,y1]
        elif elem == 2: # upper right
            c = [x1,y1]
        elif elem == 3: # lower right
            c = [x1,y0]
        else: # lower left
            c = [x0,y0]
        con = ConnectionPatch(xyA=c, xyB=point, coordsA="data", coordsB="data",
                        axesA=corner_ax, axesB=point_ax, color="blue")
        corner_ax.add_artist(con)

def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)

parser = argparse.ArgumentParser(description='Plot ')
parser.add_argument('--id', '-i', required=True, type=int, help="'ID' of the simultion in 'sim_db' database for which to the time-steps between the different PDE's.")
args = parser.parse_args()

sim_database = sim_db.SimDB(db_id=args.id)

res_dir = sim_database.read("results_dir")

t_n = get_time_last_checkpoint(sim_database)
n_checkpoints = sim_database.read('n_checkpoints')
dt_checkpoint = sim_database.read('dt_checkpoint')

base_mesh = BaseMesh(sim_database)
mesh = Mesh()
filename = os.path.join(res_dir, "functions.hdf5")
hdf5_file = HDF5File(MPI.comm_world, filename, "r")
hdf5_file.read(mesh,"mesh",False)
base_mesh.mesh = mesh
cell_cycle = RefinedFunction(base_mesh, n_refinements=0)
vegfs = RefinedFunction(base_mesh, n_refinements=0)
p53 = RefinedFunction(base_mesh, n_refinements=0)
hdf5_file.read(vegfs.function, "{0}/vector_{1}".format('vegfs', n_checkpoints-1))
local_vegfs_value=vegfs.get_local_at_cell_positions()

drugs_in_vessels = DrugsInVessels(sim_database)
cells = Cells(sim_database, base_mesh)
hdf5_file.read(cells.automata.function, "{0}/vector_{1}".format('cells', n_checkpoints-1))
vessels = Vessels(sim_database, base_mesh)
oxygen = Oxygen(sim_database, base_mesh)

ny, nx = sim_database.read("size")

vegfavastin = VegfAvastin(sim_database,base_mesh,drugs_in_vessels)

#g1 = Chemotherapy(sim_database,base_mesh, drugs_in_vessels, 'G1')
G2 = Chemotherapy(sim_database,base_mesh, drugs_in_vessels, 'G3')

def biopsy_heatmap(plt_gs, chosen_x, chosen_y, t, direction = 'right', n_checkpoints=n_checkpoints, 
        oxygen=oxygen, vegfavastin=vegfavastin, chemo=G2, cells=cells, vessels=vessels, vegfs=vegfs):
    #find closest checkpoint to t
    t_checkpoint = min(int(t*7*24/dt_checkpoint), n_checkpoints-1)
    # read data at time-point t
    hdf5_file.read(cells.automata.function, "{0}/vector_{1}".format('cells', t_checkpoint))
    hdf5_file.read(oxygen.concentration.function, "{0}/vector_{1}".format('oxygen', t_checkpoint))
    hdf5_file.read(vegfavastin.VAC.function, "{0}/vector_{1}".format('vegf_avastin', t_checkpoint))
    hdf5_file.read(vegfavastin.V.function, "{0}/vector_{1}".format('vegf', t_checkpoint))
    hdf5_file.read(chemo.concentration.function, "{0}/vector_{1}".format('G3', t_checkpoint))
    hdf5_file.read(vessels.relative_radius.function, "{0}/vector_{1}".format('vessels', t_checkpoint))
    hdf5_file.read(vegfs.function, "{0}/vector_{1}".format('vegfs', t_checkpoint))
    local_vegfs_value=vegfs.get_local_at_cell_positions()
    np.savetxt('vegfs.csv',local_vegfs_value,delimiter=',')

    ## get scatter plot for cancer cells
    cancer_idx = cells.local_cancer_indices()
    cancer_coor = np.unravel_index(cancer_idx,(nx,ny),order='C')
    ## get scatter plot for vessels
    vessel_radius = vessels.relative_radius.get_local_at_cell_positions()
    vessel_coor = np.unravel_index(np.nonzero(vessel_radius),(nx,ny),order='C')
    vessel_map = vessel_radius.reshape([nx,ny])

    ax1 = plt.subplot(plt_gs[0])
#    ax1.set_title('t = {:.3f} week'.format(t_checkpoint/n_checkpoints))
    p1 = plot(oxygen.concentration.function, cmap='plasma')
    divider = axes_grid1.make_axes_locatable(ax1)
    cax = divider.append_axes(direction, size="5%", pad=0.05)
    cbar1=plt.colorbar(p1, cax=cax, ticks=[0,20])
    ax1.axes.get_xaxis().set_visible(False)
    ax1.axes.get_yaxis().set_visible(False)
    ax1.axes.set_xlim([0,ny*10])
    ax1.axes.set_ylim([0,nx*10])
    cbar1.ax.set_ylabel('[mmHg]', fontsize = 9)
#    cbar1.ax.set_yticklabels(['0', '20 mmHg'])
    #p1_scatter = plt.scatter(cancer_coor[1]*10, cancer_coor[0]*10, alpha=0.5, marker='.', c = local_vegfs_value[cancer_idx], s= 0.5,cmap='YlGn')
#    ax1.scatter(cancer_coor[1]*10, cancer_coor[0]*10, alpha=0.3, marker='.', s= 0.5, c = local_vegfs_value[cancer_idx])
    # overlay markers for chosen points
    ax1.scatter(chosen_x[0]*10,chosen_y[0]*10,s=10, facecolors='none', edgecolors='r',marker='o')
    ax1.scatter(chosen_x[1]*10,chosen_y[1]*10,s=10, color='red', marker='*')
    if (direction=='left'):
        cax.yaxis.tick_left()  
        cax.yaxis.set_label_position('left')
    ax2 = plt.subplot(plt_gs[1])
    u1,u2,u3 = vegfavastin.VAC.function.split()
    np.savetxt('vegf.csv',vegfavastin.V.get_local_at_cell_positions(),delimiter=',')
#    p2 = plot(vegfavastin.V.function, norm=matplotlib.colors.LogNorm(vmin=1e-14,vmax=1e-3), cmap='PuBu_r')
    p2 = plot(u1, cmap='plasma')
#    p2.set_clim(0,1e-3)
    divider = axes_grid1.make_axes_locatable(ax2)
    cax = divider.append_axes(direction, size="5%", pad=0.05)
    cbar2=plt.colorbar(p2, cax=cax, format=ticker.FuncFormatter(fmt))
    cbar2.ax.locator_params(nbins=3)
    cbar2.ax.set_ylabel('[μg/mL]', fontsize = 9)
    ax2.axes.get_xaxis().set_visible(False)
    ax2.axes.get_yaxis().set_visible(False)
    ax2.axes.set_xlim([0,ny*10])
    if (direction=='left'):
        cax.yaxis.tick_left()  
        cax.yaxis.set_label_position('left')
    ax3 = plt.subplot(plt_gs[2])
#    temptemp = chemo.concentration.function.vector().get_local()
#    temptemp[temptemp <0] = 1e-11
#    chemo.concentration.function.vector().set_local(temptemp)
#    p3 = plot(chemo.concentration.function, norm=matplotlib.colors.LogNorm(vmin=1e-14,vmax=1e-4), cmap='PuBu_r')
    p3 = plot(chemo.concentration.function, cmap='plasma')
#   p3.set_clim(0,1e-4)
    ax3.axes.get_xaxis().set_visible(False)
    ax3.axes.get_yaxis().set_visible(False)
    ax3.axes.set_xlim([0,ny*10])
    p31 = ax3.scatter(vessel_coor[1]*10, vessel_coor[0]*10, alpha=0.5, marker='.', s= 0.5, cmap = 'YlGn', c = vessel_map[vessel_coor])
    divider = axes_grid1.make_axes_locatable(ax3)
    cax_31= divider.append_axes('bottom', size="5%", pad=0.1)
    cbar31=plt.colorbar(p31, cax=cax_31, orientation='horizontal', format=ticker.FuncFormatter(fmt))
    cbar31.ax.locator_params(nbins=3)
    cbar31.ax.set_xlabel('Permeability[/min]', fontsize = 9)  

    divider = axes_grid1.make_axes_locatable(ax3)
    cax = divider.append_axes(direction, size="5%", pad=0.05)  
    cbar3=plt.colorbar(p3, cax=cax, format=ticker.FuncFormatter(fmt))
    cbar3.ax.locator_params(nbins=3)
    cbar3.ax.set_ylabel('[μg/mL]', fontsize = 9)
    if (direction=='left'):
        cax.yaxis.tick_left()  
        cax.yaxis.set_label_position('left')  
    plt.tight_layout()

# Read mesh for plotting
hdf5_file.read(mesh,"mesh",False)
## get scatter plot for cancer cells
cancer_idx = cells.local_cancer_indices()
cancer_coor = np.unravel_index(cancer_idx,(nx,ny),order='C')
cancer_map = np.zeros((nx,ny))
cancer_map[cancer_coor] = 1

print(t_n)
print(len(cancer_idx))

#print(vessels.p_death)
#u_vec = u.vector()[:]
#print(sum(u_vec))
#print(len(u_vec))
#sim_database.close()

#fig, axes = plt.subplots()
#fig.suptitle('Overview at Week ' + str(int(t_n/24/7)))


fig = plt.figure(figsize=(16,10))

## set up the figure layout
outer = gridspec.GridSpec(3,4,height_ratios = [2,5,1],
                          hspace = 0.2, wspace = 0)

gs1 = gridspec.GridSpecFromSubplotSpec(3,2,
                                       subplot_spec = outer[0,1:3],
                                       hspace = 0,
                                       wspace = 0.2)
gs2 = gridspec.GridSpecFromSubplotSpec(3,1,
                                       subplot_spec = outer[1,:2],
                                       hspace = 0,
                                       wspace = 0.05)
gs3 = gridspec.GridSpecFromSubplotSpec(3,1,
                                       subplot_spec = outer[1,2:4],
                                       hspace = 0,
                                       wspace = 0.05)
gs4 = gridspec.GridSpecFromSubplotSpec(1,3,
                                       subplot_spec = outer[2,:],
                                       hspace = 0.2)
#xs = np.linspace(-np.pi,np.pi)
#funcs = [np.sin,np.cos,np.tan,np.sinh,np.cosh,np.tanh]
labels = ['Cell Cycle', 'VEGFs', 'p53']
#plt.set_cmap('coolwarm')



chosen_coorx = []
chosen_coory = []

grid = []
## Choose two cells (at random) for plotting
k = 0
#chosen_number = np.random.choice(range(len(cancer_idx)),2,replace=False)
chosen_number = [36850,98295]
markerstyle=['o','*']
for col_number in range(len(chosen_number)):
    i = chosen_number[col_number]
    chosen_idx = cancer_idx[i]
    chosen_coorx.append(cancer_coor[1][i])
    chosen_coory.append(cancer_coor[0][i])
    # gather time series for all subscellular values
    vegf_i = []
    p53_i = []
    cellcycle_i = []
    xs_cell = []
    xs = []
    for j in range(0, n_checkpoints, 10):
        hdf5_file.read(cell_cycle.function, "{0}/vector_{1}".format('cell_cycle', j))
        hdf5_file.read(p53.function, "{0}/vector_{1}".format('p53', j))
        hdf5_file.read(vegfs.function, "{0}/vector_{1}".format('vegfs', j))
        vegf_i.append(vegfs.get_local_at_cell_positions()[chosen_idx])
        p53_i.append(p53.get_local_at_cell_positions()[chosen_idx])        
        cellcycle_i.append(cell_cycle.get_local_at_cell_positions()[chosen_idx])
        xs.append(j/n_checkpoints)
        xs_cell.append(j/n_checkpoints)
    
    print(vegfavastin.V.get_local_at_cell_positions()[chosen_idx]) 
    print(cellcycle_i)
    print(vegf_i)
    print(p53_i)
    ax = plt.subplot(gs1[k])
    ax.plot(xs_cell,cellcycle_i)
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.set_ylim([0,1])
    ax.title.set_text('Cell marked with {:s}'.format(markerstyle[col_number]))
#    ax.axes.get_yaxis().set_visible(False)
    ax.text(0.05, 0.95, labels[0],
            verticalalignment='top', horizontalalignment='left',
            transform=ax.transAxes,fontsize=10)    
    grid.append(ax)
    ax = plt.subplot(gs1[k+2])
    ax.plot(xs,vegf_i)
    ax.axes.get_xaxis().set_visible(False)
#    ax.axes.set_ylim([0,None])
#    ax.axes.get_yaxis().set_visible(False)
    ax.text(0.05, 0.95, labels[1],
            verticalalignment='top', horizontalalignment='left',
            transform=ax.transAxes,fontsize=10)
    ax.axes.set_ylim([0,np.max(vegf_i)*1.1])        
    grid.append(ax)
    ax = plt.subplot(gs1[k+4])
    ax.plot(xs,p53_i)
#    ax.axes.get_xaxis().set_visible(False)
#    ax.axes.get_yaxis().set_visible(False)
    ax.text(0.05, 0.95, labels[2],
            verticalalignment='top', horizontalalignment='left',
            transform=ax.transAxes,fontsize=10)
    ax.set_xlabel('time [week]')  
    ax.axes.set_ylim([0, np.max(p53_i)*1.1])
    grid.append(ax)
    k = k + 1
    # ax.text(0.05, 0.95, label,
    #         verticalalignment='top', horizontalalignment='left',
    #         transform=ax.transAxes,fontsize=15)
print(chosen_coorx)
print(chosen_coory)

biopsy_heatmap(gs2, chosen_coorx, chosen_coory, 0.03, 'left')
biopsy_heatmap(gs3, chosen_coorx, chosen_coory, 1.0, 'right')
print(len(drugs_in_vessels.times['G1']))
ax31 = plt.subplot(gs4[0])
ax31.plot(drugs_in_vessels.times['G1'][:1010]/60/24/7, drugs_in_vessels.concentrations['G1'][:1010])
ax31.set_xlabel('time [week]')  
ax31.text(0.05, 0.95, 'intra G1',
    verticalalignment='top', horizontalalignment='left',
    transform=ax31.transAxes,fontsize=10)   
ax31.set_ylabel('Concentration \n [μg/mL]', fontsize = 9)
ax32 = plt.subplot(gs4[1])
ax32.plot(drugs_in_vessels.times['G2'][:1010]/60/24/7, drugs_in_vessels.concentrations['G2'][:1010])
ax32.set_xlabel('time [week]')  
ax32.text(0.05, 0.95, 'intra G2',
    verticalalignment='top', horizontalalignment='left',
    transform=ax32.transAxes,fontsize=10)
ax33 = plt.subplot(gs4[2])
ax33.plot(drugs_in_vessels.times['G3'][:1010]/60/24/7, drugs_in_vessels.concentrations['G3'][:1010])
ax33.set_xlabel('time [week]')  
ax33.text(0.05, 0.95, 'intra G3',
    verticalalignment='top', horizontalalignment='left',
    transform=ax33.transAxes,fontsize=10)
#ax2.plot([,3],[-0.15,0.15],'r^')

#corner_to_point(ax2,grid[2],[chosen_coorx[0]*10,chosen_coory[0]*10],[3,4])
#corner_to_point(ax2,grid[5],[chosen_coorx[1]*10,chosen_coory[1]*10],[3,4])
#plt.sca(ax2)


#plt.sca(ax4)

plt.show()

""" plt.figure()
plt.set_cmap('coolwarm')
plt.subplot(3,1,1)
p1 = plot(oxygen.concentration.function,vmin=0,vmax=25)
p1_scatter = plt.scatter(cancer_coor[1]*10, cancer_coor[0]*10, alpha=0.5, marker='.', c = local_vegfs_value[cancer_idx], s= 0.5, cmap='Greens')
cbar1 = plt.colorbar(p1,ticks=[0,20])
cbar1_scattter = plt.colorbar(p1_scatter)
cbar1.ax.set_yticklabels(['0 mmHg', '20 mmHg'])
plt.axis('off')
plt.subplot(3,1,2)
p2 = plot(g1.concentration.function)
cbar2 = plt.colorbar(p2)
plt.axis('off')
plt.subplot(3,1,3)
p3 = plot(vegf)
cbar3 = plt.colorbar(p3)
plt.axis('off')
plt.show() """

#for ax, zord in zip(axes, [1, -1]):
#    ax.plot()
#plt.scatter(cancer_coor[1]*10, cancer_coor[0]*10, alpha=0.5, marker='.', s=1)
#plt.xlabel('x') 
#plt.ylabel('y')
#

#plt.title("Oxygen solution")
#plt.colorbar(p)
#plt.savefig("demo.png")
