# Generate a random vessel configuration file to be used for the initialisation 
# of the vessels. 
# By using this file for initialisation, the placement can be calculated and 
# inspected in advance. 
# The Vessels class' vessel_placement method is used to be generate both this 
# configuration file and the configuration if no file is provided, to 
# guarantee the same result.
#
# Use: 'python generate_vessel_config.py --filename 'f.txt' --size nx ny
#                                       --vp 11.97 --vessel_sep 4 --seed 1234'
# copy: python generate_vessel_config.py --filename vessel_init.txt --size 100 100 --vp 11.97 --vessel_sep 4 --seed 1234 --first_vessel_random --make_video

import __init__
from model_parts.vessels import Vessels
from model_parts.helpers.mesh import BaseMesh
import numpy as np
import argparse


# Read in parameters from the commandline
parser = argparse.ArgumentParser(description="Generate a random vessel configuration file to be used for initialising the vessels.")
parser.add_argument('--filename', help='<Required> Name of file generated.', type=str, required=True)
parser.add_argument('--size', nargs='+', help='<Required> Define size of the vessel grid nx, ny', type = int, required=True)
parser.add_argument('--vessel_denisty', type=float, required=True, help='<Required> Density of the vessels, equivalent to the volume of blood per volume.')
parser.add_argument('--avg_radius', type=float, defalut=10.0, help='The average radius of the vessels given in micrometers.')
parser.add_argument('--vessel_sep', help='<Required> Separation of vessels.', type=int, required=True)
parser.add_argument('--first_vessel_random', action='store_true', help='Place the first vessel in a random place. Otherwise it is placed in the middle.')
parser.add_argument('--seed', help='<Required> Seed for the random number generator.', type=int, required=True)
parser.add_argument('--make_video', action='store_true', help='Make a video of placement of vessels.')
args = parser.parse_args()

np.random.seed(args.seed)

base_mesh = BaseMesh([2,2], 2)
vessels = Vessels(base_mesh, 0, 0, 0, vp=0, vessel_sep=0) 
vessel_init = vessels.generate_vessel_placement([args.size[0], args.size[1]], 
                                                args.seed, 
                                                args.vessel_denisty, 
                                                args.vessel_sep, 
                                                args.first_vessel_random,
                                                make_video=args.make_video)

np.savetxt(args.filename, vessel_init, fmt='%d', delimiter='\n')
